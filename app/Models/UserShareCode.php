<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserShareCode extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "user_id",
        "code"
    ];

    public function user(){
        return $this->belongsTo('App\Models\User'  , 'user_id','id');
    }
    public function hasOneCode(){
//        return $this->hasOne('id','user_id');
        return $this->hasOne('App\Models\User'  , 'user_id');
    }
}
