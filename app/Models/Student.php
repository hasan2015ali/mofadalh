<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
   // protected $guarded =["name","mother_name","last_name","mobile"];
   protected $fillable = ["national_id","name","mother_name","last_name","center_type_id","mobile"];

}
