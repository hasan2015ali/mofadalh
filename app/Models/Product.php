<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Category;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $appends = ['stars_average'];
    protected $fillable = [
        "name",
        "offer_price",
        "cat_id",
        "images",
        "discount",
        "offer_image",
        "discount",
        "available",
        "in_slider"
    ];
    public function cat(){
        return $this->belongsTo('App\Models\Category','cat_id');
    }
    public function prices(){
        return $this->hasMany('App\Models\Price','product_id');
    }
//    public function star_avg(){
//        $stars_sum =  $stars->pluck('stars');
//        $rating =  $stars_sum->avg();
//        return $this->$rating;
//
//        return $this->hasMany('App\Models\Price','product_id');
//    }
    public function comments(){
        return $this->hasMany('App\Models\Comment','product_id');
    }

    public function stars(){
        return $this->hasMany('App\Models\ProductStars','product_id');
    }
    public function getStarsAverageAttribute(){
       return $this->stars()->average('stars');
    }
    public function translations(){
        return $this->hasMany('App\Models\ProductTranlation', 'item_id','id');
    }
}
