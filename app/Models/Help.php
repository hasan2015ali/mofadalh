<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Help extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "title",
        "video",

    ];
    public function translations(){
        return $this->hasMany('App\Models\HelpTranslation', 'item_id','id');
    }

}
