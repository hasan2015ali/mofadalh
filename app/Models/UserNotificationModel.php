<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotificationModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "user_id",
        "region_id",
        "role_id",
        "title",
        "details",
        "img"
    ];
    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function region(){
        return $this->belongsTo('App\Models\Country','region_id','id');
    }

    public function role(){
//        return $this->belongsTo('App\Models\DistribuationRegion','region_id','id');

        return $this->belongsTo('App\Models\Role','role_id','id');
    }


}
