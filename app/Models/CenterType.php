<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CenterType extends Model
{
    use HasFactory;

    protected $guarded = [];

    public  function centers(){
        return $this->hasMany("App\Models\Center","center_type_id","id");
    }
}
