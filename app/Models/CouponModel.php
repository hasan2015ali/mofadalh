<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='coupon_models';
    protected $fillable = [
        "name",
        "code",
        "discount",
        "expiry"
    ];

    protected $hidden=["repetition"];
    protected $casts = [
        'created_at' => 'datetime:Y/m/d',
        'expiry' => 'datetime:Y/m/d',
    ];
}
