<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliverRegion extends Model
{

    /****
     * name : name of category.
     * icon : icon for category.
     *  order_id : not used currently, will use to order categories in future.
     * parent_id : will be 0 if category is main category other wise will be the number of category parent
     *
     * Func:
     * childs() : return list of Catgory Childs
     */
    use SoftDeletes;
    use HasFactory;
    protected $fillable = [
        "deliverPrice",
        "regionName",

    ];

}
