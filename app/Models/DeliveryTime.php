<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryTime extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "name",
        "from_time",
        "to_time",

    ];
}
