<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAppointment extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function student(){


        return $this->belongsTo('App\Models\Student',"student_id","national_id");
    }

}
//,"name","mother_name","last_name","mobile"
