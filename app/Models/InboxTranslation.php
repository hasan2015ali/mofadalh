<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class InboxTranslation extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "title",
        "details",
        "lang",
        "item_id"
    ];
}
