<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable,HasFactory ,HasRoles;
    use SoftDeletes;
//HasApiTokens,

    /**
     * The attributes that are mass assignable.
     * name : name of user or Supermarket. user, supermarket
     * email : optional for user ,required for supermarket
     * password : required , for user , supermarket
     * address : optional for user and supermaket
     * block : optional for user refers the block address name.
     * worktime : optional for future not used now.
     * lat : latitude : not used now
     * lng : langitude : not used now
     * role : user role 3 :for center-manager ,  4 for emp, 5 for hea
     * avatar : personal image : not used now
     * image : supermarket image
     * status : 0 not accepted. we disable this feature now.
     * is_verified : to check if account was verfied via sms
     * sms_code : when use confirm via sms
     * region_id : that user or supermaket belongs to. required
     * ratio : not
     * earniings : not
     * featured : 1 if supermarket is in featured list , 0 otherwise
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        "phone",
        "address",
        "lat",
        "lng",
        "role",
        "avatar",
        "image",
        "status",
        "is_verified",
        "sms_code",
        "block",
        "center_id"




    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function center(){
        return $this->belongsTo('App\Models\Center','center_id');
    }


}
