<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserOrderDetail extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "order_id",
        "product_id",
        "amount",
        "type_amount",
        "single_price",
        "full_price",
    ];

    public function product(){
        return $this->hasOne('App\Models\Product'  , 'product_id','id');
    }
}
