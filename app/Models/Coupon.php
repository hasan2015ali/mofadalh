<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "user_id",
        "val",
        "end_date",
        "used",
        "share_code"
    ];

    public function user(){
        return $this->belongsTo('App\Models\User'  , 'user_id','id');
    }
    public function shareCode(){
        return $this->belongsTo('App\Models\UserShareCode'  , 'share_code','id');
    }
}
