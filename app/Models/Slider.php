<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Slider extends Model
{
    use HasFactory , SoftDeletes;
    public $fillable = [
        "title",
        "details",
        "image",
        "product_id"
    ];

    public function product(){
        return $this->belongsTo('App\Models\Product' , 'product_id','id');
    }
}
