<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inbox extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "title",
        "details",

    ];

    public function translations(){
        return $this->hasMany('App\Models\InboxTranslation', 'item_id','id');
    }
}
