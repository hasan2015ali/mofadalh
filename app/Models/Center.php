<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    use HasFactory;
    protected $guarded = [];
    public  function type(){
        return $this->belongsTo("App\Models\CenterType","center_type_id","id");
    }
    public  function manager(){
        return $this->belongsTo("App\Models\User","manager_id","id");
    }
}
