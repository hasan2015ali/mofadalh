<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AdverNotification extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "title",
        "expiry",

    ];

    public function translations(){
        return $this->hasMany('App\Models\AdvertNotificationTranslation', 'item_id','id');
    }
}
