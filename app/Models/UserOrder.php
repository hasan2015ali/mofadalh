<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserOrder extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "user_id",
        "region_id",
        "region_address",
        "total",
        "loc_x",
        "loc_y",
        "details",
        "status",
        "delivery_at",
        "deliver_type",
        "deliver_date",
        "deliver_end_time",
        "coupon",
        "zip_code",
        "street",
        "city"
    ];

    public function region(){
        return $this->belongsTo('App\Models\Country'  , 'region_id','id');
    }
    public function client(){
        return $this->belongsTo('App\Models\User'  , 'user_id','id');
    }
    public function products(){
        return $this->hasMany('App\Models\UserOrderDetail'  , 'order_id','id');
    }
    public function products1(){
        return $this->hasManyThrough('App\Models\Product'  , 'App\Models\UserOrderDetail','order_id' ,'product_id',   'id'  ,'id');

    }


    protected $casts = [
        'created_at' => 'datetime:H:i m/d/Y',
        'delivery_at' => 'datetime:H:i m/d/Y',
    ];
}
