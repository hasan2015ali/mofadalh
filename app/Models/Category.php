<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    /****
     * name : name of category.
     * icon : icon for category.
     *  order_id : not used currently, will use to order categories in future.
     * parent_id : will be 0 if category is main category other wise will be the number of category parent
     *
     * Func:
     * childs() : return list of Catgory Childs
     */

    use SoftDeletes;
    use HasFactory;
    protected $fillable = [
        "name",
        "icon",
        "deleted",
        "order_id",
        "parent_id",

    ];

    public function childs(){
        return $this->hasMany('App\Models\Category', 'parent_id','id');
    }
    public function translations(){
        return $this->hasMany('App\Models\CategoryTranslation', 'cat_id','id');
    }
}
