<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryTranslation extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        "name",
        "lang",
        "cat_id"

    ];

    public function category(){
        return $this->belongsTo('App\Models\Category'  , 'cat_id','id');
    }
}
