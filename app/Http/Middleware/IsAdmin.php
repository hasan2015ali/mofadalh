<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login_form');
        }
        if (Auth::user()->role == 4 || Auth::user()->role == 1) {

            return $next($request);

        }
        else if (Auth::user()->role == 1) {
//            return redirect()->route('crud.index',['table'=>'categories']);

            return redirect()->route('home');
        }
    }
}
