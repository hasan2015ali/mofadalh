<?php

namespace App\Http\Controllers;

use App\Models\Center;
use App\Models\CenterEmp;
use App\Models\CenterType;
use App\Models\StudentAppointment;
use App\Models\Student;
use Illuminate\Http\Request;


use DB;


class AppointmentController extends Controller
{
    //زر بحث
    public function search(Request  $request , $center_type = null, $center_id = null){

        if(!empty($center_type) && !empty($center_id)){
            $centers = Center::where('center_type_id',$center_type)->get();
            $student_appointMents = [];
            $student = [];

            return view('student.appointments', compact('center_type','center_id','centers','student_appointMents','student'));

        }
       $request->validate(
            [
                'national_id'=>'required|min:2|max:16|',

            ],

        [
            "national_id.required"=>"الرجاء ادخال  رقم الاكتتاب ",
            //"national_id.unique"=>"  الرقم الوطني موجود ...لقد قمت بالحجز مسبقا !! ان كنت تريد تعديل الحجز اضغط على زر تعديل   ",
        ]

        );


        $student_appointMents = StudentAppointment::where('student_id',$request->national_id)->get();
        $student = Student::where('center_type_id',$request->center_type_id)->get();

        $centers = Center::where('center_type_id',$request->center_type_id)->get();
        $national_id = $request->national_id;
        $center_type_id = $request->center_type_id;
        return view('student.appointments', compact('student_appointMents','centers','national_id','student','center_type_id'));

    }
    //استعراض المراكز
    public  function viewCenters(){
        $centers = Center::with('type')->orderByDesc('id')->get();
        return view('student.centers', compact('centers'));
    }   public  function view(){
        $center_types = CenterType::orderByDesc('id')->get();
        return view('appointment', compact('center_types'));
    }
    // حفظ
    public function getAppointment(Request  $request){
        $center_id = $request->center_id;

        $app_date = $request->appointment_date;
        $appointment_time = $request->appointment_time;

        $request->validate(
            [
                'national_id'=>'required|min:5|max:16|unique:students',
                'name'=>'required',
                'mother_name'=>'required',
                'last_name'=>'required',
                'mobile'=>'required|min:7|max:10|',

            ]);



        // get center emps available count
        $center_emps = CenterEmp::where([['status',1],['center_id',$center_id]])->get();
        if(count($center_emps) <=0){
            return response()->json([
                "status"=>201,
                "message"=> "المركز مغلق حاليا" ,
            ]);

        }
        // set service time for each emps as 10 minutes
        // show to user how much time to wait only ?
        $service_time = 10;
        $date_time_appointments = StudentAppointment::where([['date',$app_date],["time",$appointment_time]])->get();

        if(count($date_time_appointments) >= count($center_emps)){
            return response()->json([
                "status"=>201,
                "message"=>"هذا الوقت غير متاح ",
            ]);

        }
        else{
            Student::create([
                "national_id"=>$request->national_id,

                "name"=>$request->name,

                "mother_name"=>$request->mother_name,
                "last_name"=>$request->last_name,
                "mobile"=>$request->mobile,

                "center_type_id"=>$request->center_type_id,

            ]);
           // $stu_id = Student::latest()->first()->id;

            StudentAppointment::create([
                //'stu_id' => $stu_id,
                "student_id"=>$request->national_id,

                "center_id"=>$request->center_id,
                "date"=>date("Y-m-d",strtotime($app_date) ),
                "time"=>$request->hour.":".$request->minute,

            ]);


            return response()->json([
                "status"=>200,
                "message"=>"تم حجز الموعد بنجاح",
            ]);
        }
        $data =[
            'name' => $request->name,
            'last_name' => $request->last_name,
            'mother_name' => $request->mother_name,
            'national_id' => $request->national_id,
            'center_type_id' => $type_id,

        ];
        Student::updateOrCreate(['national_id'=>$request->national_id], $data);
        $appoint = [
            "student_id"=>$request->national_id,
            "center_id"=>$center_id,
            "date"=>date("Y-m-d",strtotime($request->appointment_date)),
            "time"=> $request->hour.":".$request->minute,
        ];

        // if(count($center_emps) * $service_time){

        // }
        return response()->json([

        ]);
        return $app_date;

    }




}
