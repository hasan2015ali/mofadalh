<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\allTrait;
use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use App\Models\User;
use Validator;
use DataTables;
class SliderController extends Controller
{
    //
    use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        //
        $data = Slider::with('product')->get();
        if ($request->ajax()) {
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row){

//                    $btn = '<a href="javascript:void(0)" class="show btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-eye"></i> </a> &nbsp;';
                    $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';


                    return $btn;

                })
                    ->addColumn('image',function ($row){


                        return "<img src='".url('public/storage/'.$row->image)."' width='50' height='50'>";

                    })




                ->rawColumns(['action','image',])

                ->make(true);
        }

//        $main_cats = Category::with('childs')->where('parent_id','=',0)->get();
        $products = Product::get();


        return view('admin.slider' , compact('products'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        if(empty($request->_id)) {

        $validateErrors = Validator::make($request->all(),
            [
                'offer_image' => 'required',

            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .
//        }

        $data =[
            'product_id' => $request->product_id,

        ];

        if(!empty($request->offer_image)){
            $data['image'] = $request->offer_image;
        }

        $id =  Slider::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تمت الإضافة بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        return  $this->editController($id,Slider::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $this->destroyController($id, Slider::class);
        return response()->json(['success'=>' تم الحذف بنجاح ']);
    }

    public function deleteSlider($id){

      $this->destroyController($id, Slider::class);
        return response()->json(['success'=>' تم الحذف بنجاح ']);
    }
}
