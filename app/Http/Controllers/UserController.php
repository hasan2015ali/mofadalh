<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\allTrait;
use Validator;
use Auth;
use App\Models\User;
use DataTables;

class UserController extends Controller
{use allTrait;
    //


    public function login(Request $request){
          $request->validate(
            [
                'email'=>'required|email|min:3|max:190',
                'password'=>'required|string|min:6|max:20',
            ]);

//        return  $request->all();
        if (Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            // return json_encode($user)

            return redirect("/home");

        } // end ! is null.
        else{

            return redirect("/login")->withErrors(["message"=>"حطأ في بيانات الدخول"]);

        } // end else for ! is null.

    }
    public function showProfile(Request $request){
        $user = Auth()->user();
        return view("admin.profile", compact('user'));
    }
    public function updateProfile(Request $request){

        $user = Auth()->user();
        $check = User::where([['id','!=',$user->id],['email','=',$request->email]])->count();
        if($check > 0){
            return response()->json(['status'=>201]);
        }


        $user->name = $request->input("name");
        $user->email = $request->input("email");
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }
        $user->save();
        return response()->json(['status'=>200]);


    }
    public function logout(Request $request){

        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }

    public function index(Request $request)
    {
        //
        if ($request->ajax()) {

            $data = User::where([['role','=',2],['status','=',0]])->latest()->get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action', function($row){

                    $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> </a>';

                    $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="accept" class="btn btn-success btn-sm accept"> <i class="fa fa-check-circle"></i> قبول </a>';
                    return $btn;

                    })->addColumn('avatar',function ($row){

                        return "<img src='".asset('storage/users/uploads/'.$row->avatar)."' width='50' height='50'>";

                        })

                ->rawColumns(['action','avatar'])

                ->make(true);

            return;
        }


        return view("admin.users_requests") ;

    }

    public function users(Request $request)
    {
        //
        if ($request->ajax()) {

            $data = User::with('coupons')->where([['role','=',2],['status','=',1]])->latest()->get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action', function($row){


                    $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> </a>';


                    return $btn;


                  })->addColumn('avatar',function ($row){

                      return "<img src='".asset('storage/users/uploads/'.$row->avatar)."' width='50' height='50'>";
                      })
                    ->addColumn('coupons', function($row)
                    {
                        $coupoun= count($row->coupons);
                        return $coupoun;
                    })

                ->rawColumns(['action','avatar','coupons'])

                ->make(true);

            return;
        }

        return view("admin.users") ;

    }

    public function agree( $id)
    {
        User::find($id)->update(['status'=>1]);

        return response()->json(['success'=>' تم  قبول المستخدم بنجاح ']);


    }

    public function destroy($id)
    {
        $this->destroyController($id,User::class);

    }

}
