<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\allTrait;
use App\Models\DeliverRegion;
use DataTables;
use Validator;

class DeliverRegionController extends Controller
{  use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:deliver_region-list|deliver_region-create|deliver_region-edit|deliver_region-delete', ['only' => ['index','show']]);
        $this->middleware('permission:deliver_region-create', ['only' => ['create','store']]);
        $this->middleware('permission:deliver_region-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:deliver_region-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        if ($request->ajax()) {

            $data = DeliverRegion::get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action', function($row) use ($user){
                    $btn = '';
                    if ($user->can('deliver_region-edit')){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct"> <i class="fa fa-edit"></i> </a>';
                    }
                    if($user->can('deliver_region-delete')) {
                        $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"> <i class="fa fa-trash-o"></i> </a>';
                    }

                    return $btn;

                })



                ->rawColumns(['action'])

                ->make(true);

            return;
        }

        return view ("admin.deliverRegion");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'regionName' => 'required|string|min:3',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'regionName' => $request->regionName,
            'deliverPrice' => $request->deliverPrice,

        ];

        $id =  DeliverRegion::updateOrCreate(['id' => $request->_id], $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return  $this->editController($id,DeliverRegion::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DeliverRegion::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,DeliverRegion::class);
    }
}
