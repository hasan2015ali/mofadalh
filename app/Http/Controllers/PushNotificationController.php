<?php

namespace App\Http\Controllers;
use App\Http\Traits\allTrait;
use Kreait\Firebase\Messaging\CloudMessage;

use App\Models\UserNotificationModel;
use DataTables;
use Validator;

use Illuminate\Http\Request;


class PushNotificationController extends Controller
{
    use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        if ($request->ajax()) {

            $data = UserNotificationModel::get()->sortDesc();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action', function($row){



                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct"> <i class="fa fa-edit"></i> </a>';


                    $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"> <i class="fa fa-trash-o"></i> </a>';

                    return $btn;

                })

                ->addColumn('details', function($row){

                    if(strlen($row->details)>30){

                   return mb_substr($row->details, 0, 30) ."....";}

                   else{
                    return $row->details;
                   }

                })


                ->rawColumns(['action','details'])

                ->make(true);

            return;
        }


        return view ("admin.notifications");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'details' => 'required|string|min:3',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,
            'details' => $request->details,
        ];

        $id =  UserNotificationModel::updateOrCreate(['id' => $request->_id],
            $data)->id;
            //  sending message.
            $data = ['title' =>  $request->title,  'body' =>  $request->details,  'click_action'=>'FLUTTER_NOTIFICATION_CLICK', 'type'=>1];
            //        $pusher->trigger('chat', 'message-recived_'.$request->input('reciver_id'), $data);

                                $messaging = app('firebase.messaging');
                                $message1 = CloudMessage::fromArray([
                                    'topic' => "messageRecived",
                                    'notification' => $data, // optional
                                    'data' => ["message"=>$request->input('details'),   "created_at"=>date("Y-m-d H:i:s"),  'type'=>3, 'click_action'=>'FLUTTER_NOTIFICATION_CLICK'], // optional
                                ]);

                                $messaging->send($message1);

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserNotificationModel  $UserNotificationModel
     * @return \Illuminate\Http\Response
     */
    public function show(UserNotificationModel $UserNotificationModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserNotificationModel  $UserNotificationModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return  $this->editController($id,UserNotificationModel::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserNotificationModel  $UserNotificationModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = UserNotificationModel::find($id);
        $validateErrors = Validator::make($request->all(),
        [
            'details' => 'required|string|min:3',


        ]);
    if ($validateErrors->fails()) {
        return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
    } // end if fails .
        $item->update($request->all());
        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserNotificationModel  $UserNotificationModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $this->destroyController($id,UserNotificationModel::class);
    }

}
