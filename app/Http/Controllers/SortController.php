<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class SortController extends Controller
{
    //
    public function index($table){
        $view = "home";
        switch ($table){
            case "categories":
                $view = "admin.sort.categories";
                $data = Category::where('parent_id','0')->orderBy("order_id")->get();
                break;
        }
        return view($view , compact('data'));
    }

    public function store(Request $request, $table){
        switch ($table){
            case "categories":
                $items = $request->input("sort_order");

                $i=0;
                foreach ($items as $item){
                    Category::where("id","=",$item)->update(["order_id"=>$i]);
                    $i++;
                }

                break;
                case "sub_categories":
                $items = $request->input("sort_order");
                $names = $request->input("cat_name");

                $i=0;
                foreach ($items as $item){
                    Category::where("id","=",$item)->update(["order_id"=>$i,"name"=>$names[i]]);
                    $i++;
                }

                break;

                case "prices":
                    $items = $request->input("sort_order");
                    $name = $request->input("name");
                    $price = $request->input("price");

                    $i=0;
                    foreach ($items as $item){
                        Prices::where("id","=",$item)->update(["order_id"=>$i]);
                        $i++;
                    }

                    break;
        }
        return response()->json(['status'=>200]);
    }

}
