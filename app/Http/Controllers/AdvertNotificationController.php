<?php

namespace App\Http\Controllers;

use App\Http\Traits\allTrait;
use App\Models\AdverNotification;
use Illuminate\Http\Request;
use DataTables;
use Validator;

class AdvertNotificationController extends Controller
{
    use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        if ($request->ajax()) {

            $data = AdverNotification::get();

            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm edit"> <i class="fa fa-edit"></i> </a>';

                    $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash-o"></i> </a>';
                    $btn .= '<a href="'.url('translations/adv_notif', $row->id).'" data-toggle="tooltip" class="info btn btn-secondary btn-sm "  data-id="'.$row->id.'" style="color: white !important;"> <i class="fa fa-language"></i>  الترجمة  </a> &nbsp; ';

                    return $btn;

                })

                ->rawColumns(['action'])

                ->make(true);

            return;
        }


        return view ("admin.advert_notifications");
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [

                    'title' => 'required|string',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,

            'expiry' => $request->expiry,

        ];

       AdverNotification::updateOrCreate(['id' => $request->_id], $data);

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $item = AdverNotification::find($id);
        return response()->json($item);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [

                    'title' => 'required|string',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,

            'expiry' => $request->expiry,

        ];

       AdverNotification::updateOrCreate(['id' => $request->_id], $data);

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,AdverNotification::class);
    }
}
