<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use App\Models\Help;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Inbox;

class InboxController extends Controller
{
    //
    public function index(){
        $data = Inbox::with('translations')->orderby('id','desc')->paginate(20);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
    public function get_help(){
        $data = Help::
        with(array('translations' => function($query) {
            $query->select('title','lang','item_id');
        }))->orderby('title')->get();
        if (count($data)>0){
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
    public function get_contact(){
        $data = Contact::orderby('numberStatus')->get();
        if (count($data)>0){
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
}
