<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryTranslation;
use App\Models\Coin;
use App\Models\Country;

use App\Models\DeliverRegion;
use App\Models\DeliveryTime;
use App\Models\Product;
use App\Models\Slider;
use App\Models\UserNotificationModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function getRegions(){
        $data = Country::where('parent_id','>',0)->get(['id',"name"]);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public function getCoins(){
        $data = Coin::get(['price',"name"]);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public function get_region_delivery(){
        $data = DeliverRegion::orderBy('id')->get([ 'id','regionName','deliverPrice']);
        if (count($data)>0){
            return response()->json([
                "status"=>200,
                "message"=>$data
            ]);
        }
        else{
            return response()->json([
                "status"=>404,
                "message"=> "NOt Fount"
            ]);
        }
    }
    public function get_delivery_times(){
        $data = DeliveryTime::orderBy('id')->get(['name','from_time','to_time']);
        if (count($data)>0){
            return response()->json([
                "status"=>200,
                "message"=>$data
            ]);
        }
        else{
            return response()->json([
                "status"=>404,
                "message"=> "NOt Fount"
            ]);
        }
    }

    public function getNotification(){
        $data = UserNotificationModel::get();

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
    public function categories($lang = "ar"){
        // if($lang == "ar"){
            $data = Category::with(array('childs' => function($query) {
                $query->select('name','icon','parent_id');
            }))
            ->with(array('translations' => function($query) {
                    $query->select('name','lang','cat_id');
                }))

            ->where('parent_id','0')->orderBy('order_id')->get(['id', "name",
            "icon",
            "order_id",
            "parent_id"]);
            $last_products = Product::with(array('prices' => function($query) {
                $query->select('name','price','product_id');
            }))
            ->with(array('translations' => function($query) {
                $query->select('name','lang','item_id');
            }))

                ->orderBy('id','desc')->limit(10)->get();
            // $slider = Product::with(array('translations' => function($query) {
            //     $query->select('name','lang','item_id');
            // }))->where('in_slider','=',1)->orderBy('id','desc')->limit(3)->get();
            $slider = Slider::with(array('product' => function($query) {
                     $query->select('name',"id");
                 }))->orderBy('id','desc')->get();
        // }
        // else{
        //     $data = new Category("en");
        //     return $data->all();
        //     $data = $data->

        //     with(array('childs' => function($query) {
        //         $query->select('name','icon','parent_id');
        //     }))
        //    ->with(array('category' => function($query) {
        //         $query->select('name','icon','parent_id');
        //     }))

        //     ->orderBy('order_id')->get(['id', "name",
        //     "icon",
        //     "order_id",
        //     "parent_id"]);
        //     $last_products = Product::with(array('prices' => function($query) {
        //         $query->select('name','price','product_id');
        //     }))

        //         ->orderBy('id','desc')->limit(10)->get();
        //     // $slider = Product::with(array('translations' => function($query) {
        //     //     $query->select('name','lang','item_id');
        //     // }))->where('in_slider','=',1)->orderBy('id','desc')->limit(3)->get();
        //     $slider = Slider::with(array('product' => function($query) {
        //              $query->select('name',"id");
        //          }))->orderBy('id','desc')->get();
        // }


        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
                "last_products"=>$last_products,
                "slider"=>$slider
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
    public function categoriesAll(){
        $data = Category::with(array('childs' => function($query) {
            $query->select('name','icon','parent_id');
        }))
        ->with(array('translations' => function($query) {
            $query->select('name','lang','cat_id');
        }))
            ->where('parent_id','0')->orderBy('order_id')->get(['id', "name",
            "icon",

            "order_id",
            "parent_id"]);


        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data

            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
    public function getCategoryChild($id){
        $data = Category::
        with(array('translations' => function($query) {
            $query->select('name','lang','cat_id');
        }))
        ->where('parent_id','=',$id)->orderBy('order_id')->get(['id', "name",
            "icon",
            "order_id",
            "parent_id"]);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
}
