<?php

namespace App\Http\Controllers\Api;

use App\Models\DeliverRegion;
use App\Models\UserOrder;
use Illuminate\Http\Request;
use App\Models\UserOrderDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Messaging\CloudMessage;
use Validator;

class OrderController extends Controller
{

    //**  Request order Function */

    public function setOrder(Request $request)
    {
        $data = json_decode($request->getContent());
        $order = UserOrder::create(
            [
                'user_id' => $data->user_id,
                'total' => $data->total,
                'region_address' => $data->region_address,
                'loc_x'=> $data->loc_x,
                'loc_y'=> $data->loc_y,
                'details'=> $data->details,
                'deliver_type'=>$data->deliver_type,
                'city'=>$data->city,
                'street'=>$data->street,
                'zip_code'=>$data->zip_code,

                // وقت التوصيل
                'delivery_at'=>$data->delivery_at,
                // في حال كان بده توصيل بنحدد المنطقة كمان و ساعة النهاية و التاريخ
                'deliver_date'=>$data->deliver_date,
                'deliver_end_time'=>$data->deliver_end_time,
                'region_id'=>$data->region_id ,
                // coupn if used
                'coupon'=>$data->coupon,
            ])->id;
        foreach($data->products as $product){
            UserOrderDetail::create(
                [
                    "order_id"=>$order,
                    "product_id"=>$product->product_id,
                    "amount"=>$product->amount,
                    "type_amount"=>$product->type_amount,
                    "single_price"=>$product->single_price,
                    "full_price"=>$product->single_price * $product->amount,
                ]);
        }

        $title = 'إشعار جديد';
        $body = 'تم تسجيل طلبية جديدة';
        $notifi = ['title' =>  $title,  'body' =>  $body,  'click_action'=>url('user-carts-requests')];
        $messaging = app('firebase.messaging');
        $message1 = CloudMessage::fromArray([
            'topic' => "web",
            'notification' => $notifi, // optional
            'data' => ["message"=>$body, 'click_action'=>'FLUTTER_NOTIFICATION_CLICK'], // optional
        ]);

        $messaging->send($message1);

        return response()->json([
            "status"=>200,
            "message"=>$order,
        ]);


    }
    public function editOrder(Request $request)
    {
        $data = json_decode($request->getContent());

        $validator = Validator::make($request ->all(),[
            'user_id'=>'required',
            'id'=>'required'
        ]);

        if($validator -> fails()){
            return response()->json([
                "status"=>201,
                "message"=>$validator->errors()->first()
            ]);
        }
        $id = $request->input("id");

        $set=[['id','=',$id],['status','=',0]];

        $order = UserOrder::where($set)->update(
            [
                'user_id' => $request->user_id,
                'total' => $request->total,
                'products'=> $request->products,
            ]);
        UserOrderDetail::where('order_id','=',$id)->delete();
        foreach($data->products as $product){
            UserOrderDetail::create(
                [
                    "order_id"=>$id,
                    "product_id"=>$product->id,
                    "amount"=>$product->amount,
                    "type_amount"=>$product->type_amount,
                    "single_price"=>$product->single_price,
                    "full_price"=>$product->single_price * $product->amount,
                ]);
        }

        return response()->json([
            "status"=>200,
            "message"=>$id,
        ]);
    }

    public function getOrderNotFinished(Request $request)
    {
        $id = $request->input("id");

        $data = DB::select("select user_orders.*  from user_orders
        where user_orders.status='0' and user_orders.user_id= '".$id."'");

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }


    public function getOrderFinished(Request $request)
    {
        $id = $request->input("id");

        $data = DB::select("select user_orders.*  from user_orders
        where user_orders.status='1' and user_orders.user_id= '".$id."'");
        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public  function getOrderDetails(Request $request ){

        $id = $request->input("id");

        $order = DB::select('select user_orders.* , users.name as user_name from user_orders
                inner join users on user_orders.user_id=users.id where user_orders.id = ?', [$id]);

        $details = DB::select("SELECT
                 user_order_details.product_id as product_id , user_order_details.amount as prouduct_amount ,
                 user_order_details.type_amount as type_amount,
                 user_order_details.full_price as full_price, user_order_details.single_price as single_price,
                 products.name as product_name , products.images as images FROM user_orders inner JOIN user_order_details on
                 user_orders.id =user_order_details.order_id inner join products on
                 user_order_details.product_id=products.id WHERE user_orders.id=? ",[$id]);


        if(count($order) > 0){

            return response()->json([
                'status'=>200,
                'data'=>["order"=>$order[0],"details"=>$details]
            ]);
        }
        else{
            return response()->json([
                'status'=>404,
                'data'=>"Not Found"
            ]);
        }
    }
    public function getUserOrder(Request $request){
        $id = $request->input("id");

      $order = DB::select("select user_orders.* , users.name as user_name from user_orders
          inner join users on user_orders.user_id=users.id where user_orders.user_id='".$id."' limit 20");

        if(count($order) > 0){

            return response()->json([
                'status'=>200,
                'data'=>$order
            ]);
        }
        else{
            return response()->json([
                'status'=>404,
                'data'=>"Not Found"
            ]);
        }
    }
}
