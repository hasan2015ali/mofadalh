<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\ProductStars;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    //get all comment for product
    public function getComment(Request $request){

        $id=$request->input('id');
        $data = Comment::where('product_id','=',$id)->get(['id',
            "user_id",
            "comment"]);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
    ///set comment
    public function setComment(Request $request){
//        $data = json_decode($request->getContent());
        $user_id = $request->input('user_id');
        $product_id = $request->input('product_id');
        $comment = $request->input('comment');
        $data = Comment::create(
            [
                'user_id' => $user_id,
                'product_id' => $product_id,
                'comment' => $comment,
            ]);
        if($data){
            return response()->json([
                "status"=>200,
                "data"=>$data
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>[]
            ]);
        }
    }

    //set rating -user for product
    public function setRate(Request $request){

        $data = json_decode($request->getContent());

        $id = $request->input("user_id");
        $pro = $request->input("product_id");
        $star = $request->input("stars");

        $rate = ProductStars::where('user_id',$id)->get();
        if (count($rate)){
            return response()->json([
                "status"=>404,
                "message"=>"sorry, user_rate is already exist"
            ]);
        }
        $data = ProductStars::create(
            [
                'user_id' => $id,
                'product_id' => $pro,
                'stars' => $star,
            ]);
        if($data){
            return response()->json([
                "status"=>200,
                "data"=>$data
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>[]
            ]);
        }
    }
   //get rating for product
    public function getRate(Request $request){

        $id = $request->input('id');
        $data = ProductStars::where('product_id','=',$id)->get(['id',
            "user_id",
            "stars"]);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }


}
