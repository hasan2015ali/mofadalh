<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserOrderDetail;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

use App\Models\Category;
class ProductController extends Controller
{
    //
    public function getProduct(){

        $data = Product::with(array('prices' => function($query) {
            $query->select('name','price','product_id');
        }))
        ->with(array('translations' => function($query) {
            $query->select('name','lang','item_id');
        }))
        ->with(array('cat' => function($query) {
            $query->select('name','icon');
        }))->limit(20)
            ->get(['id','name','cat_id','images','offer_price','offer_image']);
        // $data = Product::where('status','=','1')->paginate(15);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public function searchProductName(Request $request){

        $name = $request->input("name");
        $data = Product::with(array('prices' => function($query) {
            $query->select('name','price','product_id');
            }))
            ->with(array('translations' => function($query) {
                $query->select('name','lang','item_id');
            }))
            ->with(array('cat' => function($query) {
                $query->select('name','icon','parent_id');
            }))
            ->where('name',$name)->get(['id','name','cat_id','images','offer_price','offer_image','available']);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public function getProductDetails(Request $request){
        $id = $request->input("id");

         $data = Product::with(array('prices' => function($query) {
             $query->select('name','price','product_id');
         }))
         ->with(array('translations' => function($query) {
            $query->select('name','lang','item_id');
        }))
             ->where('id','=',$id)->get(['id','name','cat_id','images','offer_price','offer_image','discount','available']);

        if(count($data) >0)
        {
            $like_pro = Product:: where('id','=',$id)->get('cat_id');

            $like_prod = $like_pro->pluck('cat_id');

            $other_products = Product::with(array('prices' => function($query) {
                $query->select('name','price','product_id');
            }))
            ->with(array('translations' => function($query) {
                $query->select('name','lang','item_id');
            }))
                ->where('cat_id','=',$like_prod)
                ->orderByRaw('RAND()')->take(10)
                ->get(["id","name","images" , 'discount','available']);
            if(count($data) >0){
                // convert images to array of product details.
                // $data[0]["images"] = explode(",",$data[0]->images);

                return response()->json([
                    "status"=>200,
                    "data"=>["prod_details"=>$data[0],"prod_may_like"=>$other_products]
                ]);
            }else{
                return response()->json([
                    "status"=>404,
                    "message"=>"Not Found",
                ]);
            }
        }
        else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found"
            ]);
        }

    }

    public function getCategoryProducts(Request $request){
        $id = $request->input("id");
        $data = Product::with(array('prices' => function($query) {
            $query->select('name','price','product_id');
        }))
        ->with(array('translations' => function($query) {
            $query->select('name','lang','item_id');
        }))
            ->where('cat_id','=',$id)
            ->get(['id',"name",
                "images",
                "cat_id" , 'discount','available']);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
}
//    public function getSuperMarketProducts(Request $request){
//        $market = $request->input("id");
//        $data = Product::where('market_id','=',$market)->get();
////            ['id',"name",
////            "type",
////            "amount",
////            "price",
////            "cat_id",
////            "market_id"]);
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//    public function getMarketProdPrice(Request $request){
//
//        $market = $request->input("id");
//
//        // $data = DB::select("select products.* from products where products.market_id='".$market."'
//        // groupby products.price");
//
//        $data = DB::table('products')
//                 ->select('id','name','price','market_id','cat_id','type')
//                 ->where('market_id','=',$market)
//                 ->orderBy('price')
//                 ->get();
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//
//
//    public function getMarketProdPriceCat(Request $request){
//
//        $market = $request->input("id");
//
//        $data = DB::table('products')
//        ->select('id','name','price','market_id','cat_id','type')
//        ->where('market_id','=',$market)
//        ->orderBy('price','ASC')
//        ->orderBy('cat_id','ASC')
//        ->get();
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//
//
//    public function getProdMaxSale(Request $request){
//
////        $data = DB::select("select user_order_details.*, count('product_id') AS Product
////        from user_order_details groupBy 'cat_id'");
//        $cat = $request->input("id");
//
//        $prod = UserOrderDetail::where('cat_id','=',$cat)->get()->count();
//            // ->withCount('product_id')
//            // ->orderBy('product_id', 'asc')->get();
//
//
//        $data = max($prod);
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//
//
//    public function getSuperMarketProductsCat(Request $request){
//        $market = $request->input("id");
//        $cat = $request->input("cat");
//        $data = DB::select("select products.* from products
//        where products.market_id='".$market."' and  products.cat_id ='".$cat."'");
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//
//    public function getSuperMarketFeatured(Request $request){
//
//
//            $data = DB::select('select users.* , b.name as city_name , e.name as region_name from users
//            inner join countries e on users.region_id=e.id
//            inner join countries b on e.parent_id = b.id where users.role="5" and  users.featured=1');
//
//
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//    public function getRegionSuperMarkets(Request $request){
//
//        $id = $request->input("id");
//        $data = DB::select("select users.* , b.name as city_name , e.name as region_name from users
//            inner join countries e on users.region_id=e.id
//            inner join countries b on e.parent_id = b.id where users.role='5' and  users.region_id='".$id."' ");
//
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//    public function searchSuperMarkets(Request $request){
//
//            $name = $request->input("name");
//            $data = DB::select("select users.* , b.name as city_name , e.name as region_name from users
//            inner join countries e on users.region_id=e.id
//            inner join countries b on e.parent_id = b.id where users.role='5' and  users.name like  '%".$name."%'");
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//    public function searchSuperMarketsNaRe(Request $request){
//
//        $name = $request->input("name");
//        $region = $request->input("id");
//        $data = DB::select("select users.* , b.name as city_name , e.name as region_name from users
//            inner join countries e on users.region_id=e.id
//            inner join countries b on e.parent_id = b.id where users.role='5' and  users.name like  '%".$name."%'
//            and  users.region_id='".$region."' ");
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//    public function getSuperMarkerDetails(Request $request){
//        $id = $request->input("id");
//
//            $data = DB::select("select users.* , b.name as city_name , e.name as region_name from users
//            inner join countries e on users.region_id=e.id
//            inner join countries b on e.parent_id = b.id where users.role='5' and users.id='".$id."'");
//
//
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//
//     //get search product name + category
//     public function searchNameCat(Request $request){
//
//         $name = $request->input("name");
//         $cat = $request->input("cat");
//         $data = DB::select("select products.*  from products
//            where products.name like  '%".$name."%' and  products.cat_id='".$cat."' ");
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data,
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//     //get search product price
//     public function searchProductPrice($price){
//        $data = Product::where('price','=',$price)->get();
//
//        if(count($data) >0){
//            return response()->json([
//                "status"=>200,
//                "data"=>$data->first(),
//            ]);
//        }else{
//            return response()->json([
//                "status"=>404,
//                "message"=>"Not Found",
//            ]);
//        }
//    }
//}
