<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    //
    public function index(){
        $data = Slider::orderby('id','desc')->get();

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }
}
