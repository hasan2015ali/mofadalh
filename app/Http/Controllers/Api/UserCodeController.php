<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\CouponModel;
use App\Models\CouponSetting;
use App\Models\User;
use App\Models\UserOrder;
use App\Models\UserShareCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserCodeController extends Controller
{

    public function getCoupon(){
//      $user_id = $request->input('user_id');
        $data =CouponModel::orderBy('id', 'desc')->get(['name','code','discount','expiry']);

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    //get user coupon
    public function addUserCoupon(Request $request){
        $code_share = $request->input('code_share');
        $user_id = $request->input('user_id');
        $code = UserShareCode::where('code',$code_share)->get();

        if(count($code) == 0 ){
            return response()->json([
                "status"=>404,
                "message"=>"Code Not Found",
            ]);
        }
        // not allowed for user to use him share code.
        if($code->first()->user_id == $user_id){
            return response()->json([
                "status"=>201,
                "message"=>"Code Not Allowed",
            ]);
        }

        $setting = CouponSetting::first();
        $val =$setting->discountRate;
        $exp=$setting->expiry;

        $data = Coupon::create([
            'user_id' => $user_id,
            'val' => $val,
            'end_date' =>date("y/m/d", strtotime ( "+ ".$exp ." days" )),
            'used' => 0,
            'share_code'=>$code_share
            ]);
        if($data){
            return response()->json([
                "status"=>200,
                "data"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public function check_coupon(Request $request){
        $user_id =$request->input('user_id');
        $coupon =$request->input('coupon');


        $check_code = Coupon::where([['share_code',$coupon],['user_id',$user_id]])->get();

//        $expiry = Coupon::where([['share_code',$coupon],['user_id',$user_id]])->
//        where('end_date', '>', Carbon::now())->get();

//        $date = Carbon::now();
//
//        $check_code2 = $expiry > $date;


        if(count($check_code) > 0){

            $end_date =Coupon::whereDate('end_date','>',Carbon::now())->get();

           if(count($end_date) == 0) {
               return response()->json([
                   "status" => 419,
                   "message" => "Sorry! Coupon has expired"
               ]);

           }
           else{
               $coupon_user_order = UserOrder::where([['coupon', $coupon], ['user_id', $user_id]])->get();
               if (count($coupon_user_order) > 0) {
                   return response()->json([
                       "status" => 201,
                       "message" => "Sorry! Coupon Already Used",
                   ]);
               } else {
                   return response()->json([
                       "status" => 200,
                       "message" => "You Can Use This Code Successfully!" ,
                       "coupon"=>$coupon
                   ]);
               }

           }

        }
        else{
            return response()->json([
                "status"=>404,
                "message"=>"Coupon Code Not Found",
            ]);
        }

    }
    //get user share code
    public function generateShareCode(Request $request){

        $mob = $request->input('phone');
        $id = $request->input('id');
        $mobile = User::where('id',$id)->Where('phone', $mob)->get();

        if(count($mobile) <= 0){
            return response()->json([
                "status"=>401,
                "message"=>"invalid phone"
                ]);
        }
        $code = UserShareCode::where('user_id',$id)->first();
        if ($code)
        {
            $old_code =$code->code;
            return response()->json([
                "status"=>201,
                "message"=>$old_code
            ]);
        }
        $share_code1 =Str::random(8);
        $share_code = $share_code1.$mob;
        $share_code =strtoupper($share_code);
            $data = UserShareCode::create(
                [
                    'user_id' => $id,
                    'code' => $share_code
                ]);
            if($data){
                return response()->json([
                    "status"=>200,
                    "data"=>$data
                ]);
            }else{
                return response()->json([
                    "status"=>404,
                    "message"=>[]
                ]);
            }
    }

}
