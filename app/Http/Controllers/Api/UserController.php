<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /// Register Function

    public function UserRegister(Request $request){

        $validator = Validator::make($request ->all(),[
            'name'=>'required|min:2|string',
            'password'=>'required|min:8 ',
            'phone'=>'required|unique:users',
            'address'=>'required|min:3'
        ]);

        if($validator -> fails()){
            return response()->json([
                "status"=>201,
                "message"=>$validator->errors()->first()
            ]);
        }else{
            if(!empty($request->email)){
                $check = User::where('email','=',$request->email)->get();
                if(count($check)>0){
                    return response()->json([
                        "status"=>201,
                        "message"=>"البريد المدخل مسجل مسبقا"
                    ]);
                }

            }
            $user_data = [
                'name'=>$request->name,
                'phone'=>$request->phone,
                'password'=>Hash::make($request->password),
                'address'=>$request->address,
                'block'=>$request->block,

                'role'=> 2,
                'status'=> 0,
                'region_id'=> $request->region_id,
                'lat'=>$request->lat,
                'lng'=>$request->lng,
            ];
            if(!empty($request->email)){
                $user_data['email']= $request->email;

            }
            if(!empty($request->input("image"))){
                $image= base64_decode($request->image);

                $image = str_replace('data:image/jpeg;base64,', '', $image);
                $imageName = 'pharma_'.time().'.'."png";
                Storage::disk('public')->put('/pharma/' . $imageName , $image);


                $user_data["image"] = "market/". $imageName;

            }
            if(!empty($request->input("avatar"))){
                $image= base64_decode($request->avatar);

                $image = str_replace('data:image/jpeg;base64,', '', $image);
                $imageName = 'pharma_av_'.time().'.'."png";
                Storage::disk('public')->put('/market/ava/' . $imageName , $image);


                $user_data["avatar"] = "market/". $imageName;


            }

            $data = User::create(
        $user_data
            );
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }
    }
    public function updateData(Request $request , $id){

        $validator = Validator::make($request ->all(),[
            'name'=>'required|min:2|string',
            'phone'=>'required',

            'address'=>'required|min:3'
        ]);

        if($validator -> fails()){
            return response()->json([
                "status"=>201,
                "message"=>$validator->errors()->first()
            ]);
        }else{
            if(!empty($request->old_password) && !empty($request->password) ){
                $user = User::find($id);
                if(!Hash::check($request->input('old_password'), $user->first()->password)) {
                    return response()->json([
                        "status"=>201,
                        "message"=>"كلمة السر القديمة غير صحيحة"
                    ]);
                    }
                }


            $data = [

                'name'=>$request->name,

                'phone'=>$request->phone,
                'email'=>$request->email,

                'address'=>$request->address,
                'lat'=>$request->lat,
                'lng'=>$request->lng,
                'block'=>$request->block,
                'region_id'=>$request->region_id,


            ];
            if(!empty($request->input("password"))){

                $data['password'] = Hash::make($request->password);
            }   if(!empty($request->input("email"))){
                $data['email'] = $request->email;
            }
             User::where('id','=',$id)->update(
            $data
            );


            return response()->json([
                "status"=>200,
                "data"=>User::find($id),
                "message"=>"تم التعديل بنجاح"

            ]);
        }
    }


    /// Login Function

    public function UserLogin(Request $request){
        $user = User::where("phone","=",$request->input("phone"))->orWhere('email','=',$request->input("phone"))->get();
        $validator = Validator::make($request ->all(),[
            'phone'=>'required |min:10|max:15',
            'password'=>'required|min:8',

        ]);
        if($validator->fails()){
            return response()->json([
                "status"=>201,
                "message"=>$validator->errors()->first(),
            ]);
        }else{
            if(count($user)>0){
                // if(Hash::check($request->input('password'), $user->first()->password)){
                    if($request->input('password')){
                //  if( intval( $user->first()->status)==1  ){
                       return response()->json([
                           "status"=>200,
                           "message"=>$user->first(),
                       ]);
                //    }
                //    else{
                //        return response()->json([
                //            "status"=>201,
                //            "message"=>"الرجاء التواصل مع الادارة لتفعيل الحساب",
                //        ]);
                //    }

                }else{
                    return response()->json([
                        "status"=>201,
                        "message"=>"password error",
                    ]);
                }
            }else{
                return response()->json([
                    "status"=>404,
                    "message"=>"Not Found",
                ]);
            }
        }
    }
}
