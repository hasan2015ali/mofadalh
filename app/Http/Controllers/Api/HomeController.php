<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AdverNotification;
use App\Models\CenterEmp;
use App\Models\CenterType;
use App\Models\Color;
use App\Models\Student;
use App\Models\StudentAppointment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function getCenters(Request  $request){
        if( empty($request->app_key) || $this->appKey != $request->app_key){
            return response()->json([
                "status"=>400,
                "message"=>"Bad Access",
            ]);
        }
        $data = CenterType::with('centers')->get();

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    } public function getStudentAppointments(Request  $request){
        if( empty($request->app_key) || $this->appKey != $request->app_key){
            return response()->json([
                "status"=>400,
                "message"=>"Bad Access",
            ]);
        }
        $data = StudentAppointment::with('student')->where('student_id',$request->national_id)->get();

        if(count($data) >0){
            return response()->json([
                "status"=>200,
                "message"=>$data,
            ]);
        }else{
            return response()->json([
                "status"=>404,
                "message"=>"Not Found",
            ]);
        }
    }

    public function getAppointment(Request  $request){
        if( empty($request->app_key) || $this->appKey != $request->app_key){
            return response()->json([
                "status"=>400,
                "message"=>"Bad Access",
            ]);
        }
        $type_id = $request->type_id;
        $center_id = $request->center_id;
        $app_date = date("Y-m-d",strtotime($request->appointment_date));
        if(date("D" , strtotime($app_date)) == "Fri"){
            return response()->json([
                "status"=>201,
                "message"=> "لا يمكنك الحجز يوم الجمعه" ,
            ]);

        }
        $appointment_time = $request->hour.":".$request->minute;
        // get center emps available count
        $center_emps = CenterEmp::where([['status',1],['center_id',$center_id]])->get();
        if(count($center_emps) <=0){
            return response()->json([
                "status"=>201,
                "message"=> "المركز مغلق حاليا" ,
            ]);

        }
        // set service time for each emps as 10 minutes
        // show to user how much time to wait only ?
        $service_time = 10;
        $date_time_appointments = StudentAppointment::where([['date',$app_date],["time",$appointment_time]])->get();

        if(count($date_time_appointments) >= count($center_emps)){
            return response()->json([
                "status"=>201,
                "message"=>"هذا الوقت غير متاح ",
            ]);

        }
        else{
            $data =[
                'name' => $request->name,
                'last_name' => $request->last_name,
                'mother_name' => $request->mother_name,
                'national_id' => $request->national_id,
                'center_type_id' => $type_id,

            ];
            Student::updateOrCreate(['national_id'=>$request->national_id], $data);
            $appoint = [
                "student_id"=>$request->national_id,
                "center_id"=>$center_id,
                "date"=>date("Y-m-d",strtotime($request->appointment_date)),
                "time"=> $request->hour.":".$request->minute,
            ];

            StudentAppointment::updateOrCreate(['student_id'=>$request->national_id],$appoint);
            return response()->json([
                "status"=>200,
                "message"=>"تم حجز الموعد بنجاح",
            ]);
        }

        return response()->json([
            "status"=>201,
            "message"=>" يرجى المحاولة لاحقا      ",
        ]);


    }
}
