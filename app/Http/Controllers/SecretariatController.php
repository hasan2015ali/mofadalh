<?php

namespace App\Http\Controllers;
use App\Http\Traits\allTrait;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Validator;
use DataTables;

class SecretariatController extends Controller
{use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request  $request)
    {

        if ($request->ajax()) {
            $data = User::where([['role','=','4'],['status','=',1]])->get();
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row){



//                    $btn = '<a href="javascript:void(0)" class="show btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-eye"></i> </a> &nbsp;';

                    $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="'.$row->id.'"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';




                    return $btn;

                })

                ->rawColumns(['action'])

                ->make(true);
        }
        return view('admin.secretariat');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        if(empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'name' => 'required|string|min:3',
                    'phone' => 'required|digits:10',
                    'email' => 'required|email|min:3|max:255|unique:users',
                    'password' => 'required|string|min:6|max:8',

                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }
        $data =[
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'email' => $request->email,
            'role' => 4,
            'status' => 1,
            'password' => Hash::make( $request->password),

        ];


        $id =  User::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = User::find($id);

        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       

        return  $this->editController($id,User::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateErrors = Validator::make($request->all(),
            [
                'name' => 'required|string|min:3',
                'phone' => 'required|digits:10',
                'email' => 'required|email|min:3|max:255|unique:users,email,'.$id

            ]);

        if(!empty($request->password)){
            $validateErrors = Validator::make($request->all(),
                [
                    'password' => 'string|min:6|max:8',
                ]);
        }

        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .

        $data =[
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'email' => $request->email,


        ];
        if(!empty($request->password)){

            $data['password'] = Hash::make( $request->password);
        }

        User::updateOrCreate(['id' => $request->_id],
            $data);


        return response()->json(['status'=>200,'message' => 'تم حفظ البيانات بنجاح.' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,User::class);
    }
}
