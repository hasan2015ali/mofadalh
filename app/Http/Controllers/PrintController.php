<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PDF;

class PrintController extends Controller
{
    //

    public function printInvoice( $id, $region = null){

        if($region == 0){
            $order = DB::select('select user_orders.* , users.name as full_name from user_orders
        inner join users on user_orders.user_id=users.id where user_orders.id = ?', [$id]);

        }else{
            $order = DB::select('select user_orders.* , users.name as full_name, 
        deliver_regions.deliverPrice as deliv_coast, deliver_regions.regionName as region from user_orders
        inner join deliver_regions on user_orders.region_id=deliver_regions.id 
        inner join users on user_orders.user_id=users.id where user_orders.id = ?', [$id]);

        }
        $products = DB::select("SELECT 
         user_order_details.product_id as product_id , user_order_details.amount as p_amount ,
         user_order_details.single_price as pr_price, user_order_details.full_price as pr_full_price,
         user_order_details.type_amount as type_a,
         products.name as product_name, products.images as image FROM user_orders 
         inner JOIN user_order_details on user_orders.id =user_order_details.order_id 
         inner join products on user_order_details.product_id=products.id 
         WHERE user_orders.id=? ",[$id]);

//        $output = '';

        if(count($order) > 0){

            $data =["order"=>$order[0],"products"=>$products];
            view()->share('data',$data);

            $pdf = PDF::loadView('admin.invoice',$data);
            $pdf_name =  rand(2,15);
            return $pdf->download($pdf_name.'.pdf');


        }
        else{
            return response()->json([
                'status'=>404,
                'data'=>[]
            ]);
        }
    }
}
