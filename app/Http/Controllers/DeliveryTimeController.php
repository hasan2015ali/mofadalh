<?php

namespace App\Http\Controllers;

use App\Models\DeliveryTime;
use Illuminate\Http\Request;
use DataTables;
use Validator;

class DeliveryTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:deliver_time-list|deliver_time-create|deliver_time-edit|deliver_time-delete', ['only' => ['index','show']]);
        $this->middleware('permission:deliver_time-create', ['only' => ['create','store']]);
        $this->middleware('permission:deliver_time-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:deliver_time-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        //
        $data = DeliveryTime::all();

        if ($request->ajax()) {
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row) use ($user){
                    $btn = '';
                    if ($user->can('deliver_time-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="' . $row->id . '"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }
                    if($user->can('deliver_time-delete')) {
                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="' . $row->id . '"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
                    }

                    return $btn;

                })->rawColumns(['action'])

                ->make(true);
        }

        return view('admin.delivery_times');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validateErrors = Validator::make($request->all(),
            [
                'name' => 'required|string|min:3',
                'to_time' => 'required|string|min:3',
                'from_time' => 'required|string|min:3',

            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .


        $data =[
            'name' => $request->name,
            'to_time' => $request->to_time,
            'from_time' => $request->from_time
        ];

        DeliveryTime::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تمت الإضافة بنجاح.' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = DeliveryTime::find($id);

        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item = DeliveryTime::find($id);
        if($item){
            $item->delete();
        }
        return response()->json(['status'=>200,'message'=>' تم الحذف بنجاح.']);
    }
}
