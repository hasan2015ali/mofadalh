<?php

namespace App\Http\Controllers;

use App\Models\Center;
use App\Models\CenterEmp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\UserOrder;
use App\Models\DeliverRegion;
use App\Models\StudentAppointment;
use App\Models\UserOrderDetail;
use Auth;


class HomeController extends Controller
{
    //
    public function index(){

        $products = Product::all()->count();
        $newOrders = UserOrder::where('status',0)->count();
        $allOrders = UserOrder::where('status','!=',0)->count();
        $accepted_users = StudentAppointment::count();
        $regions = DeliverRegion::all()->count();
        $not_available_products = Product::where('available','=',0)->count();
        $orders_delivered = UserOrder::where('status','=',4)->count();
        $orders_will_deliver = UserOrder::where('status','=',3)->count();
        $users = User::where('status',0)->count();
        $today_products = UserOrderDetail::whereDate('created_at', Carbon::today())->sum('amount');
        $earned_money = UserOrder::whereDate('created_at', Carbon::today())->sum('total');

        return view("admin.home", compact('newOrders','allOrders','accepted_users','products','regions','not_available_products','orders_delivered','orders_will_deliver','users','today_products','earned_money'));
    }
    public  function studentHome(){

        $centers = Center::count();
        $emps = CenterEmp::count();
        return view('home', compact('centers','emps'));
    }

    public  function viewNewOrders(Request  $request){
        $offers = UserOrder::where('status',0)->get()->sortByDesc('id');
        $status = ['0'=>'قيد المراجعة ', '1'=> 'تم القبول', '2'=>'جاري التحضير', '3'=>'جاري التوصيل','4'=>'تم التوصيل'];
        return view('admin.purchases.purchases' , compact('offers','status'));
    }

    public  function viewAllOrders(Request  $request){
        $offers = UserOrder::where('status','!=',0)->get()->sortByDesc('id');
        $status = ['0'=>'قيد المراجعة ', '1'=> 'تم القبول', '2'=>'جاري التحضير', '3'=>'جاري التوصيل','4'=>'تم التوصيل'];
        return view('admin.purchases.purchases' , compact('offers','status'));
    }


}
