<?php

namespace App\Http\Controllers;

use App\Http\Traits\allTrait;
use App\Models\AdverNotification;
use App\Models\Category;

use App\Models\Language;
use App\Models\Product;
use App\Models\AdvertNotificationTranslation;
use Illuminate\Http\Request;
use DataTables;
use Validator;

class AdvNotificationsTranslationController extends Controller
{
    use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request , $id)
    {
        if ($request->ajax()) {

            $data = AdvertNotificationTranslation::where('item_id',$id)->get();

            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm edit"> <i class="fa fa-edit"></i> </a>';

                    $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash-o"></i> </a>';

                    return $btn;

                })

                ->rawColumns(['action'])

                ->make(true);

            return;
        }

        $langs = Language::where('code','!=','ar')->get();
        $item_to_trans = AdverNotification::find($id);
        return view ("admin.translations.advert_notification" , compact('langs', 'item_to_trans'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $id)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [

                    'title' => 'required|string',

                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,

            'lang' => $request->lang,
            'item_id' => $id,

        ];

       AdvertNotificationTranslation::updateOrCreate(['id' => $request->_id], $data);

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id , $item_id)
    {   $item = AdvertNotificationTranslation::find($item_id);
        return response()->json($item);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [

                    'title' => 'required|string',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,

            'expiry' => $request->expiry,

        ];

        AdvertNotificationTranslation::updateOrCreate(['id' => $request->_id], $data);

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id , $item_id)
    {
        $this->destroyController($item_id,AdvertNotificationTranslation::class);
    }
}
