<?php

namespace App\Http\Controllers;


use App\Models\CenterType;
use App\Models\Product;
use Illuminate\Http\Request;

use App\Models\Category;

use Validator;
use DataTables;
use App\Http\Traits\allTrait;


class CategoryController extends Controller
{ use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:category-list|category-create|category-edit|category-delete', ['only' => ['index','show']]);
        $this->middleware('permission:category-create', ['only' => ['create','store']]);
        $this->middleware('permission:category-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:category-delete', ['only' => ['destroy']]);
        $this->middleware('permission:cat_sub-list|cat_sub-create|cat_sub-edit|cat_sub-delete', ['only' => ['index','show']]);
        $this->middleware('permission:cat_sub-create', ['only' => ['create','store']]);
        $this->middleware('permission:cat_sub-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:cat_sub-delete', ['only' => ['destroy']]);
    }
    public function index( Request $request)
    {
        //
        $user = auth()->user();
        if ($request->ajax()) {
            $data = CenterType::orderBy('id','desc')->select('*');
            return Datatables::of($data)

            ->addIndexColumn()

            ->addColumn('action', function($row) use ($user){
                $btn = '';
                if ($user->can('category-edit')){
                $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="' . $row->id . '"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                }
                if($user->can('category-delete')) {
                   $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="' . $row->id . '"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
               }


                return $btn;

            })

            ->rawColumns(['action','icon'])

            ->make(true);
        }
        return view('admin.categories');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


            $validateErrors = Validator::make($request->all(),
                [
                    'name' => 'required|string|min:3',

                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
//        }
        $data =[
            'name' => $request->name,

        ];





       $id =  CenterType::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' , "data"=>Category::where('parent_id','=',$request->cat_id)->orderby('order_id')->get() ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = CenterType::find($id);

        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return  $this->editController($id,CenterType::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validateErrors = Validator::make($request->all(),
        [
            'name' => 'required|string|min:3',


        ]);
    if ($validateErrors->fails()) {
        return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
    } // end if fails .

            $data =[
                'name' => $request->name,

            ];

        CenterType::updateOrCreate(['id' => $request->_id],
    $data);


return response()->json(['status'=>200,'message' => ' تم حفظ البيانات بنجاح    .' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,CenterType::class);

    }

//    public function getSubs($id){
//        //
//        $data = Category::where([['parent_id','=',$id],['deleted','=',0]])->orderBy('order_id')->get();
//
//        $sub = Category::find($id);
//
//        return view('admin.category_sub',compact(["data","sub"]));
//    }


    public function deleteCat( $id)
    {

        $this->destroyController($id,CenterType::class);
    }
}
