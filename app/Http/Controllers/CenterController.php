<?php

namespace App\Http\Controllers;


use App\Models\Center;
use App\Models\CenterType;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

use App\Models\Category;

use Validator;
use DataTables;
use App\Http\Traits\allTrait;


class CenterController extends Controller
{ use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:category-list|category-create|category-edit|category-delete', ['only' => ['index','show']]);
        $this->middleware('permission:category-create', ['only' => ['create','store']]);
        $this->middleware('permission:category-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:category-delete', ['only' => ['destroy']]);
        $this->middleware('permission:cat_sub-list|cat_sub-create|cat_sub-edit|cat_sub-delete', ['only' => ['index','show']]);
        $this->middleware('permission:cat_sub-create', ['only' => ['create','store']]);
        $this->middleware('permission:cat_sub-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:cat_sub-delete', ['only' => ['destroy']]);
    }
    public function index( Request $request)
    {
        //
        $user = auth()->user();
        if ($request->ajax()) {
            $data = Center::with('type')->with('manager')->orderBy('id','desc')->select('*');
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row) use ($user){
                    $btn = '';
                    if ($user->can('category-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="' . $row->id . '"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }
                    if($user->can('category-delete')) {
                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="' . $row->id . '"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
                    }
                    $btn .= '<a href="'.url('center/emps', $row->id).'" data-toggle="tooltip" class="info btn btn-secondary btn-sm "  data-id="'.$row->id.'" style="color: white !important;"> <i class="fa fa-empire"></i>  الموظفين  </a> &nbsp; ';


                    return $btn;

                })

                ->rawColumns(['action','icon'])

                ->make(true);
        }
        $cats = CenterType::get();
        $managers = User::where('role',3)->get();
        return view('admin.centers', compact('cats','managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validateErrors = Validator::make($request->all(),
            [
                'name' => 'required|string|min:3',
                'address' => 'required|string|min:3',
                'center_type_id' => 'required|numeric',

            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .
//        }
        $data =[
            'name' => $request->name,
            'address' => $request->address,
            'center_type_id' => $request->center_type_id,
            'manager_id' => $request->manager_id,
            'workers' => $request->workers,
            'lat' => $request->lat,
            'lng' => $request->lng,

        ];





        $id =  Center::updateOrCreate(['id' => $request->_id],
            $data)->id;
            $user = User::where('id',$request->manager_id)->first();
            $user->update([['center_id'=>$id],['role'=>5]]);


        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' , "data"=>"" ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = Center::find($id);

        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return  $this->editController($id,Center::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validateErrors = Validator::make($request->all(),
            [
                'name' => 'required|string|min:3',
                'address' => 'required|string|min:3',
                'center_type_id' => 'required|numeric',


            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .

        $data =[
            'name' => $request->name,
            'address' => $request->address,
            'center_type_id' => $request->center_type_id,
            'manager_id' => $request->manager_id,
            'workers' => $request->workers,

        ];

        Center::updateOrCreate(['id' => $request->_id],
            $data);
            if(!empty($request->manager_id)){
                User::where('id',$request->manager_id)->update([['center_id',$request->_id],['role'=>5]]);
            }


        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات بنجاح    .' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,Center::class);

    }

//    public function getSubs($id){
//        //
//        $data = Category::where([['parent_id','=',$id],['deleted','=',0]])->orderBy('order_id')->get();
//
//        $sub = Category::find($id);
//
//        return view('admin.category_sub',compact(["data","sub"]));
//    }


}
