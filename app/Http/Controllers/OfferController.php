<?php

namespace App\Http\Controllers;
use App\Http\Traits\allTrait;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use DataTables;
use Auth;

class OfferController extends Controller
{ use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:offer-list|offer-create|offer-edit|offer-delete', ['only' => ['index','show']]);
        $this->middleware('permission:offer-create', ['only' => ['create','store']]);
        $this->middleware('permission:offer-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:offer-delete', ['only' => ['destroy']]);
    }
    public function index( Request $request)
    {
        $user = auth()->user();

        //
        $data = Product::where('offer_image','!=','')->get();
        if ($request->ajax()) {
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action',  function($row) use ($user){
                    $btn = '';
                    if ($user->can('offer-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
                    }
                    if($user->can('offer-delete')) {
                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="'.$row->id.'"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }

//                    $btn = '<a href="javascript:void(0)" class="show btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-eye"></i> </a> &nbsp;';

                    return $btn;

                })->addColumn('cat',function ($row){
                    if(empty($row->cat_id))
                        return "";
                    return Category::find($row->cat_id)->name;

//                })->addColumn('market',function ($row){
//                    if(empty($row->market_id))
//                        return "";
//                    return User::find($row->market_id)->name;

                })->addColumn('offer_image',function ($row){

                    return "<img src='".url('public/storage/'.$row->offer_image)."' width='50' height='50'>";
                })

                ->rawColumns(['action','cat','offer_image'])

                ->make(true);
        }

//        $main_cats = Category::with('childs')->where('parent_id','=',0)->get();
        $cats = Category::with('childs')->where('parent_id','=',0)->get();
        $markets = User::where('role','=',5)->get();

        return view('admin.offers' , compact('markets','cats'));
    }


    public function filter( Request $request, $cat_id)
    {
        //
        $data = Product::where([['cat_id', '=', $cat_id],['offer_image','!=',null]])->get();
        if ($request->ajax()) {
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row){

//                    $btn = '<a href="javascript:void(0)" class="show btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-eye"></i> </a> &nbsp;';
                    $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';

                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="'.$row->id.'"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    return $btn;

                })->addColumn('cat',function ($row){
                    if(empty($row->cat_id))
                        return "";
                    return Category::find($row->cat_id)->name;

//                })->addColumn('market',function ($row){
//                    if(empty($row->market_id))
//                        return "";
//                    return User::find($row->market_id)->name;


                })->addColumn('offer_image',function ($row){

                    return "<img src='".url('public/storage/'.$row->offer_image)."' width='50' height='50'>";
                })

                ->rawColumns(['action','cat','offer_image'])


                ->make(true);
        }


        $cats = Category::with('childs')->where('parent_id','=',0)->get();
        $markets = User::where('role','=',5)->get();

        return view('admin.offers' , compact('markets','cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        if(empty($request->_id)) {

        $validateErrors = Validator::make($request->all(),
            [
                'offer_price' => 'required',

            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .
//        }

        $data =[
            'offer_price' => $request->offer_price,

        ];

        if(!empty($request->offer_image)){
            $data['offer_image'] = $request->offer_image;
        }

        $id =  Product::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تمت الإضافة بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        return  $this->editController($id,Product::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteOffer($id){
        $data = [
            'offer_price'=> null,
            'offer_image'=> null
        ];
        Product::find($id)->update($data);
        return response()->json(['success'=>' تم الحذف بنجاح ']);
    }
}
