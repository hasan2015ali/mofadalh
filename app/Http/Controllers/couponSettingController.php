<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CouponSetting;
use DataTables;

class couponSettingController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        if ($request->ajax()) {

            $data = CouponSetting::get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action',  function($row) use ($user){
                    $btn = '';
                    if ($user->can('coupon_set-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="' . $row->id . '"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }
//                    if($user->can('color-delete')) {
//                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="' . $row->id . '"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
//                    }
                    return $btn;

                })



                ->rawColumns(['action'])

                ->make(true);

            return;
        }

        return view ("admin.couponSetting");
    }
    public function getCouponInfo($id){

        $data = CouponSetting::find($id);

        return response()->json($data);

    }

    public function updateCoupon(Request $request, $id)
    {
        $data = CouponSetting::find($id);

        $data->discountRate = $request->input('discountRate');
        $data ->expiry = $request->input('expiry');
        $data->update($request->all());
        return response()->json($data);
    }
}
