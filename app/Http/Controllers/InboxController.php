<?php

namespace App\Http\Controllers;
use App\Http\Traits\allTrait;
use App\Models\Inbox;
use DataTables;
use Validator;

use Illuminate\Http\Request;


class InboxController extends Controller
{
    use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:inbox-list|inbox-create|inbox-edit|inbox-delete', ['only' => ['index','show']]);
        $this->middleware('permission:inbox-create', ['only' => ['create','store']]);
        $this->middleware('permission:inbox-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:inbox-delete', ['only' => ['destroy']]);
    }
    public function index( Request $request)
    {
        $user = auth()->user();
        if ($request->ajax()) {

            $data = Inbox::get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action',  function($row) use ($user){
                    $btn = '';
                    if ($user->can('inbox-edit')){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct"> <i class="fa fa-edit"></i> </a>';
                    }
                    if($user->can('inbox-delete')) {
                        $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"> <i class="fa fa-trash-o"></i> </a>';
                    }
                    $btn .= '<a href="'.url('translations/inbox', $row->id).'" data-toggle="tooltip" class="info btn btn-secondary btn-sm "  data-id="'.$row->id.'" style="color: white !important;"> <i class="fa fa-language"></i>  الترجمة  </a> &nbsp; ';



                    return $btn;

                })

                ->addColumn('details', function($row){

                    if(strlen($row->details)>30){

                   return mb_substr($row->details, 0, 30) ."....";}

                   else{
                    return $row->details;
                   }

                })


                ->rawColumns(['action','details'])

                ->make(true);

            return;
        }


        return view ("admin.inbox");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'details' => 'required|string|min:3',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,
            'details' => $request->details,
        ];

        $id =  Inbox::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function show(Inbox $inbox)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return  $this->editController($id,Inbox::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Inbox::find($id);
        $validateErrors = Validator::make($request->all(),
        [
            'details' => 'required|string|min:3',


        ]);
    if ($validateErrors->fails()) {
        return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
    } // end if fails .
        $item->update($request->all());
        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $this->destroyController($id,Inbox::class);
    }

}
