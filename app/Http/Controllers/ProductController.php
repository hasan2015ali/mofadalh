<?php

namespace App\Http\Controllers;
use App\Http\Traits\allTrait;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Price;
use App\Models\Comment;
use App\Models\ProductStars;
use Validator;
use DataTables;
use Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        //

        $data = Product::with('stars')->with('prices')->get();

        if ($request->ajax()) {
            return Datatables::of($data)

            ->addIndexColumn()

            ->addColumn('action', function($row){

                $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="'.$row->id.'"> <i class="fa fa-edit"></i> </button>&nbsp;';
                $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button>&nbsp;';
                $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="view btn btn-secondary btn-sm" data-id="'.$row->id.'"> <i class="fa fa-info"></i> </button>&nbsp;';
                $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="comments btn btn-warning btn-sm" data-id="'.$row->id.'"> <i class="fa fa-commenting"></i> </button>&nbsp;';
                if(intval($row->in_slider) == 0)
                $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="addToSlider btn btn-primary btn-sm" data-id="'.$row->id.'"> <i class="fa fa-image"></i> </button>&nbsp;';
                $btn .= '<a href="'.url('translations/product', $row->id).'" data-toggle="tooltip" class="info btn btn-secondary btn-sm "  data-id="'.$row->id.'" style="color: white !important;"> <i class="fa fa-language"></i>  الترجمة  </a> &nbsp; ';

                return $btn;

            })


            ->addColumn('available', function($row){

                    $btn = "";
                if($row->available == 0){

                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="makeAvailable btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-minus"></i> </button>&nbsp;';
                }
                else{

                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="makeNotAvailable btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-check"></i> </button>&nbsp;';

                }
                return $btn;

            })

            ->addColumn('cat',function ($row){
                if(empty($row->cat_id))
                    return "";
                return Category::find($row->cat_id)->name ?? "فئة محذوفة";

            })
            ->addColumn('image',function ($row){

                $images = explode(",",$row->images);
                return "<img src='".url('public/storage/'.$images[0])."' width='50' height='50'>";

            })
            ->addColumn('offer_image',function ($row){

                return "<img src='".url('public/storage/'.$row->offer_image)."' width='50' height='50'>";
            })

            ->addColumn('price', function($row){
                //if ( sizeof($prices) >0)

                if(count($row->prices)>0)

                {
                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="details" class=" btn btn-dark btn-sm price">'.$row->prices[0]->price.'</a>';

                   return $btn    ;
                }
                else
               { $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="details" class=" btn btn-dark btn-sm price"> أضف السعر</a>';

                return $btn;}
            })
             ->addColumn('stars',function ($row){
                $stars_sum =  $row->stars->pluck('stars');

                $rating =  $stars_sum->avg();
                return $this->showStars($rating);

              })

            ->rawColumns(['action','cat','image','price','offer_image','stars','available'])

            ->make(true);
        }

        $cats = Category::with('childs')->where('parent_id','=',0)->get();

        $markets = User::where('role','=',5)->get();

        return view('admin.products' , compact('markets','cats'));
    }



    public function store_price(Request $request){
        $validateErrors = Validator::make($request->all(),
        [
            'name' => 'required|string|min:3',
            'price' => 'required',

        ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .

        $data =[
            'name' => $request->name,
            'price' => $request->price,
            'product_id' => $request->product_id,
            'order_id'=> 0,

        ];
        $id =  Price::Create($data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' , "data"=>Price::where('product_id','=',$request->product_id)->orderby('order_id')->get() ]);
    }
    //new price for products

    public function getPrices($id){

        $data=Price::where('product_id','=',$id)->orderby('order_id')->get();
        return response()->json($data);
    }

    public function deletePrice($id){

        $this->destroyController($id,Price::class);}


    public function UpdatePrice(Request $request ){


        $names=$request->name;
        $prices=$request->price;
        $orders_id=$request->order_id;
        $price_id=$request->price_id;
        $i=0;
        foreach($names as $name ){
        $data =[
            'name' => $name,
            'price' =>$prices[$i],
            'order_id'=> $i,


        ];


       Price::where('id',"=",$price_id[$i])->update($data);
       $i++;
        }



        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' , "data"=>Price::where('product_id','=',$request->product_id)->orderby('order_id')->get() ]);


    }

    public function getComments($id){

        $data=Comment::with('user')->where('product_id','=',$id)->get();
        return response()->json($data);

    }

    public function deletComment($id){
        $this->destroyController($id,Comment::class);
    }

    public function getStars($id){

        $data=ProductStars::where('product_id','=',$id)->get();
        return response()->json($data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        if(empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'name' => 'required|string|min:3',

                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
//        }

        $data =[
            'name' => $request->name,
            'offer_price' => $request->offer_price,
            'images' => $request->image,
            'cat_id' => $request->cat,
            'discount' => $request->discount,

        ];
        if(!empty($request->input("document"))){
            $images = implode(",",$request->input("document"));
            $data['images'] = $images;
        }

        if(!empty($request->offer_image)){
            $data['offer_image'] = $request->offer_image;
        }

       $id =  Product::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تمت الإضافة بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = Product::find($id);

        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        return  $this->editController($id,Product::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validateErrors = Validator::make($request->all(),
        [
            'name' => 'required|string|min:3',

        ]);
    if ($validateErrors->fails()) {
        return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
    } // end if fails .

            $data =[
                'name' => $request->name,
                'offer_price' => $request->offer_price,
                'images' => $request->image,
                'offer_image' => $request->offer_image,
                'cat_id' => $request->cat,
                'discount' => $request->discount,

            ];

  Product::updateOrCreate(['id' => $request->_id],
    $data);


        return response()->json(['status'=>200,'message' => ' تمت الإضافة بنجاح    .' ]);
    }
    public function changeAvailableStatus(Request $request)
    {
        $data =[
            'available' => $request->status,

        ];

            Product::where('id' ,  $request->product)->update($data);
            return response()->json(['status'=>200,'message' => ' تمت العملية بنجاح    .' ]);

    }
    public function changeSliderProduct(Request $request)
    {
        $data =[
            'in_slider' => $request->slider,

        ];

            Product::where('id' ,  $request->product)->update($data);
            return response()->json(['status'=>200,'message' => ' تمت العملية بنجاح    .' ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,Product::class);

    }

    public function filter( Request $request, $cat_id)
    {
        //
        $data = Product::with('stars')->with('prices')->where([['cat_id', '=', $cat_id]])->get();
        if ($request->ajax()) {
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row){

//                    $btn = '<a href="javascript:void(0)" class="show btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-eye"></i> </a> &nbsp;';

                    $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="'.$row->id.'"> <i class="fa fa-edit"></i> </button>&nbsp;';
                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button >&nbsp;';
                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="view btn btn-secondary btn-sm" data-id="'.$row->id.'"> <i class="fa fa-info"></i> </button >&nbsp;';
                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="comments btn btn-warning btn-sm" data-id="'.$row->id.'"> <i class="fa fa-commenting"></i> </button >&nbsp;';
                    if(intval($row->in_slider) == 0)
                $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="addToSlider btn btn-primary btn-sm" data-id="'.$row->id.'"> <i class="fa fa-image"></i> </button>&nbsp;';
                $btn .= '<a href="'.url('translations/product', $row->id).'" data-toggle="tooltip" class="info btn btn-secondary btn-sm "  data-id="'.$row->id.'" style="color: white !important;"> <i class="fa fa-language"></i>  الترجمة  </a> &nbsp; ';

                    return $btn;

                })
                ->addColumn('available', function($row){

                    $btn = "";
                if($row->available == 0){

                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="makeAvailable btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-minus"></i> </button>&nbsp;';
                }
                else{

                    $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="makeNotAvailable btn btn-success btn-sm" data-id="'.$row->id.'"> <i class="fa fa-check"></i> </button>&nbsp;';

                }
                return $btn;

                  })

                ->addColumn('cat',function ($row){
                    if(empty($row->cat_id))
                        return "";
                    return Category::find($row->cat_id)->name;



                })->addColumn('image',function ($row){

                    $images = explode(",",$row->images);
                    return "<img src='".url('public/storage/'.$images[0])."' width='50' height='50'>";

                })->addColumn('offer_image',function ($row){

                    return "<img src='".url('public/storage/'.$row->offer_image)."' width='50' height='50'>";
                })

                ->addColumn('price', function($row){

                    if(count($row->prices)>0)

                    {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="details" class=" btn btn-dark btn-sm price">'.$row->prices[0]->price.'</a>';

                       return $btn    ;
                    }
                    else
                   { $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="details" class=" btn btn-dark btn-sm price"> أضف السعر</a>';

                    return $btn;}

                })->addColumn('stars',function ($row){
                    $stars_sum =  $row->stars->pluck('stars');

                    $rating =  $stars_sum->avg();
                    return $this->showStars($rating);

                })
                ->rawColumns(['action','cat','image','price','offer_image','stars','available'])


                ->make(true);
        }


        $cats = Category::with('childs')->where('parent_id','=',0)->get();
        $markets = User::where('role','=',5)->get();

        return view('admin.products' , compact('markets','cats'));
    }




      public function showStars($rating)
    {

                       $out = "";
                       foreach(range(1,5) as $i){
                            $out .= '<span class="fa-stack" style="width:1em">
                            <i class="fa fa-star fa-stack-1x"></i>';



                           if($rating >0){
                            if($rating >0.5){
                                $out .= '   <i class="fa fa-star fa-star-full fa-stack-1x" ></i>';
                               }

                               else{
                                $out .= '    <i class="fa fa-star-half fa-stack-1x" ></i>';
                               }
                           }



                         $rating--;
                        $out .=' </span>';
                        }

                            return $out;
    }
}
