<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function index()
    {
    	$filePath = public_path("dd.pdf");
    	$headers = ['Content-Type: application/pdf'];
    	$fileName = time().'.pdf';

    	return response()->download($filePath, $fileName, $headers);
    }


	public function indexapp()
    {
    	$filePath = public_path("dd.APK");
    	$headers = ['Content-Type: application/APK'];
    	$fileName ="مفاضلة_طرطوس".time().'.APK';

    	return response()->download($filePath, $fileName, $headers);
    }

}
