<?php

namespace App\Http\Controllers;


use App\Models\Center;
use App\Models\CenterType;
use App\Models\Product;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;

use App\Models\Category;

use Validator;
use DataTables;
use App\Http\Traits\allTrait;
use App\Models\StudentAppointment;
use Hash;
use Auth;
use Illuminate\Support\Facades\DB;

class CenterAppointmentsController  extends Controller
{

    use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
//        $this->middleware('permission:category-list|category-create|category-edit|category-delete', ['only' => ['index','show']]);
//        $this->middleware('permission:category-create', ['only' => ['create','store']]);
//        $this->middleware('permission:category-edit', ['only' => ['edit','update']]);
//        $this->middleware('permission:category-delete', ['only' => ['destroy']]);

    }
    public function index( Request $request)
    {
        //
        $center_id = 1;
        $user = auth()->user();
        $center_id = $user->center_id;

        if ($request->ajax()) {
            $data = StudentAppointment::with('student')->where('center_id',$center_id)->orderBy('id','desc')->select('*');
        //    $data = DB::table('student_appointments')->join('students','students.national_id','=','student_appointments.student_id')->get();
            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row) use ($user){
                    $btn = '';
                    if ($user->can('category-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="' . $row->id . '"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }
                    if($user->can('category-delete')) {
                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="' . $row->id . '"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
                    }


                    return $btn;

                })

                ->rawColumns(['action',])

                ->make(true);
        }
        $cats = CenterType::get();
        return view('admin.centers.appointments', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validateErrors = Validator::make($request->all(),
            [
                'name' => 'required|string|min:3',



            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .
//        }
        $data =[
            'name' => $request->name,
            'last_name' => $request->last_name,
            'mother_name' => $request->mother_name,
            'mobile' => $request->mobile,
            'national_id' => $request->national_id,
            'center_type_id' => 1,

        ];
        Student::updateOrCreate(['national_id'=>$request->national_id],
        $data);
        $appoint = [
            "student_id"=>$request->national_id,
            "center_id"=>Auth::user()->center_id,
            "date"=>date("Y-m-d",strtotime($request->date)),
            "time"=> $request->hour.":".$request->minute,
        ];

        StudentAppointment::updateOrCreate(['student_id'=>$request->national_id],$appoint);

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' , "data"=>"" ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = User::find($id);

        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = StudentAppointment::with("student")->find($id);
        return response()->json($item);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validateErrors = Validator::make($request->all(),
            [
                'name' => 'required|string|min:3',



            ]);
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .

        $data =[
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,

            'role' => 4,

        ];
        if(!empty($request->password)){
            $data['password'] = Hash::make( $request->password);
        }
        User::updateOrCreate(['id' => $request->_id],
            $data);


        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات بنجاح    .' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,StudentAppointment::class);

    }



}
