<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\allTrait;
use App\Models\Help;
use DataTables;
use Validator;

class HelpController extends Controller
{ use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:help-list|help-create|help-edit|help-delete', ['only' => ['index','show']]);
        $this->middleware('permission:help-create', ['only' => ['create','store']]);
        $this->middleware('permission:help-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:help-delete', ['only' => ['destroy']]);
    }

    public function index( Request $request)
    {
        $user = auth()->user();
        if ($request->ajax()) {

            $data = Help::get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action',  function($row) use ($user){
                    $btn = '';
                    if ($user->can('help-edit')){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct"> <i class="fa fa-edit"></i> </a>';
                    }
                    if($user->can('help-delete')) {
                        $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"> <i class="fa fa-trash-o"></i> </a>';
                    }
                    $btn .= '&nbsp; <a href="'.url('translations/help', $row->id).'" data-toggle="tooltip" class="info btn btn-secondary btn-sm "  data-id="'.$row->id.'" style="color: white !important;"> <i class="fa fa-language"></i>  الترجمة  </a> &nbsp; ';

                    return $btn;

                })
                ->addColumn('video',function ($row){

                return "<video width='200' height='150' controls>
                    <source src='".asset('storage/'.$row->video)."' type='video/mp4'> </video>";
            })


                ->rawColumns(['action','video'])

                ->make(true);

            return;
        }


        return view ("admin.help");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'title' => 'required|string|min:3',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'title' => $request->title,
            'video' => $request->video,
        ];

        $id =  Help::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return  $this->editController($id,Help::class);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Help::find($id);
        $validateErrors = Validator::make($request->all(),
        [
            'title' => 'required|string|min:3',


        ]);
    if ($validateErrors->fails()) {
        return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
    } // end if fails .
        $item->update($request->all());
        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,Help::class);  }
}
