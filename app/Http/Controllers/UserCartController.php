<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserNotificationModel;
use App\Models\UserOrder;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Kreait\Firebase\Messaging\CloudMessage;
use Validator;

class UserCartController extends Controller
{
    //

    public  function viewRequests(Request  $request){
        $offers = UserOrder::all()->sortByDesc('id');
        $status = ['0'=>'قيد المراجعة ', '1'=> 'تم القبول', '2'=>'جاري التحضير', '3'=>'جاري التوصيل','4'=>'تم التوصيل'];
        return view('admin.purchases.purchases' , compact('offers','status'));
    }


//    public  function setOrderDelegate(Request  $request){
//
//         UserOrder::where('id','=',$request->_order_id)->update(['delegate_id'=>$request->delegate_id]);
//        return  response()->json(['status'=>200,'message'=>'تم بنجاح']);
//    }

    public  function getOrderDetails( $id, $region = null){

//        $data  =  UserOrder::with('products:id,product_id, product.name,amount,price,full_price  ')->where('id','=',$id)->get();
        // $data  =  UserOrder::with('products:id,product_id,amount,price,full_price')->where('id','=',$id)->get();
      //  $data  =  UserOrder::with('products.product')->where('id','=',$id)->get();
//        $region = $request->region_val;
//        dd($region);
        if($region == 0){
            $order = DB::select('select user_orders.* , users.name as full_name from user_orders
        inner join users on user_orders.user_id=users.id where user_orders.id = ?', [$id]);

        }else{
            $order = DB::select('select user_orders.* , users.name as full_name, 
        deliver_regions.deliverPrice as deliv_coast, deliver_regions.regionName as region from user_orders
        inner join deliver_regions on user_orders.region_id=deliver_regions.id 
        inner join users on user_orders.user_id=users.id where user_orders.id = ?', [$id]);

        }


        $products = DB::select("SELECT 
         user_order_details.product_id as product_id , user_order_details.amount as p_amount ,
         user_order_details.single_price as pr_price, user_order_details.full_price as pr_full_price,
         user_order_details.type_amount as type_a,
         products.name as product_name, products.images as image FROM user_orders 
         inner JOIN user_order_details on user_orders.id =user_order_details.order_id 
         inner join products on user_order_details.product_id=products.id 
         WHERE user_orders.id=? ",[$id]);

        if(count($order) > 0){

            $date = ["delivery_at"=>$this->date($order[0]->delivery_at),
                  "deliver_end_time"=>$this->date($order[0]->deliver_end_time)];

            return response()->json([
                'status'=>200,
                'data'=>["order"=>$order[0],"products"=>$products,"date"=>$date]
            ]);
        }
        else{
            return response()->json([
                'status'=>404,
                'data'=>[]
            ]);
        }
    }
    public function deleteOrder($id){

        $item = UserOrder::find($id);
        if($item){
            $item->delete();
        }

        return response()->json(['status'=>200,'message'=>' تم الحذف بنجاح']);

    }

    public  function filterRequests(Request  $request, $status){
        $data = UserOrder::with('client')->where('status','=',$status)->orderby('id','desc')->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function updateOrder(Request $request){

        $validateErrors = Validator::make($request->all(),
            [
                'notes' => 'string|min:3|max:255',

            ]);

        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        } // end if fails .

        $data =[
            'details' => $request->notes,

        ];

        UserOrder::updateOrCreate(['id' => $request->_id],
            $data);


        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات بنجاح .' ]);
    }

    public  function markOrderViewed(Request $request, $id){

        $id = UserOrder::find($id)->user_id;
        $data = $request->status_update;
        UserOrder::where('id',$id)->update(['status'=>$data]);

        $details = ['1'=> 'تم قبول طلبيتك', '2'=>'جاري تحضير طلبيتك', '3'=>'جاري توصيل طلبيتك','4'=>'تم توصيل طلبيتك'];
        $title = "إشعار جديد";

//        UserNotificationModel::updateOrCreate(['id' => $request->_id], $data)->id;
        //  sending message.
        $notifi = ['title' =>  $title,  'body' =>  $details[$request->status_update],  'click_action'=>'FLUTTER_NOTIFICATION_CLICK'];
        //        $pusher->trigger('chat', 'message-recived_'.$request->input('reciver_id'), $notifi);

        $messaging = app('firebase.messaging');
        $message1 = CloudMessage::fromArray([
            'topic' => "messageRecived".$id,
            'notification' => $notifi, // optional
            'data' => ["message"=>$details[$request->status_update], 'click_action'=>'FLUTTER_NOTIFICATION_CLICK'], // optional
        ]);

        $messaging->send($message1);
        return  response()->json(['status'=>200]);
    }

    public function date($date){
        if($date != '') {
            $dateTime = new DateTime($date);
            $dateFormat = $dateTime->format("H:i l j F Y");
            return $dateFormat;
        }
    }

}
