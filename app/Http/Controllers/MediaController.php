<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    //
    public function storeMedia(Request $request , $table)
    {

        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $name =  "";
        switch ($table){
            case "users":
                $name = "users". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
                $path = storage_path('app/public/users/uploads');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = "users/uploads/".$name;
                break;

            case "sliders":
                $name = "slide". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
                $path = storage_path('app/public/sliders/uploads');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = "sliders/uploads/".$name;
                break;
            case "products":
                $name = "product". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
                $path = storage_path('app/public/products/uploads');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = "products/uploads/".$name;
                break;

                case "categories":
            $name = "cat". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
            $path = storage_path('app/public/categories/uploads');
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $name = "categories/uploads/".$name;
            break;
//            case "category_sub":
//                $name = "cat". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
//                $path = storage_path('app/public/category_sub/uploads');
//                if (!file_exists($path)) {
//                    mkdir($path, 0777, true);
//                }
//                $name = "category_sub/uploads/".$name;
//                break;
            case "notifications":
            $name = "notification". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
            $path = storage_path('app/public/notifications/uploads');
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $name = "notifications/uploads/".$name;
            break;

            case "helps":
                $name = "help". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
                $path = storage_path('app/public/helps/uploads');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = "helps/uploads/".$name;
                break;

        }

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
            "img2"=>str_replace("/","_",$name)
        ]);
    }



    public function destroyMedia($table, Request $request){
        switch ($table){
            case "users":
                $item = User::find($request->user_id);
                $images = explode(",",$item->images);
                $key = array_search($request->image, $images);
                if($key !== false){

                    unset($images[$key]);
                }
                $images =implode(",",$images);

                User::where('id','=',$request->user_id)->update(['images'=>$images]);
                return response()->json(["status"=>200,"images" =>$images, "img"=>$request->image , "img2"=>str_replace("/","_",$images)]);
                break;

            case "products":
                $item = Product::find($request->product_id);
                $images = explode(",",$item->images);
                $key = array_search($request->image, $images);
                if($key !== false){

                    unset($images[$key]);
                }
                $images =implode(",",$images);

                Product::where('id','=',$request->product_id)->update(['images'=>$images]);
                return response()->json(["status"=>200,"images" =>$images, "img"=>$request->image , "img2"=>str_replace("/","_",$images)]);
                break;

            case "offer_products":
                $item = Product::find($request->product_id);
                $offer_images = explode(",",$item->offer_image);
                $key = array_search($request->image, $offer_images);
                if($key !== false){

                    unset($offer_images[$key]);
                }
                $offer_images =implode(",",$offer_images);

                Product::where('id','=',$request->product_id)->update(['offer_image'=>$offer_images]);
                return response()->json(["status"=>200,"offer_image" =>$offer_images, "img"=>$request->image , "img2"=>str_replace("/","_",$offer_images)]);
                break;
        }

    }
}
