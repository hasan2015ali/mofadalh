<?php

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\Country;
use App\Models\Role;
use App\Models\UserNotificationModel;
use Illuminate\Http\Request;
use DataTables;
use Validator;



class UserNotificationsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:notify-list|notify-create|notify-edit|notify-delete', ['only' => ['index','show']]);
        $this->middleware('permission:notify-create', ['only' => ['create','store']]);
        $this->middleware('permission:notify-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:notify-delete', ['only' => ['destroy']]);
    }
    public  function viewNotifications(Request $request){

        $user = auth()->user();

        if ($request->ajax()) {
            $data = UserNotificationModel::with('user')->with('region')->with('role')->orderBy('id','desc')->get();


            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function($row) use ($user){
                    $btn = '';
                    if ($user->can('notify-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="'.$row->id.'"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }
                    if($user->can('notify-delete')) {
                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
                    }

                    return $btn;

                })

                ->addColumn('icon', function($row){
                    return "<img src='".asset('storage/'.$row->img)."' width='50' height='50'>";

                })
                ->rawColumns(['action','icon'])

                ->make(true);
        }

        $areas = Country::where('parent_id','=',0)->get();
        $roles = Role::where('id','!=',1)->get();
    return view('admin.notifications' , compact('roles','areas'));
    }

    public function store(Request $request)
    {
        //


        if(empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'title' => 'required|string|min:3',
                    'details' => 'required|string|min:3',



                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }
        $data =[
            'title' => $request->title,
            'details' => $request->details,
            'user_id' => $request->user_id,
            'region_id' => $request->region_id,
            'role_id' => $request->role_id,
            'img' => $request->icon,

        ];


        $id =  UserNotificationModel::updateOrCreate(['id' => $request->_id],
            $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);
    }

    public function edit($id)
    {

        return  $this->editController($id,UserNotificationModel::class);
    }

}
