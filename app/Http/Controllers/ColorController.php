<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\allTrait;
use App\Models\Color;
use DataTables;
use Validator;
class ColorController extends Controller
{   use allTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:color-list|color-create|color-edit|color-delete', ['only' => ['index','show']]);
        $this->middleware('permission:color-create', ['only' => ['create','store']]);
        $this->middleware('permission:color-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:color-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        if ($request->ajax()) {

            $data = Color::get();

            return Datatables::of($data)

                ->addIndexColumn()


                ->addColumn('action', function($row) use ($user){
                    $btn = '';
                    if ($user->can('color-edit')){
                        $btn = '<button href="javascript:void(0)" data-toggle="tooltip" class="edit btn btn-primary btn-sm"  data-id="' . $row->id . '"> <i class="fa fa-edit"></i> </button> &nbsp; ';
                    }
                    if($user->can('color-delete')) {
                        $btn .= '<button href="javascript:void(0)" data-toggle="tooltip" class="delete btn btn-danger btn-sm" data-id="' . $row->id . '"> <i class="fa fa-trash-o"></i> </button > &nbsp; ';
                    }

                    return $btn;

                })



                ->rawColumns(['action'])

                ->make(true);

            return;
        }

        return view ("admin.color");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( empty($request->_id)) {

            $validateErrors = Validator::make($request->all(),
                [
                    'arabicName' => 'required|string|min:3',


                ]);
            if ($validateErrors->fails()) {
                return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
            } // end if fails .
        }

        $data =[
            'arabicName' => $request->arabicName,
            'englishName' => $request->englishName,
            'colorValue' => $request->colorValue,
        ];

        $id =  Color::updateOrCreate(['id' => $request->_id], $data)->id;

        return response()->json(['status'=>200,'message' => ' تم حفظ البيانات  بنجاح .' ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return  $this->editController($id,Color::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Color::find($id);
        $validateErrors = Validator::make($request->all(),
        [
            'arabicName' => 'required|string|min:3',


        ]);
    if ($validateErrors->fails()) {
        return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
    } // end if fails .
        $item->update($request->all());
        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id,Color::class);
    }
}
