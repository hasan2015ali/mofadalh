@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                الفئات
                            </p>
                            <div class="row">
                                <form action="{{route('crud.store',['table'=>'categories'])}}" method="post" id="addForm">
                                    @csrf

                                    <div class="col-sm-9">
                                        <fieldset class="form-group">
                                            <label for="roundText">  اسم الفئة</label>
                                            <input type="text" id="name" name="name" class="form-control round" placeholder="  ادخل اسم الفئة">
                                            <br>
                                            <button type="button" id="addFormBtn" class="btn gradient-purple-bliss">إضافة</button>

                                        </fieldset>
                                    </div>
                                </form>
                            </div>
                            <div class="row">

                                <div class="col-sm-12">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th>اسم الفئة</th>



                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل بيانات الفئة  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromData">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <label> اسم الفئة </label>
                        <div class="form-group">
                            <input type="text" name="name" id="name_edit" placeholder="" class="form-control">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveBtn" class="btn btn-primary" value="حفظ">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('myjs')

    <script type="text/javascript">

        $(function () {


            var table = $('.data-table').DataTable({

                processing: true,

                serverSide: true,

                ajax: "{{ route('crud.index',['table'=>'categories']) }}",

                columns: [

                    {data: 'id', name: 'id'},

                    {data: 'name', name: 'name'},


                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });

            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('crud.store',['table'=>'categories']) }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');


                        $('#addForm').trigger("reset");

                    alert(data.message);
                        // toastr.success('تم الحفظ بنجاح');

                        table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');



                    }

                });
            }); // end add new record

            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');

                $.get("{{ route('crud.index',['table'=>'categories']) }}" + '/' + product_id + '/edit', function (data) {




                    $("#saveBtn").val("حفظ");


                    $('#_id').val(data.id);

                    $('#name_edit').val(data.name);



                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ route('crud.index',['table'=>'categories']) }}"+ '/' + product_id + '/update',

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);

                        $('#editFromData').trigger("reset");

                        alert(data.message);


                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);



                    }

                });
            }); // end save  record data

            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                var co = confirm("  هل أنت متأكد من الحذف  !");
                if (!co) {
                    return;
                }


                $.ajax({

                    type: "DELETE",

                    url: "{{ route('crud.index',['table'=>'categories']) }}"+ '/' + product_id +"/delete",
                    data:{
                        '_token':'{{csrf_token()}}'
                    },
                    success: function (data) {
                       alert(data.message);
                        table.draw(false);

                    },

                    error: function (data) {
                        toastr.error("حدث خطأ     ");
                        console.log('خطأ:', data);

                    }

                });

            }); // end delete row

        });

    </script>

    @endpush