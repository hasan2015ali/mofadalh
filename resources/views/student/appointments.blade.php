<!DOCTYPE html>

<html class="loading" lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Apex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Apex admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>  واجهة الحجز</title>
    <link href="{{asset('app-assets/img/gallery/logo1.png')}}" rel="icon">
    <link href="{{asset('app-assets/img/gallery/apple_icon.png')}}" rel="apple-touch-icon">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="{{asset('css.css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\fonts\feather\style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\fonts\simple-line-icons\style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\fonts\font-awesome\css\font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\vendors\css\perfect-scrollbar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\vendors\css\prism.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\vendors\css\switchery.min.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\css-rtl\bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\css-rtl\bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\css-rtl\colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\css-rtl\components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\css-rtl\themes\layout-dark.min.css')}}">
    <link rel="stylesheet" href="{{asset('app-assets\css-rtl\plugins\switchery.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets\css-rtl\custom-rtl.min.css')}}">

    <link rel="stylesheet" href="{{asset('app-assets\css-rtl\pages\authentication.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\style-rtl.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />


</head>

<body class="vertical-layout vertical-menu 1-column auth-page navbar-sticky blank-page" data-menu="vertical-menu" data-col="1-column">

<div class="wrapper">
    <div class="main-panel">

        <div class="main-content">
            <div class="content-overlay"></div>
            <div class="content-wrapper">
                <section id="login" class="auth-height">
                    <div class="row full-height-vh m-0">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="card overflow-hidden">
                                <div class="card-content">
                                    <div class="card-body auth-img">
                                        <div class="row m-0">
                                            <div class="col-lg-6 d-none d-lg-flex justify-content-center align-items-center auth-img-bg p-3">
                                                <img src="{{asset('app-assets\img\gallery\logo1.png')}}" alt="" class="img-fluid" width="300" height="230">
                                            </div>



                                            <div class="col-lg-6 col-12 px-4 py-3">
                                                <h4 class="mb-2 card-title"> احجز مركز المفاضلة </h4>
                                                <p>  @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    @endif</p>
                                                    <form action="{{route('appointment-get')}}" method="post" id="editFromData">
                                                        @csrf
                                                        <input type="number" name="national_id"  class="form-control " value="{{$national_id ?? ''}}" placeholder="أدخل رقم الاكتتاب " readonly ><br>
                                                        <input type="text" name="name"  class="form-control " value="{{$student_appointMents->first()->student->name ?? ''}}" placeholder="أدخل  الاسم  "><br>
                                                        <input type="text" name="mother_name"  class="form-control " value="{{$student_appointMents->first()->student->mother_name ?? ''}}" placeholder="أدخل اسم الأم   "><br>
                                                        <input type="text" name="last_name"  class="form-control " value="{{$student_appointMents->first()->student->last_name ?? ''}}" placeholder="أدخل اسم الكنية   "><br>
                                                        <input type="text" name="mobile"  class="form-control " value="{{$student_appointMents->first()->student->mobile ?? ''}}" placeholder="أدخل رقم الموبايل    "><br>
                                                        <input type="hidden" name="center_type_id"  class="form-control " value="{{$center_type_id ?? ''}}" placeholder="   " readonly><br>


                                                        <label for="">اختر المركز</label>
                                                        <select name="center_id" id="center_id" class="form-control mb-2">
                                                            @foreach($centers as $item)
                                                                <option value="{{$item->id}}" @if( isset($center_id) && $item->id == $center_id) selected @endif>{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="">التاريخ</label>
                                                        <input  name="appointment_date"  class="form-control mb-2  " id="txtdate" type="text"  value="{{$student_appointMents->first()->date ?? ''}}">


                                                        <label for="">الساعة</label>
                                                        <select name="hour" id="hour">
                                                            @for ($i=8;$i<=16;$i++)
                                                                <option value='{{$i}}' @if( count($student_appointMents) > 0 &&  $i==date('H',strtotime($student_appointMents->first()->time))) selected @endif >{{$i}}</option>
                                                            @endfor
                                                        </select>
                                                        <label for="">الدقيقة</label>
                                                        <select name="minute" id="minute">
                                                            @for ($i=0;$i<=59;$i+=15)

                                                                <option value='{{$i}}' @if(count( $student_appointMents)>0 && $i==date('i',strtotime($student_appointMents->first()->time))) selected @endif>{{$i}}</option>
                                                            @endfor

                                                        </select>
                                                        <div class="d-sm-flex justify-content-between mb-3 font-small-2">


                                                        </div>
                                                        <div class="d-flex justify-content-between flex-sm-row flex-column">

                                                            <button class="btn  btn-primary" id="appointmentBtn"> الحصول على موعد </button>
                                                        </div>
                                                    </form>
                                                    <br>
                                                <h5 style="text-align: center">
                                                    <a href="{{url('appointment/home')}}">رجوع  <i class="fa fa-arrow-left"></i></a>

                                                </h5>

                                                         @if(count($student_appointMents) <=0)
                                                             <div class="alert alert-danger">
                                                                 لايوجد مواعيد سابقة
                                                             </div>
                                                         @endif



                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Login Page Ends-->
            </div>
        </div>
        <!-- END : End Main Content-->
    </div>
</div>

<script src="{{asset('app-assets\vendors\js\vendors.min.js')}}"></script>
<script src="{{asset('app-assets\vendors\js\switchery.min.js')}}"></script>

<script src="{{asset('app-assets\js\core\app-menu.min.js')}}"></script>
<script src="{{asset('app-assets\js\core\app.min.js')}}"></script>
<script src="{{asset('app-assets\js\notification-sidebar.min.js')}}"></script>
<script src="{{asset('app-assets\js\customizer.min.js')}}"></script>
<script src="{{asset('app-assets\js\scroll-top.min.js')}}"></script>
<script src="{{asset('app-assets\vendors\js\sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets\js\scripts.js')}}"></script>
<script src="{{asset('app-assets\js\helpers.js')}}"></script>
<script src="{{asset('assets\js\cu.js')}}"></script>


</body>

</html>
<script>
$(function(){


    $("#appointmentBtn").click(function (e) {
                e.preventDefault();

                $("#appointmentBtn").html('جاري الخفظ ..');
                $("#appointmentBtn").attr('disabled',true);

                var url = "{{ route('appointment-get') }}";

                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: "POST",

                    dataType: 'json',
                    timeout:10000,
                    success: function (data) {

                        $("#appointmentBtn").html(' بحث');
                        $("#appointmentBtn").attr('disabled',false);

                        if(data.status==200){
                            $('#editFromData').trigger("reset");

                            showSuccesFunctionParam(data.message);

                        }
                        else{
                            showErrorFunctionMessage(data.message);
                        }



                    },

                    error: function (data) {
                        $("#appointmentBtn").html(' حفظ');
                        $("#appointmentBtn").attr('disabled',false);

                        showErrorFunction();


                    }

                });
            }); // end save  record data

});

</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $("#txtdate").datepicker({
                minDate: 0
            });
        });
    </script>


