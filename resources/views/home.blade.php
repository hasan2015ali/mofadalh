<!DOCTYPE html>
<html  lang="ar">

<head>
    <meta charset="utf-8">

    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>   جامعة طرطوس - المفاضلة </title>
    <meta content="" name="description">

    <meta content="" name="keywords">


    <link href="{{asset('app-assets/img/gallery/logo1.png')}}" rel="icon">
    <link href="{{asset('app-assets/img/gallery/apple_icon.png')}}" rel="apple-touch-icon">


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <link href="{{asset('site/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('site/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('site/assets/vendor/aos/aos.css')}}" rel="stylesheet">
    <link href="{{asset('site/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('site/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
    <link href="{{asset('site/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">


    <link href="{{asset('site/assets/css/style.css')}}" rel="stylesheet">


</head>
<style>
.a {
	text-align: center;
}
.p{
    text-align:justify;

}
</style>
<body>

<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

        <a href="{{url('/')}}" class="logo d-flex align-items-center">
            <img src="{{asset('site/assets/img/logo1.png')}}" alt="">
            <span>    مفاضلة جامعة طرطوس</span>
        </a>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="#hero">الرئيسية</a></li>
                <li><a class="nav-link scrollto" href="#about"> حول</a></li>
                <li><a class="nav-link scrollto" href="#values">الخدمات</a></li>
                <li><a class="nav-link scrollto" href="#features">الميزات والارشادات  </a></li>
                <li><a class="nav-link scrollto" href="#faq">الاسئلة الشائعة </a></li>
                <li><a class="nav-link scrollto" href="#portfolio">المعرض</a></li>


                <li><a class="getstarted scrollto" href="{{url('appointment/home')}}">   احجز الأن</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="hero d-flex align-items-center">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center" >
                <h1 data-aos="fade-up">  مرحبا بك في موقع مفاضلة </h1>
                <h1  data-aos="fade-up" class="p">جامعة طرطوس </h1>
                <h2 data-aos="fade-up" data-aos-delay="400">            المفاضلة العامة للعام 2021-2022      <div data-aos="fade-up" data-aos-delay="600">
                    <div class="text-center text-lg-start">
                        <a href="{{url('appointment/home')}}" class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                            <span style="font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;"> احجز الأن      </span>
                            <i class="bi bi-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
                <img src="{{asset('site/assets/img/hero-img.png')}}" class="img-fluid" alt="">
            </div>
        </div>
    </div>

</section>

<main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about">

        <div class="container" data-aos="fade-up">
            <div class="row gx-0">

                <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
                    <div class="content a">
                        <h2 >  ماهي المفاضلة  </h2>
<font color="Blue">بخصوص المفاضلة للبكالوريا الفرعين العلمي والمهني بأفرعه (صناعة تجارة نسوي)<br>
بالبداية يجب ان نعلم بأنه دائماً لايوجد سوى مفاضلة واحدة فقط تصدرها وزارة التعليم العالي وهي احدى الطرق التي تتبعها الوزارة للقبول الجامعي (كليات ومعاهد) <br>
تقوم الوزارة باصدار المفاضلة وفق مرحلتين</font><br>

<font color="Purple">المرحلة الاولى</font><br>
تسمى (<font color="Purple">اعلان المفاضلة</font>) : ويتم من خلالها اصدار <font color="Purple">معدلات ابتدائية</font> للافرع الجامعية للكليات والمعاهد.
بعدها يسجل الطلاب رغباتهم بالترتيب (الترتيب مهم جداً) وفق فترة زمنية تحددها وزارة التعليم العالي <br>
<font color="DarkGreen">المرحلة الثانية</font><br>
تسمى (<font color="DarkGreen">نتيجة المفاضلة</font>) : تصدر من خلالها وزارة التعليم العالي <font color="DarkGreen">المعدلات النهائية </font>المطلوبة لكل فرع بالجامعة (كليات ومعاهد)
بعدها وفق رغبات الطالب الذي دونها بالمرحلة الاولى وبالترتيب يبدأ:
-هل علامته بالبكالوريا حقق المعدل النهائي للرغبة الاولى ؟
ان كان الجواب (نعم) : اذاً الرغبة الاولى مقبولة وهو الفرع الذي حصل عليه ويذهب لفرعه بالجامعة للتسجيل به وانتهى الامر.
اذا كان الجواب (لا) : اذاً الرغبة الاولى مرفوضة وينتقل للرغبة التالية
-هل علامته بالبكالوريا حقق المعدل النهائي للرغبة الثانية ؟
ان كان الجواب (نعم) : اذاً الرغبة الثانية مقبولة وهو الفرع الذي حصل عليه ويذهب لفرعه بالجامعة للتسجيل به وانتهى الامر.
اذا كان الجواب (لا) : اذاً الرغبة الثانية مرفوضة وينتقل للرغبة التالية
وهكذا بالترتيب حصراً حتى يصل الى الرغبة التي حقق <font color="DarkGreen">معدلها النهائي</font>...<br>






                    </div>
                </div>

                <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
                    <img src="{{asset('site/assets/img/about.jpg')}}" class="img-fluid" alt="">
                </div>

            </div>
        </div>

    </section><!-- End About Section -->

    <!-- ======= Values Section ======= -->
    <section id="values" class="values">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2> هدفنا راحتك  </h2>
                <p> عزيزي الطالب/ة يسرنا ان نقدم لك خدمتنا الالكترونية</p>
            </header>

            <div class="row">

                <div class="col-lg-4">
                    <div class="box" data-aos="fade-up" data-aos-delay="200">
                        <img src="{{asset('site/assets/img/values-1.png')}}" class="img-fluid" alt="">
                        <h3>   أدخل اولا للموقع و ضع رقم الاكتتاب</h3>
                        <p>  ثم اختر اليوم و الوقت الذي يناسبك لنخبرك فيما كان متاحا</p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0">
                    <div class="box" data-aos="fade-up" data-aos-delay="400">
                        <img src="{{asset('site/assets/img/values-2.png')}}" class="img-fluid" alt="">
                        <h3> مكانك أصبح باسمك</h3>
                        <p> تستطيع الأن القدوم للمركز الذي اخترته في الوقت الذي طلبته دون الانتظار لساعات
                            - نحن بخدمتك
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0">
                    <div class="box" data-aos="fade-up" data-aos-delay="600">
                        <img src="{{asset('site/assets/img/values-3.png')}}" class="img-fluid" alt="">
                        <h3> تحديد موعد التسجيل</h3>
                        <p> حدد التاريخ والوقت المناسب لك </p>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- End Values Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
        <div class="container" data-aos="fade-up">

            <div class="row gy-4">

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-emoji-smile"></i>
                        <div>
                            <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter"></span>
                            <h5>المواعيد  </h5>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-journal-richtext" style="color: #ee6c20;"></i>
                        <div>
                            <span data-purecounter-start="0" data-purecounter-end="{{$centers}}" data-purecounter-duration="1" class="purecounter"></span>
                            <h5>المراكز </h5>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-headset" style="color: #15be56;"></i>
                        <div>
                            <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1" class="purecounter"></span>
                            <h5>الايام المتبيقية   </h5>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-people" style="color: #bb0852;"></i>
                        <div>
                            <span data-purecounter-start="0" data-purecounter-end="{{$emps}}" data-purecounter-duration="1" class="purecounter"></span>
                            <h5>الموظفين  </h5>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Counts Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2>Features</h2>
                <p>ميزات موقعنا </p>
            </header>

            <div class="row">

                <div class="col-lg-6">
                    <img src="{{asset('site/assets/img/features.png')}}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-6 mt-5 mt-lg-0 d-flex">
                    <div class="row align-self-center gy-4">

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Eos aspernatur rem</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="300">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Facilis neque ipsa</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="400">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Volup amet voluptas</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="500">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Rerum omnis sint</h3>
                            </div>
                        </div>





                    </div>
                </div>

            </div> <!-- / row -->

            <!-- Feature Tabs -->
            <div class="row feture-tabs" data-aos="fade-up">
                <div class="col-lg-6 " >
                    <h3 class="a">ارشادات </h3>

                    <!-- Tabs -->
                    <ul class="nav nav-pills mb-3" dir="rtl">
                        <li>
                            <a class="nav-link active" data-bs-toggle="pill" href="#tab1"> ملاحظات هامة</a>
                        </li>
                        <li>
                            <a class="nav-link" data-bs-toggle="pill" href="#tab2">المفاضلات الخاصة غير المركزية </a>
                        </li>
                        <li>
                            <a class="nav-link" data-bs-toggle="pill" href="#tab3">السنة التحضيرية</a>
                        </li>


                    </ul><!-- End Tabs -->

                    <!-- Tab Content -->
                    <div class="tab-content justify-content-center" dir="rtl">

                        <div class="tab-pane fade show active justify" id="tab1">

                            <p> <h6>  - يجب  قراءة إعلان المفاضلة جيدا ً قبل التقدم إليها لمعرفة متطلبات القبول وشروطه
                            <br>
                            - يتقدم الطلاب السوريين ومن في حكمهم إلى مفاضلة الفرع العلمي ببطاقة مفاضلة واحدة فقط
                           <br> - يتقدم الطلاب السوريين ومن في حكمهم من حملة الشهادة الثانوية العامة (الفرع العلمي)لهذا العام إلى مفاضلة الموازي (للاختصاصات الأخرى عدا السنة التحضيرية)بعد صدور نتائج المفاضلة العامة<br>
                            - يتقدم الطالب من حملة الشهادة الثانوية للفرعين العلمي والأدبي  من (السوري غير المقيم – العرب والأجانب  – المعاقين جسميا ) إلى المفاضلة الخاصة بهم إضافة إلى تقدمهم إلى المفاضلات الأخرى التي يحق لهم التقدم إليها

قوا شروط القبول (العام – الموازي-محافظات شرقية) إلى التسجيل المباشر وفق الإعلان الخاص بالتسجيل المباشر<br>
 - يتقدم الطلاب السوريون ومن في حكمهم من حملة الشهادة الثانوية العامة (الفرع الأدبي )لهذا العام من (أبناء أعضاء الهيئة  التدريسية – ذوي الشهداء) إلى المفاضلة الخاصة بهم إضافة إلى تقدمهم إلى التسجيل المباشر للفرع الأدبي <br>
</h6></p>
                             </div><!-- End Tab 1 Content -->

                        <div class="tab-pane fade show justify-content-center" id="tab2">
<p><h5>
- بجامعة البعث : (التربية الموسيقية)كليات (العلوم الرياضيات بتدمر – معلم صف بتدمر – العلوم الكيمياء بتدمر)<br>

- معهد العلوم الشرعية والعربية بدمشق للثانوية الشرعية<br>
- معاهد وزارة التربية (التربية الرياضية – التربية الفنية التشكيلية والتطبيقية – التربية الموسيقية ) ويتم الاعلان عن شروطها من قبل وزارة التربية<br>

- لا يحق  للطلاب المقبولين في هذه المفاضلات بالتقدم إلى المفاضلة العامة والتسجيل المباشر للفرع الأدبي ويعد طلبهم ملغى حكما ويحق لهم التقدم إلى باقي المفاضلات
</h5></p>

                            </div><!-- End Tab 2 Content -->

                        <div class="tab-pane fade show justify-content-center" id="tab3">
                              <p>

                              <h4>	تهدف السنة التحضيرية المعتمدة كأساس للقبول في كليات  (الطب البشري- طب الأسنان- الصيدلة) إلى: </h4><br>
	تهيئة  طالب متميز قادر على التفوق وخدمة المجتمع بمهنية عالية
	قياس قدرات الطالب وتوجيهه إلى الاختصاص الذي يتناسب مع ميوله<br>
	تهيئة  الطالب للتواصل المباشر مع الوسط الجامعي<br>
	تحقيق مبدأ تكافؤ الفرص بين الطالب<br>
	تعد السنة التحضيرية من السنوات الدراسية للطالب<br>

    </p>
                            <div class="d-flex align-items-center mb-2">
                                <i class="bi bi-check2"></i>
                                <h4>نظام الدراسة والامتحانات في السنة التحضيرية</h4>
                            </div>
                            <p>- تعد السنة التحضيرية سنة من سنوات الدراسة المقررة للكليات الطبية <br>
 - اللغة الأجنبية المعتمدة للدارسة في السنة التحضيرية هي اللغة الانكليزية حصرا<br>
 - يتقدم الطالب في الفصل الأول إلى امتحانات مقررات هذا الفصل ويتقدم في الفصل الثاني إلى امتحانات مقررات الفصل الثاني <br>
 - يحسب معدل الطالب من أجل تحديد فرزه في نهاية السنة التحضرية على أساس نتائجه في مقررات الفصل الأول التي تقدم إليها في الفصل الأول  فقط ومقررات الفصل الثاني التي تقدم إليها في الفصل الثاني فقط (سواء نجح بها أو رسب بها أو لم يتقدم إلى امتحاناتها(
 </p>
                             </div><!-- End Tab 3 Content -->

                    </div>

                </div>

                <div class="col-lg-6">
                    <img src="{{asset('site/assets/img/features-2.png')}}" class="img-fluid" alt="">
                </div>

            </div><!-- End Feature Tabs -->

            <!-- Feature Icons -->
           <!-- End Feature Icons -->

        </div>

    </section><!-- End Features Section -->

    <!-- ======= Services Section ======= -->
    <!-- End Services Section -->



    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq">

        <div class="container" data-aos="fade-up" dir="rtl">

            <header class="section-header">
                <p> الاسئلة الشائعة  </p>
            </header>

            <div class="row">
                <div class="col-lg-6">
                    <!-- F.A.Q List 1-->
                    <div class="accordion accordion-flush " id="faqlist1">
                        <div class="accordion-item">
                            <h2 class="accordion-header" dir="rtl">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1">
                                   هل يستطيع الطالب ان يعدل بطاقة المفاضلة خلال فترةالمفاضلة ؟
                                </button>
                            </h2>
                            <div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                <div class="accordion-body">
                                نعم يمكن للطالب تعديل رغباته أكثر من مرة خلال  فترة التقدم للمفاضلة وذلك في المركز الخاص بتعديل الرغبات  في الجامعة التي سبق وتقدم فيها وذلك بعد تقديم طلب التعديل من قبل صاحب العلاقة أو من ينوب عنه إلى رئيس الجامعة وبعد حصوله على الموافقة يتم مراجعة مركز التعديل ببطاقة المفاضلة السابقة والممهورة من مركز التسجيل يقوم المركز المختص بتعديل الرغبات الكترونيا و تسليم النسخة المعدلة للطالب وفق الألية المعتمدة و يحتفظ بالنسخة الأخرى مرفقة بالطلب وبطاقة المفاضلة السابقة في المركز

                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-2">
                                    ماهي اجراءات التقدم للمفاضلة والتسجيل المباشر ؟
                                </button>
                            </h2>
                            <div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                <div class="accordion-body">
                                -  يتقدم الطالب شخصيا أو وكيله القانوني بموجب وكالة عامة او خاصة ويمكن ان يتقدم عن الطالب في حال عدم وجوده كل من (أب –أم- أخ –أخت –جد –جدة) بموجب تفويض خطي من الطالب أو المفوض بالنسبة للشهادات الثانوية غير السورية <br>
                                 -	يختار الطالب دورة الشهادة الثانوية التي اختارها وحصل عليها واللغة الأجنبية التي يرغب بالتسجيل على أساسها. <br>
-	يقوم موظف التسجيل بإدخال  الالكتروني لبيانات الطالب (فرع الشهادة الثانوية، رقم الاكتتاب ، دورة الشهادة الثانوية ومصدرها ، اللغة الأجنبية  التي اختارها الطالب ). <br>
-	يقوم موظف التسجيل بإدخال رغبات الطالب، ثم يضغط على زر حفظ الرغبات والمتابعة للتثبيت.

 - <br> يقوم موظف التسجيل بإدخال باقي بيانات الطالب (رقم الهاتف، الجوال، العنوان، الرقم الوطني للطالب) والبيانات الخاصة بكل مفاضلة.
<br>
-	بعد تأكد الطالب من صحة إدخال بياناته ورغباته يقوم موظف الإدخال بالتثبيت وطباعة بطاقة المفاضلة على نسختين أصليتين
<br>- يوقع الطالب على النسختين الأصليتين  و يلصق الطوابع المطلوبة عليهما، ويقوم الموظف بالتوقيع عليهما وتسجيلهما وختمهما بالأختام الرسمية
<br> - يسلم الموظف للطالب نسخة ملصق عليها الطوابع إشعارا بالاشتراك في هذه المفاضلة  ويحتفظ الطالب بهذه النسخة لابرازها عند الضرورة
<br>- يحتفظ بالنسخة الثانية للمفاضلة العامة في الجامعة مرفقة بصورة عن البطاقة الشخصية للطالب أو إخراج القيد.
 <br>- تحفظ النسخة الثانية من بطاقة المفاضلة العامة للطلاب  العرب وأبناء المواطنات السوريات في الجامعة.
<br>- ترسل النسخة الثانية من بطاقة المفاضلة الخاصة ب بناء أعضاء الهيئة التدريسية ، وذوي الشهداء والجرحى والمفقودين ،المعاقين جسميا إلى الوزارة مرفقة ً


                            </div>
                            </div>
                        </div>

                        <div class="accordion-item" dir="rtl">
                            <h2 class="accordion-header "dir="rtl" >
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-3" dir="rtl">
                                ماهي الاختصاصات التي يتم القبول فيها على أساس العلامةالاختصاصية وآليةالقبول
                                 عند تساوي العلامة في المفاضلة العامة والموازي؟
                                </button>
                            </h2>
                            <div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                <div class="accordion-body">
                                -  كل طالب حصل على درجة اختصاصية أعلى من درجة الحد الأدنى المعلنة للقبول للعلامة الاختصاصية فلا ينظر إلى المجموع <br>
- كل طالب حصل على الحد الأدنى للقبول على أساس العلامة الاختصاصية في أقسام كليات  الاداب (اللغة العربية – اللغة الإنكليزية - اللغة الفرنسية- اللغة الروسية)وأقسام كلية العلوم (الرياضيات – الفيزياء- الكيمياء – علم الحياة – الإحصاء الرياضي  ) ينظر إلى المجموع العام للدرجات في الشهادة الثانوية بعد طي درجة مادة التربية الدينية وإحدى اللغتين، ويقبل من حاز على المجموع الأعلى
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-6">

                    <!-- F.A.Q List 2-->
                    <div class="accordion accordion-flush" id="faqlist2">

                        <div class="accordion-item" dir="rtl">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq2-content-1">
                                ماهي المواد التي سينظر إليها عند تساوي الحد الأدنى  للقبول (الفرع العلمي)؟
                                </button>
                            </h2>
                            <div id="faq2-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist2">
                                <div class="accordion-body">
                                آلية القبول عند التساوي في الحد الأدنى لدرجات القبول في المفاضلة العامة : <br>
-  كل طالب حصل على مجموع أعلى من الحد الأدنى المعلن للقبول في أي اختصاص ومحققا ً لشروطه يقبل دون النظر إلى الحد الأدنى للمواد المذكورة<br>
-  كل طالب حصل على الحد الأدنى المعلن للقبول في أي اختصاص يتم النظر إلى المواد وفق التسلسل الوارد في الجدول و على النحو التالي : <br>
o	إذا كان الطالب حاصلا على علامة أعلى من الحد الأدنى المعلن للمادة الأولى يقبل دون النظر إلى باقي المواد. <br>
o	إذا كان الطالب حاصلا على الحد الأدنى المعلن للمادة الأولى فيتم النظر إلى المادة الثانية وإذا كان حاصلا على علامة أعلى في هذه المادة لا ينظر إلى المادة الثالثة وهكذا....
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header ">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq2-content-2">
                                ما شروط تسوية الأوضاع  للطلاب المرفوضة رغباتهم في المفاضلة ؟
                                </button>
                            </h2>
                            <div id="faq2-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist2">
                                <div class="accordion-body">
                                                    الطالب الذين رفضت جميع رغباتهم بنتيجة المفاضلات (العامة- الموازي- السوري غير المقيم- العرب و الأجانب) ممن دونوا عدد الرغبات المحددة ً لتسوية الأوضاع والواردة في اعلان كل مفاضلة يتقدمون بطلب يحدد موعده لاحقا للقبول في الكلية أو المعهد الذي يرغبون إذا حققوا الحدود الدنيا المعلنة للقبول والشروط المطلوبة<br>
 يتقدم هؤلاء الطلاب بطلباتهم الكترونيا ً في مديرية شؤون الطالب المركزية في إحدى الجامعات وضمن المواعيد التي ستحدد باعلان النتائج.
<br>
 لا يجوز تسوية أوضاع الطالب المرفوضة رغباتهم بنتيجة مفاضلتي  العامة والموازي على الحدود الدنيا المعلنة للقبول في النسب  المخصصة  لأبناء المحافظات في كليات  فروع الجامعات و كليات جامعة الفرات.
<br>
 لا يتم تسوية أوضاع الطلاب على رغبات منح الجامعات الخاصة أو المفاضلات الخاصة (ذوي الشهداء – أبناء أعضاء الهيئة التدريسية)
<br>
 لا يسوى وضع الطالب في الكليات والمعاهد ومدارس التمريض التي تتضمن اختبار أو مسابقة ما لم يكن ناجحا في الاختبارات أو المسابقات المطلوبة ويستثنى من هذا الشرط الذين تم تعديل درجاتهم في الشهادة الثانوية السورية بقرار من وزارة التربية ولم تكن درجاتهم تخولهم الاشتراك في الاختبارات أو المسابقات المطلوبة وسيتم إجراء هذه الاختبارات أو المسابقات من قبل لجان خاصة بهم في الكلية أو المعهد المراد القبول فيه.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq2-content-3">
                                    كم رغبة يمكن ان يدونها الطالب في بطاقة المفاضلة للفرع العلمي ؟
                                </button>
                            </h2>
                            <div id="faq2-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist2">
                                <div class="accordion-body">
                                يتاح للطالب أن يدون في بطاقة المفاضلة /45 /رغبة للفرع العلمي كحد أقصى، منها /35 / رغبة كحد أقصى للقبول العام والمحافظات الشرقية ومفاضلتي ذوي الشهداء وأبناء أعضاء الهيئة التدريسية وفق ما هو متاح له، و/10 /رغبات كحد أقصى لمنح الجامعات الخاصة. يمكن للطالب في كل نوع منها أن يدون رغبة أو أكثر أو ألا يدون أي رغبة كأن يختار فقط قبول عام أو يختار فقط منح جامعات خاصة أو كلا القبولين وفق ( الخيارات الموجودة في برنامج التقدم للمفاضلة)                                 </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </section><!-- End F.A.Q Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2>Portfolio</h2>
                <p>معرض الصور </p>
            </header>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">1</li>
                        <li data-filter=".filter-card">2</li>
                        <li data-filter=".filter-web">3</li>
                    </ul>
                </div>
            </div>

            <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/11.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/11.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title=""><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/2.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/2.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="Web 3"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/3.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/3.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="App 2"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/4.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/4.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="Card 2"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/5.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/5.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="Web 2"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/6.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/6.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="App 3"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/7.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/7.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="Card 1"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/8.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/8.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="Card 3"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="{{asset('site/assets/img/portfolio/9.PNG')}}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <p>...</p>
                            <div class="portfolio-links">
                                <a href="{{asset('site/assets/img/portfolio/9.PNG')}}" data-gallery="portfolioGallery" class="portfokio-lightbox" title="Web 3"><i class="bi bi-plus"></i></a>
                                <a href="#" title="More Details"><i class="bi bi-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->


</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer" class="footer">

    <div class="footer-newsletter">
        <div class="container">
        <div class="row justify-content-center">
<div class="col-md-6 text-center">

    <h4>  تحميل  </h4>
                    <h5>دليل الطالب للقبول الجامعي </h5>
                    <a href="{{url('file-download')}}" class="btn btn-success "> تحميل دليل القبول  </a>

 </div>
 <div class="col-md-6 text-center">

 <h4>  تحميل  </h4>
                    <h5>   تطبيق المفاضلة </h5>
                    <a href="{{url('app-download')}}" class="btn btn-success "> تحميل  التطبيق   </a>

 </div>
 </div>

        </div>
    </div>






    <div class="footer-top">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-5 col-md-12 footer-info">
                    <a href="{{url('/')}}" class="logo d-flex align-items-center">
                        <img src="{{asset('app-assets/img/gallery/logo1.png')}}" alt="">

                        <span>جامعة طرطوس</span>
                    </a>
                    <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus.</p>
                    <div class="social-links mt-3">
                        <a href="https://webmail.tartous-univ.edu.sy/" class="twitter"><i class="bi bi-twitter"></i></a>
                        <a href="https://www.facebook.com/%D8%AC%D8%A7%D9%85%D8%B9%D8%A9-%D8%B7%D8%B1%D8%B7%D9%88%D8%B3-Tartous-University-428744704243253/" class="facebook"><i class="bi bi-facebook"></i></a>
                        <a href="https://webmail.tartous-univ.edu.sy/" class="instagram"><i class="bi bi-instagram bx bxl-instagram"></i></a>
                        <a href="http://tartous-univ.edu.sy/admin/posts.rss" class="linkedin"><i class="bi bi-linkedin bx bxl-linkedin"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>روابط ذات صلة  </h4>
                    <ul>
                        <li><i class="bi bi-chevron-right"></i> <a href="http://tartous-univ.edu.sy/tartous-university/ar/index">  موقع جامعة طرطوس </a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="http://www.mohe.gov.sy/mohe/"> موقع وزارة التعليم العالي  </a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="http://www.mof.sy/"> موقع القبول الجامعي   </a></li>

                    </ul>
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>خدماتنا  </h4>
                    <ul>
                        <li><i class="bi bi-chevron-right"></i> <a href="#values"> الخدمات </a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#faq">الاسئلة المتكررة </a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#features"> ميزات  وارشادات  </a></li>
                    </ul>
                </div>



            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>Tartous University</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

                Developed by <a href="#">ICTE</a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="{{asset('site/assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
<script src="{{asset('site/assets/vendor/aos/aos.js')}}"></script>
<script src="{{asset('site/assets/vendor/php-email-form/validate.js')}}"></script>
<script src="{{asset('site/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
<script src="{{asset('site/assets/vendor/purecounter/purecounter.js')}}"></script>
<script src="{{asset('site/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('site/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{asset('site/assets/js/main.js')}}"></script>

</body>

</html>
