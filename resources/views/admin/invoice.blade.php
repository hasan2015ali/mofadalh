<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style>
        body{
            font-family: Arial, sans-serif;
            font-size: 17px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: right;
            padding: 8px;
        }
        .col-md-4{
            width: 45%;
        }

    </style>
</head>


<body class="deep-purple-skin">
    <div class="row">
        <div class="col-md-4 col-4 pull-right" dir="rtl">
            <p>بازار</p>
            <p>فاتورة طلبية</p>
        </div>

        <div class="col-md-4 col-4" dir="rtl">
            <img src="{{asset('app-assets\img\ico\logo.png')}}" width="20%" height="20%">
        </div>

        <div class="col-md-4 col-4 pull-left" dir="ltr">
            <p>TU</p>
            <p>Purchase Invoice</p>
        </div>
    </div>
    <hr>
    <br>

    <div class="row" dir="rtl">
        <div class="col-md-4 col-4 pull-right">
            <p> الاسم: {{$data['order']->full_name}}</p>

            {{--@if($data['order']->deliver_type == 2 && $data['order']->delivery_at != '')--}}
                {{--@php--}}
                    {{--$deliverDate =   date("H:i l j F Y", strtotime($data['order']->delivery_at));--}}
                {{--@endphp--}}
                {{--<p> وقت الاستلام: {{$deliverDate}}</p>--}}

            @if($data['order']->deliver_type == 0 && $data['order']->deliver_end_time != '' && $data['order']->delivery_at != '')
                @php
                    $deliverDate =   date("l j F Y H:i", strtotime($data['order']->delivery_at));
                    $endDate =   date("l j F Y H:i", strtotime($data['order']->deliver_end_time));
                @endphp
                <p> وقت الاستلام الأول: {{$deliverDate}}</p>
                <p> وقت الاستلام الثاني: {{$endDate}}</p>

            @elseif($data['order']->deliver_type == 1 && $data['order']->delivery_at != '')
                @php
                    $deliverDate =   date("l j F Y H:i", strtotime($data['order']->delivery_at));
                @endphp
                <p> وقت التوصيل السريع: {{$deliverDate}}</p>

                @else
                <p> </p>

            @endif

            <p> المنطقة: {{$data['order']->region ?? ''}}</p>
            <p> العنوان: {{$data['order']->region_address ?? ''}}</p>
        </div>

        <div class="col-md-4 col-4 pull-up"></div>

        <div class="col-md-4 col-4 pull-left">
            <p> المدينة: {{$data['order']->city ?? ''}}</p>
            <p> الشارع: {{$data['order']->street ?? ''}}</p>
            <p> الرمز البريدي: {{$data['order']->zip_code ?? ''}}</p>
        </div>

    </div>
<hr>
    <div class="row" dir="rtl">
        <div class="col-md-12">
            <div class="container">
                <div class="table-responsive">
                <table class="table table-striped table-responsive">
                    <thead  class="thead-dark">
                    <tr>
                        <th width="10%">#</th>
                        <th width="10%">الاسم</th>
                        <th width="10%">السعر</th>
                        <th width="10%">اسم الوحدة</th>
                        <th width="10%">الكمية</th>
                        <th width="10%">الإجمالي</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(count($data) >0)
                                @php $i = 0; @endphp
                            @foreach($data['products'] as $item)
                                @php $i +=1; @endphp

                            <tr>
                    <td> {{$i}} </td>
                    <td> {{$item->product_name }} </td>
                    <td> {{$item->pr_price }} </td>
                    <td> {{$item->type_a }} </td>
                    <td> {{$item->p_amount }} </td>
                    <td> {{$item->pr_full_price }} </td>
                            </tr>

                            @endforeach
                        @endif
                    </tbody>

                </table>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <div class="row text-center" dir="rtl">
        <p> السعر الإجمالي: {{$data['order']->total}}</p>
        {{--<p> سعر التوصيل: {{$data['order']->deliv_coast}}</p>--}}
        {{--@php--}}
            {{--$sum = $data['order']->total + $data['order']->deliv_coast--}}
        {{--@endphp--}}
        {{--<p> الكلفة الإجمالية: {{$sum}}</p>--}}
    </div>
    <div class="col-md-12">
        <center>
        </center>
    </div>

</body>
</html>
