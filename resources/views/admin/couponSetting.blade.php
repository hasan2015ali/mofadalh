@extends("admin_layout")
@section('content')


<section id="input-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <p>
                      إعدادات الكوبونات
                    </p><br>
                        <div class="row">
                            <div class="col-sm-12" style="overflow-x:auto;" >
                                <table id="tableData" class="table table-striped table-sm data-table">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> نسبة الحسم </th>
                                        <th> مدة الصلاحية </th>
                                        <th> العمليات </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog " role="document">
    <div class="modal-content">
        <div class="modal-header">
            <label class="modal-title text-text-bold-600" id="myModalLabel33">   المعلومات   </label>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-times"></i></span>
            </button>
        </div>
                    <form action="#" method="post" id="editFromData" >
                        @csrf
                        <input type="hidden" name="_id" id="_id">
                        <div class="modal-body">
                            
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <label>نسبة الحسم  </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="discountRate" id="discountRate">
    
                                    </div>
                                </div>
                                    <div class="col-md-4">
                                     <label>مدة الصلاحية</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="expiry" id="expiry">
    
                                    </div>
    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10" id="coupon">
    
                                </div>
                            </div>
    
    
                        </div>
                        <div class="modal-footer">
                            <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                            <input type="submit" name="saveBtn" id="saveBtn" class="btn btn-primary" value="حفظ">
    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    

@endsection


@push('myjs')

    <script type="text/javascript">

        $(function () {


            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

            ajax: "{{ route('index') }}",

            columns: [

                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'discountRate', name: 'discountRate'},
                {data: 'expiry', name: 'expiry'},
                {data: 'action', name: 'action', orderable: false, searchable: false},

]

}); // fill select delegate

            $('body').on('click', '.editProduct', function () {

            var coupon_id = $(this).data('id');

            $.get("{{ url('coupon') }}" + '/' + coupon_id , function (data) {

                $("#saveBtn").val("حفظ");
                $('#_id').val(data.id);
                $('#discountRate').val(data.discountRate);
                $('#expiry').val(data.expiry);
                $("#inlineForm").modal('show');
            })


            }) ;// end getCouponInfo function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();
                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var coupon_id = $("#_id").val();
                
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url :"{{ url('updateCoupon') }}"+ '/' + coupon_id ,

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);
                        $('#editFromData').trigger("reset");
                        showSuccesFunction();
                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data


});



    </script>

@endpush
