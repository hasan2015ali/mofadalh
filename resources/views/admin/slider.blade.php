@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                السلايدر الاعلاني
                            </p>
                            <br>
                            <button type="button" id="addClick" class="btn gradient-purple-bliss">إضافة</button>



                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th> المنتج</th>

                                            <th> الصورة   </th>

                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل بيانات المنتج  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
               <div class="container">
                   <form action="{{route('slider.store')}}" method="post" id="addForm">
                       <input type="hidden" name="_id" id="_id" >
                       <div class="row">
                           @csrf

                           <div class="col-sm-6">
                            <br>
                               <fieldset class="form-group">
                                <label >اختر صورة </label>
                                <div class="needsclick dropzone" id="document-dropzone">
                                    <br> </div>
                                   <label for="roundText">  صورة العرض  </label>
                                   <div id="image_edit">
                                   </div>
                                   <br>



                               </fieldset>
                           </div>

                           <div class="col-sm-6">
                               <br>
                               <fieldset class="form-group">
                                   <label for="roundText">   المنتج    </label>
                                    <select name="product_id" id="product_id" class="form-control select2">
                                        @foreach ($products as $product )
                                        <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach
                                    </select>
                               </fieldset>
                           </div>

                           <div class="col-sm-12">
                               <!-- Draggable cards section start -->
                               <section id="draggable-cards1">
                                   <div class="row match-height" id="card-drag-area">

                                   </div>
                               </section>
                               <!-- // Draggable cards section end -->
                               <fieldset class="form-group">


                                   <br>
                                   <button type="button" id="addFormBtn" class="btn gradient-purple-bliss">حفظ</button>
                               </fieldset>
                           </div>

                       </div>
                   </form>
               </div>
            </div>
        </div>
    </div>

@endsection

@push('myjs')
    {{--dropzone--}}
    <script type="text/javascript">

var uploadedDocumentMap = {}
    var alesDropZone  =   new Dropzone( "#document-dropzone", {
        url: '{{ route('projects.storeMedia',["table"=>"helps"]) }}',
        maxFiles:1,
        maxFilesize: 5, // MB
        addRemoveLinks: true,
         headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        acceptedFiles: "image/*",
        processing:function(){
            $('#saveBtn').prop('disabled',true);
            toastr.options.positionClass = 'toast-top-center';
            toastr.success('انتظر اكتمال التحميل');
        },
        init: function() {
            this.on('addedfile', function(file) {
             if (this.files.length > 1) {
            this.removeFile(this.files[1]);
            toastr.options.positionClass = 'toast-top-center';
            toastr.success('لايمكن رفع اكثر من ملف');}
      });


        },
        success: function (file, response) {
            $('#addForm').append('<input type="hidden" name="offer_image" value="' + response.name + '">')

            uploadedDocumentMap[file.name] = response.name
            $('#saveBtn').prop('disabled',false);
        },
        removedfile: function (file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedDocumentMap[file.name]
            }
            $('#addForm').find('input[name="offer_image"][value="' + name + '"]').remove()
        },

    });



        $(function () {

            $('body').on('click', '#addClick', function (e)
             {
                e.preventDefault();
                $("#_id").val('');
                $('#editFromData').trigger("reset");
                $("#saveBtn").val("إضافة");
                $("#inlineForm").modal("show");
            });

            var table = $('.data-table').DataTable({
                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('slider.index')}}",

                columns: [

                    {data: 'DT_RowIndex', name: 'id'},

                    {data: 'product.name', name: 'product.name'},

                    {data: 'image', name: 'image'},


                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });




            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('slider.store') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                            $("#addFormBtn").html(' حفظ');
                            $("#addFormBtn").attr('disabled',false);

                            $('#addForm').trigger("reset");

                            showSuccesFunction();
                            $("#inlineForm").modal('hide');

                            table.draw(false);

                            // table.row.add(data.data).draw( false );
                        } else{
                            showErrorFunction();
                            $("#addFormBtn").attr('disabled', false);
                            $("#addFormBtn").html(' حفظ');
                        }
                    },

                    error: function (data) {
                        $("#addFormBtn").html(' حفظ');
                        $("#addFormBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end add new record




            //soft delete
            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                var co = confirm("  هل أنت متأكد من الحذف !");
                if (!co) {
                    return;
                }


                $.ajax({

                    type: "DELETE",

                    url: "{{ route('slider.index')}}"  +"/"+ product_id,

                    data:{
                        '_token':'{{csrf_token()}}'
                    },

                    success: function (data) {
                        Swal.fire({title:"  تم الحذف بنجاح   ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        table.draw(false);

                    },

                    error: function (data) {
                        Swal.fire({title:" حدث خطأ ما ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        console.log('خطأ:', data);
                    }

                });

            });
        });



    </script>

@endpush
