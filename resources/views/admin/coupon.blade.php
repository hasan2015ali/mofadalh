@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                أكواد الخصم
                            </p>
                            <br>
                            @can('coupon-create')
                            <div class="row" style="margin: 20px;">
                                <button type="button" id="addClick" class="btn btn-primary">إضافة</button>

                            </div>
                            @endcan
                            <br>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table id="tableData" class="table table-striped table-sm data-table">
                                        <thead>
                                        <tr>
                                            <th> #</th>
                                            <th> الاسم </th>
                                            <th> الكود </th>
                                            <th> نسبة الحسم% </th>
                                            <th> تاريخ الانتهاء </th>
                                            <th> العمليات </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="title">title  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>

                    </button>
                </div>
                <form action="#" method="post" id="editFromData" >
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">

                        <div class="row" style="margin-right: 5px;">
                            <div class="row">
                            <div class="col-md-6">
                                <label>الاسم  </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" id="name">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>الكود  </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="code" id="code">

                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <label>نسبة الحسم%  </label>
                                <div class="form-group">
                                    <input type="number" class="form-control" name="discount" id="discount">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>تاريخ الانتهاء </label>
                                <div class="form-group">
                                    <input type="text" class="form-control datepicker" name="expiry" id="expiry" >

                                </div>
                            </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" name="saveBtn" id="saveBtn" class="btn btn-primary" value="حفظ">

                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection

@push('myjs')
    {{--dropzone--}}
    <script type="text/javascript">

        $(function () {

        $("#addClick").click(function (e) {
            e.preventDefault();
            $("#saveBtn").html("إضافة");
            $("#title").html("إضافة كود");
            $('#_id').val('') ;
            $('#editFromData').trigger("reset");
            $("#inlineForm").modal("show");

        });
            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('coupons.index') }}",

                columns: [

                    {data: 'DT_RowIndex', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'code', name: 'code'},
                    {data: 'discount', name: 'discount'},
                    {data: 'expiry', name: 'expiry'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });




            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');
                $("#title").html("تعديل كود");

                $.get("{{ route('coupons.index') }}" + '/' + product_id + '/edit', function (data) {


                    $("#saveBtn").val("حفظ");

                    $('#_id').val(data.id);
                    $('#name').val(data.name);
                    $('#code').val(data.code);
                    $('#expiry').val(data.expiry);
                    $('#discount').val(data.discount);
                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الحفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                var url = "{{ route('coupons.store') }}";

                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                       if(data.status==200){
                        $('#editFromData').trigger("reset");

                    showSuccesFunction();

                    $("#inlineForm").modal('hide');

                    table.draw(false);
                       }
                       else if(data.status==201){
                        showErrorFunctionCustom(data.message);
                       }


                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data



            $('body').on('click', '.delete', function () {
                var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('coupons.index') }}"+ '/' + product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
             showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });

            }); // end delete row


        });

    </script>

@endpush
