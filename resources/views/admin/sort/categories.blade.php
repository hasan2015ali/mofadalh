@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                            عرض ترتيب الفئات في التطبيق
                            </p>
                            <div class="row">


                                    <div class="col-sm-9">
                                        <fieldset class="form-group">

                                            {{-- <button type="button" id="saveBtn" class="btn gradient-purple-bliss">حفظ</button> --}}

                                        </fieldset>
                                    </div>

                            </div>
                            <form action="" id="formData">
                            <div class="row">

                                @csrf
                                <div class="col-sm-12" id="sortable-lists">
                                    <ul class="list-group" id="basic-list-group2">
                                       @foreach($data as $item)
                                        <li class="list-group-item draggable">
                                            <input type="hidden" name="sort_order[]" value="{{$item->id}}">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h5 class="mt-0">{{$item->name}}</h5>

                                                </div>
                                            </div>
                                        </li>
                                      @endforeach


                                    </ul>
                                </div>


                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@push('myjs')

    <script type="text/javascript">


                function autoSort(){
                    $.ajax({

            data: $("#formData").serialize(),

            url: "{{ route('sort.store',['table'=>'categories']) }}",

            type: "POST",

            dataType: 'json',
            timeout:4000,
            success: function (data) {
                $("#saveBtn").html('حفظ');
                toastr.options.positionClass = 'toast-top-left';
                toastr.success('تم الحفظ بنجاح');


},

            error: function (data) {
                $("#saveBtn").html('حفظ');

                toastr.warning(data.success);
            }

            });
                }


        $(function () {

   

            $("#saveBtn").click(function (e) {
                e.preventDefault();
                autoSort();

                $("#saveBtn").html('<i class="fa fa-load"></i> ... ');
               
            }); // end add new record




            var drake = dragula([document.getElementById('basic-list-group2')]);

            drake.on('drop', function(el, target, source, sibling) {
  
          autoSort();
          
            });

        });

    </script>

@endpush