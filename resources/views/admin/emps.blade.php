@extends("admin_layout")
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                 الموظفين
                            </p>
                            @can('category-create')
                                <div class="row" style="margin: 20px;">
                                    <button type="button" id="addClick" class="btn btn-primary">إضافة</button>

                                </div>
                            @endcan
                            <br>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th>الاسم</th>
                                            <th>البريد الالكتروني  </th>
                                            <th> رقم الهاتف</th>


                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="title">title  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>

                    </button>
                </div>
                <form action="#" method="post" id="editFromData">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <div class="row">


                            <div class="col-md-6"> <label>  الاسم    </label>
                                <div class="form-group">
                                    <input type="text" name="name" id="name_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label> رقم الهاتف   </label>
                                <div class="form-group">
                                    <input type="text" name="phone" id="phone_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label>البريد الالكتروني</label>
                                <div class="form-group">
                                    <input type="text" name="email" id="email_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label>كلمة السر  </label>
                                <div class="form-group">
                                    <input type="text" name="password" id="password_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveBtn" class="btn btn-primary" value="حفظ">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('myjs')


        <script type="text/javascript">

            $(function () {

            $("#addClick").click(function (e) {
                e.preventDefault();
                $("#saveBtn").html("إضافة");
                $("#title").html("إضافة  ");
                $('#_id').val('') ;
                $('#editFromData').trigger("reset");
                $("#inlineForm").modal("show");


                $("#card-drag-area").html("");
            });
            var table = $('.data-table').DataTable({

            destroy: true,
            processing: true,

            serverSide: true,
            stateSave: true,

            ajax: "{{ route('emps.index') }}",

            columns: [

        {data: 'DT_RowIndex', name: 'id'},

        {data: 'name', name: 'name'},
        {data: 'email', name: 'email'},
        {data: 'phone', name: 'phone'},


        {data: 'action', name: 'action', orderable: false, searchable: false},

            ]

        });




            $('body').on('click', '.edit', function () {

            var product_id = $(this).data('id');
            $("#title").html("تعديل مدير المركز");

            $.get("{{ route('emps.index') }}" + '/' + product_id + '/edit', function (data) {




            $("#saveBtn").val("حفظ");

            $('#_id').val(data.id);
            $('#name_edit').val(data.name);
            $('#phone_edit').val(data.phone);
            $('#email_edit').val(data.email);


            $("#inlineForm").modal('show');
        })


        }) ;// end edit function;

            $("#saveBtn").click(function (e) {
            e.preventDefault();

            $("#saveBtn").html('جاري الخفظ ..');
            $("#saveBtn").attr('disabled',true);
            var product_id = $("#_id").val();
            var url = "{{ route('emps.store') }}";
             {{--if(product_id){--}}
             {{--   url = "{{ route('emps.index') }}"+ '/' + product_id + '/update'--}}
             {{--}--}}
            $.ajax({

            data: $('#editFromData').serialize(),

            url: url,

            type: "POST",

            dataType: 'json',
            timeout:4000,
            success: function (data) {
            $("#saveBtn").html(' حفظ');
            $("#saveBtn").attr('disabled',false);

            $('#editFromData').trigger("reset");

            showSuccesFunction();

            $("#inlineForm").modal('hide');

            table.draw(false);

            // table.row.add(data.data).draw( false );

        },

            error: function (data) {
            $("#saveBtn").html(' حفظ');
            $("#saveBtn").attr('disabled',false);

            showErrorFunction();
            $("#inlineForm").modal('hide');

        }

        });
        }); // end save  record data



            $('body').on('click', '.delete', function () {
            var product_id = $(this).data("id");

            sweetConfirm( function (confirmed) {
            if (confirmed) {
            $.ajax({

            type: "DELETE",

            url: "{{ route('emps.index') }}"+ '/' + product_id,
            data:{
            '_token':'{{csrf_token()}}'
        },
            success: function (data) {
            showSuccesFunction();
            table.draw(false);

        },

            error: function (data) { }

        });   }

        });

        }); // end delete row


        });

    </script>
@endpush
