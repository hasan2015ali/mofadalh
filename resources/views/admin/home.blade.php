@extends("admin_layout")
@section('content')

    <style>
       a{color: #343A40}
    </style>

    <section id="input-style">
        <div class="row">
            <div class="col-12" style="overflow-x:auto;">


                <div class="row">

                    <div class="col-xl-3 col-lg-6 col-12">
                        <a href="#">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="mb-1 danger">{{$accepted_users ?? ''}}</h3>
                                                <span>   المواعيد المسجلة اليوم  </span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="fa fa-child danger font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                        @if(Auth::user()->role != 5)
                    <div class="col-xl-3 col-lg-6 col-12">
                        <a href="">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="mb-1 danger">{{$today_products ?? ''}}</h3>
                                                <span>    مركز</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="fa fa-building danger font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif

                    <div class="col-xl-3 col-lg-6 col-12">
                        <a href="#">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="mb-1 danger">{{$users ?? ''}} </h3>
                                                <span> موظف    </span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="fa fa-user-circle danger font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>



                </div>



            </div>
        </div>
    </section>


@endsection

