@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                العروض
                            </p>


                            <div class="row justify-content-center">

                                <div class="col-sm-3 col-3">
                                    <fieldset class="form-group">
                                        <label for="roundText">  الفئة  </label>
                                        <select id="test" class="form-control" style="width: 100%"></select>

                                    </fieldset>
                                </div>

                                <div class="col-sm-1 col-1">
                                    <fieldset class="form-group">
                                        <br>
                                   <button class="btn btn-success" id="filterBtn">   <i class="fa fa-filter"></i> </button>

                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th>اسم المنتج</th>

                                            <th>الفئة  </th>

                                            <th>صورة العرض </th>
                                            <th>سعر العرض </th>

                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل بيانات المنتج  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
               <div class="container">
                   <form action="{{route('products.store')}}" method="post" id="addForm">
                       <input type="hidden" name="_id" id="_id" >
                       <div class="row">
                           @csrf

                           <div class="col-sm-6">
                            <br>
                               <fieldset class="form-group">
                                <label >اختر صورة </label>
                                     <div class="needsclick dropzone" id="document-dropzone2">
                                    <br> </div>
                                   <label for="roundText">  صورة العرض  </label>
                                   <div id="image_edit">
                                   </div>
                                   <br>



                               </fieldset>
                           </div>

                           <div class="col-sm-6">
                               <br>
                               <fieldset class="form-group">
                                   <label for="roundText">  سعر العرض  </label>
                                   <input type="text" id="offer_price" name="offer_price" class="form-control round" placeholder="  ادخل سعر العرض">

                               </fieldset>
                           </div>

                           <div class="col-sm-12">
                               <!-- Draggable cards section start -->
                               <section id="draggable-cards1">
                                   <div class="row match-height" id="card-drag-area">

                                   </div>
                               </section>
                               <!-- // Draggable cards section end -->
                               <fieldset class="form-group">


                                   <br>
                                   <button type="button" id="addFormBtn" class="btn gradient-purple-bliss">حفظ</button>
                               </fieldset>
                           </div>

                       </div>
                   </form>
               </div>
            </div>
        </div>
    </div>


@endsection

@push('myjs')
    {{--dropzone--}}
    <script type="text/javascript">

        $("#main_cat").select2({
            theme: "classic",
            dir: "rtl"
        });

        // Dropzone.autoDiscover = false;

        var uploadedDocumentMap2 = {};
        var ales2DropZone  =   new Dropzone( "#document-dropzone2", {
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('#addFormBtn').attr('disabled',false);
                $('#addForm').append('<input type="hidden" name="offer_image" value="' + response.name + '">')
                uploadedDocumentMap2[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                }
                $('#addForm').find('input[name="offer_image"][value="' + name + '"]').remove()
            },

            url: '{{ route('projects.storeMedia',['table'=>"products"]) }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            timeout:0,
            acceptedFiles: ".jpg, .png",
            processing:function () {
                $('#addFormBtn').attr('disabled',true);
                toastr.options.positionClass = 'toast-top-center';
                toastr.warning('انتظر اكتمال التحميل');


            },
            init: function() {
                this.on( 'addedfile', function (file) {
                    if(this.files.length > 1){
                        this.removeFile(this.files[1]);
                        toastr.options.positionClass = 'toast-top-center';
                        toastr.warning('لا يمكن رفع أكثر من ملف');
                    }
                } );
            }

        } );


        $(function () {

            var table = $('.data-table').DataTable({
                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('offers.index')}}",

                columns: [

                    {data: 'DT_RowIndex', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'cat', name: 'cat'},
                    {data: 'offer_image', name: 'offer_image'},
                    {data: 'offer_price', name: 'offer_price'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });

            $('body').on('click', '.edit', function () {

                $("#addFormBtn").html('تعديل');
                ales2DropZone.removeAllFiles( true );
                $("#card-drag-area").html("");

                var product_id = $(this).data('id');

                $.get("{{ route('offers.index') }}" + '/' + product_id + '/edit', function (data) {
                    $("#addFormBtn").html('تعديل');

                    $('#_id').val(data.id);

                    $('#offer_price').val(data.offer_price);

                    $("#image_edit").html('');

                    if(data.offer_image != null && data.offer_image.length > 5) {

                        var offer_image = data.offer_image;

                        var basic_list_group2 = "";
                        var asset_url2 = '{{asset('/storage/')}}';

                        basic_list_group2 += "<div class=\"col-xl-3 col-md-6 col-12\">\n" +
                            "                                           <div class=\"card draggable\">\n" +
                            "                                               <div class=\"card-header\">\n" +
                            "                                                   <h4 class=\"card-title\">\n" +
                            "                                    <input type=\"hidden\" name=\"sort_order[]\" value='" + offer_image + "'>\n" +

                            "                                    <input type=\"hidden\" name=\"offer_image\" value='" + offer_image + "'>\n" +

                            "                                                       <p class=\"m-0\">\n" +
                            "                                                           <img  id='" + offer_image + "' src='" + asset_url2 +"/"+ offer_image + "' width=\"100\" height=\"100\">\n" +
                            "                                                       </p>\n" +
                            "\n" +
                            "                                                   </h4>\n" +
                            "                                               </div>\n" +
                            "                                               <div class=\"card-content\">\n" +
                            "                                                   <div class=\"card-body\">\n" +
                            "\n" +
                            "                                                   </div>\n" +
                            "                                               </div>\n" +
                            "                                           </div>\n" +
                            "                                       </div>";

                        $("#image_edit").html(basic_list_group2);
                    }

                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;


            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('offers.store') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                            $("#addFormBtn").html(' حفظ');
                            $("#addFormBtn").attr('disabled',false);

                            $('#addForm').trigger("reset");

                            showSuccesFunction();
                            $("#inlineForm").modal('hide');

                            table.draw(false);

                            // table.row.add(data.data).draw( false );
                        } else{
                            showErrorFunction();
                            $("#addFormBtn").attr('disabled', false);
                            $("#addFormBtn").html(' حفظ');
                        }
                    },

                    error: function (data) {
                        $("#addFormBtn").html(' حفظ');
                        $("#addFormBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end add new record



        $("#filterBtn").on('click', function (e) {

            var cat_id = $("#test").val();

            table.clear();
            table = $('.data-table').DataTable({
                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ url('filter/offers/') }}" + "/" + cat_id,

                columns: [

                    {data: 'DT_RowIndex', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'cat', name: 'cat'},
                    {data: 'offer_image', name: 'offer_image'},
                    {data: 'offer_price', name: 'offer_price'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });


        });

            //soft delete
            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                var co = confirm("  هل أنت متأكد من الحذف !");
                if (!co) {
                    return;
                }


                $.ajax({

                    type: "POST",

                    url: "{{ route('offers.delete')}}"  +"/"+ product_id,

                    data:{
                        '_token':'{{csrf_token()}}'
                    },

                    success: function (data) {
                        Swal.fire({title:"  تم الحذف بنجاح   ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        table.draw(false);

                    },

                    error: function (data) {
                        Swal.fire({title:" حدث خطأ ما ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        console.log('خطأ:', data);
                    }

                });

            });
        });


        $(document).ready(function() {
            $('#test').select2({
                placeholder: 'ابحث عن فئة',

                data: [

                        @foreach ($cats as $cat)

                        @if (strlen($cat->childs) <4)
                    {
                        id: '{{$cat->id}}', text: '{{$cat->name}}*'
                    },


                        @else
                    {
                        text: '{{$cat->name}}*' ,id: '{{$cat->id}}', children:
                            [
                                    @foreach ($cat->childs as $cat2)
                                {
                                    id: '{{$cat2->id}}', text: '{{$cat2->name}}'
                                },

                                @endforeach

                            ],

                    },
                    @endif
                    @endforeach


                ],
            });
        });
    </script>

@endpush
