@extends("admin_layout")
@section('content')


<section id="input-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <p>
                  المناطق
                    </p><br>
                        @can('deliver_region-create')
                        <div class="row" style="margin: 20px;">
                    <button type="button" id="addClick" class="btn btn-primary">إضافة</button>
                        </div>
                        @endcan
                        <div class="row">
                            <div class="col-sm-12" style="overflow-x:auto;">
                                <table id="tableData" class="table table-striped table-sm data-table">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> اسم المنطقة </th>
                                        <th> قيمة التوصيل  </th>
                                        <th >العمليات </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
<div class="modal-dialog " role="document">
<div class="modal-content">
    <div class="modal-header">
        <label class="modal-title text-text-bold-600" id="myModalLabel33">    معلومات  االمنطقة </label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times"></i></span>
        </button>
    </div>
                <form action="#" method="post" id="editFromData" >
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">

                        <div class="form-group">
                            <fieldset>
                            <div class="row">

                               <div class="col-md-6">
                                    <label > اسم المنطقة </label>
                                    <div class="form-group">
                                    <input type="text" id="regionName" name="regionName" class="form-control" placeholder=" ">

                                </div>
                              </div>
                                    <div class="col-md-6">
                                        <label>   سعر التوصيل  </label>
                                         <div class="form-group">
                                    <input type="text" id="deliverPrice" name="deliverPrice" class="form-control" placeholder=" ">
                                </div>
                              </div>
                            </div>


                            </fieldset>
                        </div>

                        </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" name="saveBtn" id="saveBtn" class="btn btn-primary" value="حفظ">

                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- print forms -->
    <div id="print_form_1_print" style="display: none !important;">

    </div>

@endsection


@push('myjs')

    <script type="text/javascript">

        $(function () {


            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

            ajax: "{{ route('deliverRegion.index') }}",

            columns: [

                {data: 'DT_RowIndex', name: 'DT_RowIndex'},

                {data: 'regionName', name: 'regionName'},
                {data: 'deliverPrice', name: 'deliverPrice'},

                {data: 'action', name: 'action', orderable: false, searchable: false},

]

});

            // fill select delegate


           $('body').on('click', '#addClick', function (e)
             {
           e.preventDefault();
           $("#_id").val('');
           $('#editFromData').trigger("reset");
           $("#saveBtn").val("إضافة");
           $("#inlineForm").modal("show");
        });

        $("#addColor").click(function (e) {
            e.preventDefault();

            $("#action").html('<i class="fa fa-load"></i> ... ');
            $.ajax({

                data: {
                    "_token":$("input[name=_token]").val(),
                    "regionName":$("#regionName").val(),
                    "deliverPrice":$("#deliverPrice").val(),

                    "_id":$("#_id").val(),
                },

                url: "{{ route('deliverRegion.store') }}",

                type: "POST",

                dataType: 'json',
                timeout:4000,
                success: function (data) {

                    $("#action").html('إضافة ');
                    if(data.status==200) {
                        showSuccesFunction();}

                    else{
                        showErrorFunction();
                    }
                },

                error: function (data) {
                    $("#addColor").html('إضافة منطقة'); }


                });
            }); // end add new record


            $('body').on('click', '.editProduct', function () {

            var region_id = $(this).data('id');

            $.get("{{ route('deliverRegion.index') }}" + '/' + region_id + '/edit', function (data) {

                $("#saveBtn").val("حفظ");
                $('#_id').val(data.id);
                $('#regionName').val(data.regionName);
                $('#deliverPrice').val(data.deliverPrice);
                $("#inlineForm").modal('show');
})


}) ;// end edit function;

              $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var region_id = $("#_id").val();
                var method="post";
                var url = "{{ route('deliverRegion.store') }}";
                if(region_id){
                   url = "{{ route('deliverRegion.index') }}"+ '/' + region_id ;
                   method="PATCH";
                }
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: method,

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        showSuccesFunction();
                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data


            $('body').on('click', '.deleteProduct', function () {


                var region_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('deliverRegion.index') }}"+ '/' + region_id ,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
            showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { showErrorFunction();}

            });   }

            });

            }) ;// end delete function;





});



    </script>

@endpush
