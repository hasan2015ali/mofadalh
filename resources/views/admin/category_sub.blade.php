@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                الفئات الفرعية للفئة:
                                <a style="color: #0C3B87 ;font-style:initial;">  {{$sub->name}} </a>
                            </p>
                                <div class="alert alert-danger" >

                                    <p> <i class="fa fa-bell" ></i>
                                        إذا قمت بإضافة فئات فرعية ضمن هذه الفئة ولها منتجات يجب عليك تعديل الفئة الخاصة بهذه المنتجات!</p>
                                </div>
                            @can('cat_sub_create')


                            <div class="row" style="margin: 20px;">
                                <button type="button" id="addClick" class="btn btn-primary">إضافة</button>

                            </div>
                            @endcan
                            <div class="row">

                                <div class="col-sm-12">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>اسم الفئة</th>
                                            <th>الأيقونة  </th>
                                            <th >العمليات</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="titleModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="titleModal"> عنوان </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>

                    </button>
                </div>
                <form action="#" method="post" id="editFromData">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <input type="hidden" name="cat_id" id="cat_id" value="{{$sub->id}}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6"> <label> اسم الفئة </label>
                                <div class="form-group">
                                    <input type="text" name="name" id="name_edit" class="form-control" required>
                                    <div id="img-list">

                                    </div>
                                </div><br>
                                <label for="roundText">  الأيقونة</label>
                                   <div class="container">
                                <section id="draggable-cards">
                                    <div class="row match-height" id="card-drag-area">

                                    </div>

                                </section>
                            </div>
                         </div>
                         <div class="col-md-6">
                            <fieldset class="form-group">
                             <div class="needsclick dropzone" id="document-dropzone">
                             </div> <br>



                            </fieldset>
                        </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveBtn" class="btn btn-primary" value="حفظ">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('myjs')
    {{--dropzone--}}
    <script>

        var uploadedDocumentMap = {};
        var alesDropZone  =   new Dropzone( "#document-dropzone", {
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            acceptedFiles: ".png, .jpg, .jpeg",
                success: function (file, response) {
                    $('#editFromData').append('<input type="hidden" name="icon" value="' + response.name + '">')

                    uploadedDocumentMap[file.name] = response.name
                },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('#editFromData').find('input[name="document[]"][value="' + name + '"]').remove()
            }, // end remove file

            url: '{{ route('projects.storeMedia',['table'=>"categories"]) }}',
            maxFilesize: 1, // MB
            addRemoveLinks: true,
            timeout:0,
            init: function() {

            } // end init
        } ); // end dropzone.
    </script>
    <script type="text/javascript">

        $(function () {

        $("#addClick").click(function (e) {
            e.preventDefault();
            $("#saveBtn").html("إضافة");
            $("#titleModal").html("إضافة فئة");
            $('#_id').val('') ;
            $('#editFromData').trigger("reset");
            $("#inlineForm").modal("show");
            alesDropZone.removeAllFiles(true);
            $("#image_sub").html("");
            $("#card-drag-area").html("");


        });
        var id = '{{$sub->id}}';
            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ url('categories/subs/index/') }}" +'/'+ id,

                columns: [

                    {data: 'id', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'icon', name: 'icon'},


                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });

            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');
                alesDropZone.removeAllFiles(true);
                $.get("{{ route('categories.index') }}" + '/' + product_id + '/edit', function (data) {




                    $("#saveBtn").val("حفظ");

                    $("#titleModal").html("تعديل فئة ");

                    $('#_id').val(data.id);

                    $('#name_edit').val(data.name);
                    var icon_s = "<img src='{{asset('/storage')}}/"+data.icon+"' width='120' height='120'>";
                    $("#card-drag-area").html(icon_s);

                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                var url = "{{ route('categories.store') }}";

                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");
                        $("#_id").val('0')
                        showSuccesFunction()
                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction()
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data

            $('body').on('click', '.delete', function () {
                var btn = $(this);

                var product_id = $(this).data("id");
                btn.html("<i class='fa fa-spinner'></i>");
                btn.attr("disabled",true);

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",


            url: "{{ route('categories.index') }}"+ '/' + product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
            showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });


            }); // end delete row


        });

    </script>

@endpush
