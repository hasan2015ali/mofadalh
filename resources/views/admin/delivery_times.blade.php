@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                أوقات الطلبيات
                            </p>
                            @can('deliver_time-create')
                            <div class="row">
                                <button id="add" class="btn btn-primary"> إضافة </button>
                            </div>
                            @endcan
<br>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>
                                            <th>اسم  </th>
                                            <th> من الوقت</th>
                                            <th> إلى الوقت </th>
                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-xs" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل البيانات  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
                <div class="container" style="width: 70%">
                        <form action="{{route('delivery_times.store')}}" method="post" id="editFormData">
                        <input type="hidden" name="_id" id="_id" >
                            @csrf


                                <fieldset class="form-group">
                                    <label for="name">  الاسم  </label>
                                    <input type="text" id="name" name="name" class="form-control" placeholder="  ادخل اسم ">

                                </fieldset>


                                <fieldset class="form-group">
                                    <label for="from_time"> من الوقت </label>
                                    <input type="text" id="from_time" name="from_time" class="form-control" placeholder="  ادخل وقت  ">

                                </fieldset>


                                <fieldset class="form-group">
                                    <label for="to_time"> إلى الوقت  </label>
                                    <input type="text" id="to_time" name="to_time" class="form-control" placeholder="  ادخل وقت  ">

                                </fieldset>


                                <fieldset class="form-group">

                                    <button type="button" id="saveBtn" class="btn gradient-purple-bliss">تعديل</button>
                                </fieldset>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('myjs')
    <script type="text/javascript">

        $(function () {

            $("#add").click(function () {
                $("#saveBtn").html('حفظ');
                $("#myModalLabel33").html('إضافة ');
                $('#_id').val('');
                $("#editFormData").trigger("reset");
                $("#inlineForm").modal('show');

            });

            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('delivery_times.index') }}",

                columns: [

                    {data: 'id', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'from_time', name: 'from_time'},
                    {data: 'to_time', name: 'to_time'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#editFormData').serialize(),

                    url: "{{ route('delivery_times.store') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                            $("#saveBtn").html(' حفظ');
                            $("#saveBtn").attr('disabled',false);

                            $('#editFormData').trigger("reset");

                            Swal.fire({
                                title: " ", text: "تم الحفظ بنجاح",
                                type: "success", confirmButtonClass: "btn btn-primary", buttonsStyling: !1
                            });
                            $("#inlineForm").modal('hide');

                            table.draw(false);

                        } else{
                            Swal.fire({
                                title: "  ", text: data.message,
                                type: "warning", confirmButtonClass: "btn btn-primary", buttonsStyling: !1
                            });
                            $("#saveBtn").attr('disabled', false);
                            $("#saveBtn").html(' حفظ');
                        }
                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        Swal.fire({title:"  حدث خطأ ما ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end add new record


            $('body').on('click', '.edit', function () {

                var id = $(this).data('id');
                $("#myModalLabel33").html('تعديل ');

                $.get("{{ route('delivery_times.index') }}" + '/' + id + '/edit', function (data) {


                    $("#saveBtn").val("حفظ");

                    $('#_id').val(data.id);

                    $('#name').val(data.name);
                    $('#to_time').val(data.to_time);
                    $('#from_time').val(data.from_time);

                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;


            $('body').on('click', '.delete', function () {

                var id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('delivery_times.index') }}"+'/' + id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
                showSuccessDelete();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });


            }); // end delete row



        });

    </script>


@endpush
