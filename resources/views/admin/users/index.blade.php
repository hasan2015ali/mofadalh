@extends('admin_layout')



@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <h2>ادارة المستخدمين</h2>
            </div>
            <br>
            <br>
            <br>
            @can('user-create')
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('users.create') }}"> اضافة مستخدم</a>
            </div>
            @endcan
            <br>
            <br>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>الاسم</th>
            <th>الايميل</th>
            <th>الادوار</th>
            <th width="280px">العمليات</th>
        </tr>
        @foreach ($data as $key => $user)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $v)
                            <label class="badge badge-success">{{ $v }}</label>
                        @endforeach
                    @endif
                </td>
                <td>
                    <a class="btn btn-info" style="margin: 5px;" href="{{ route('users.show',$user->id) }}">عرض</a>

                    @can('user-edit')
                        <a class="btn btn-primary" style="margin: 5px;" href="{{ route('users.edit',$user->id) }}">تعديل</a>

                    @endcan
                    @can('user-delete')
                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                        {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}

                    @endcan

                </td>
            </tr>
        @endforeach
    </table>


    {!! $data->render() !!}

@endsection
