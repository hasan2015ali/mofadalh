@extends("admin_layout")
@section('content')


<section id="input-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <p>
                      المساعدة
                    </p><br>
                        @can('help-create')
                        <div class="row" style="margin: 20px;">
                    <button type="button" id="addClick" class="btn gradient-purple-bliss">إضافة</button>
                        </div>
                        @endcan
                        <div class="row">
                            <div class="col-sm-12" style="overflow-x:auto;">
                                <table id="tableData" class="table table-striped table-sm data-table">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> العنوان </th>
                                        <th> الفيديو </th>
                                        <th >العمليات </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
<div class="modal-dialog " role="document">
<div class="modal-content">
    <div class="modal-header">
        <label class="modal-title text-text-bold-600" id="myModalLabel33">   إضافة فيديو   </label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times"></i></span>
        </button>
    </div>
                <form action="#" method="post" id="editFromData">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>العنوان  </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="title" id="title">

                                </div></div>
                                <div class="col-md-6">
                                 <label> الفيديو </label>
                                <div class="form-group">
                                    <div class="needsclick dropzone" id="document-dropzone">

                                </div>

                            </div>

                            <div class="col-md-6" id="uploadVideo">

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" name="saveBtn" id="saveBtn" class="btn btn-primary" value="حفظ">

                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- print forms -->


@endsection


@push('myjs')
  {{--dropzone--}}
  <script>
    var uploadedDocumentMap = {}
    var alesDropZone  =   new Dropzone( "#document-dropzone", {
        url: '{{ route('projects.storeMedia',["table"=>"helps"]) }}',
        maxFiles:1,
        maxFilesize: 20, // MB
        addRemoveLinks: true,
         headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        acceptedFiles: ".mp4",
        processing:function(){
            $('#saveBtn').prop('disabled',true);
            toastr.options.positionClass = 'toast-top-center';
            toastr.success('انتظر اكتمال التحميل');
        },
        init: function() {
            this.on('addedfile', function(file) {
             if (this.files.length > 1) {
            this.removeFile(this.files[1]);
            toastr.options.positionClass = 'toast-top-center';
            toastr.success('لايمكن رفع اكثر من ملف');}
      });



    // this.hiddenFileInput.removeAttribute('multiple');



        },
        success: function (file, response) {
            $('#editFromData').append('<input type="hidden" name="video" value="' + response.name + '">')

            uploadedDocumentMap[file.name] = response.name
            $('#saveBtn').prop('disabled',false);
        },
        removedfile: function (file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedDocumentMap[file.name]
            }
            $('#editFromData').find('input[name="video"][value="' + name + '"]').remove()
        },

    });
</script>

    <script type="text/javascript">

        $(function () {


            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

            ajax: "{{ route('help.index') }}",

            columns: [

                {data: 'DT_RowIndex', name: 'DT_RowIndex'},

                {data: 'title', name: 'title'},
                {data: 'video', name: 'video'},


                {data: 'action', name: 'action', orderable: false, searchable: false},

]

});

            // fill select delegate
            $("#addClick").click(function (e) {
           e.preventDefault();
           $("#saveBtn").val("إضافة");
           $('#editFromData').trigger("reset");
           alesDropZone.removeAllFiles( true );
           $("#uploadVideo").html('');
           $("#inlineForm").modal("show");
        });



            $('body').on('click', '.editProduct', function () {

            var product_id = $(this).data('id');

            $.get("{{ route('help.index') }}" + '/' + product_id + '/edit', function (data) {

                $("#saveBtn").val("حفظ");
                alesDropZone.removeAllFiles( true );
                $('#_id').val(data.id);
                $('#title').val(data.title);
                $("#uploadVideo").html('');
                var html="";
                var asset_url2 = '{{asset('/storage/')}}/' +data.video;
                var html ="<video width=\"100\" height=\"100\" controls> "
                +" <source src='"+asset_url2 +"' type='video/mp4'> </video>"
                    $("#uploadVideo").html(html);
                $("#inlineForm").modal('show');
})


}) ;// end edit function;

              $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                var method="post";
                var url = "{{ route('help.store') }}";
                if(product_id){
                   url = "{{ route('help.index') }}"+ '/' + product_id ;
                   method="PATCH";
                }
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: method,

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        showSuccesFunction();
                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data


            $('body').on('click', '.deleteProduct', function () {

                var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('help.index') }}"+ '/' + product_id ,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
            showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });

            }) ;// end delete function;




});



    </script>

@endpush
