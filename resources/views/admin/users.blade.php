@extends("admin_layout")
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                 المستخدمين المقبولين
                            </p>

                            <br>
                            <div class="row">

                                <div class="col-sm-12"  style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th>الاسم</th>
                                            <th>البريد الالكتروني  </th>
                                            <th> رقم الهاتف</th>
                                            <th> العنوان  </th>
                                            <th>  صورة </th>
                                            <th>  الكوبونات </th>
                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('myjs')

    {{--dropzone--}}
    <script>
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('projects.storeMedia',["table"=>"users"]) }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('#editFromData').append('<input type="hidden" name="image" value="' + response.name + '">')

                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('#editFromData').find('input[name="image"][value="' + name + '"]').remove()
            },
            init: function () {
                        @if(isset($project) && $project->document)
                var files =
                {!! json_encode($project->document) !!}
                    for (var i in files) {
                    var file = files[i];
                    this.options.addedfile.call(this, file);
                    file.previewElement.classList.add('dz-complete');
                    $('#addForm').append('<input type="hidden" name="document[]" value="' + file.file_name + '">');
                }
                @endif
            }
        }
    </script>
    <script type="text/javascript">

        $(function () {

            $("#addClick").click(function (e) {
                e.preventDefault();
                $("#saveBtn").val("إضافة");
                $('#_id').val('');
                $('#password_edit').attr('placeholder','');
                $("#editFromData").trigger("reset");
                $("#inlineForm").modal("show");
            });


            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('users.accepted') }}",

                columns: [

                    {data: 'id', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {data: 'avatar', name: 'avatar'},
                    {data: 'coupons', name: 'coupons'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });

            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('superMarkets.store') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');


                        $('#addForm').trigger("reset");

                        alert(data.message);
                        // toastr.success('تم الحفظ بنجاح');

                        table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');



                    }

                });
            }); // end add new record

            $('body').on('click', '.delete', function () {
   
                var product_id = $(this).data("id");
                
                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('usersRequests.index') }}"+ '/' + product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
                showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }
                
            });
                   
                   

            }); // end delete row

            $('body').on('click', '.accept', function () {


                var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {


                $.ajax({

                    type: "POST",

                    url: "{{ route('usersRequests.agree') }}" + '/' + product_id,

                    success: function (data) {
                        showSuccesFunction();
                        table.draw(false);

                    },

                    error: function (data) {
                        showErrorFunction();
                        console.log('خطأ:', data);
                    }

                });}
            });

        });
    });

    </script>

@endpush
