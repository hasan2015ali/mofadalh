@extends("admin_layout")
@section('content')

<style>
    .fa-star-full{
        color:orange;

    }
    .fa-star-half {

        right: 0;
    transform: rotateY(
180deg
);
    width: 50%;
    height: 100%;
    color: orange;
}

</style>

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                المنتجات
                            </p>

                            <div class="row">
                                <button id="add" class="btn btn-primary"> إضافة </button> &nbsp
                                {{--<button id="report" class="btn btn-primary"> تقرير </button>--}}
                            </div>

                            <div class="row justify-content-center">
                                {{--<div class="col-sm-3">--}}
                                    {{--<fieldset class="form-group">--}}
                                        {{--<label for="roundText">  الفئة الرئيسية </label>--}}
                                        {{--<br>--}}
                                        {{--<select class="form-control" id="main_cat_filter" name="cat_id12">--}}
{{--                                            @foreach($main_cats as $item)--}}
{{--                                                <option value="{{$item->id}}">{{$item->name}}</option>--}}
{{--                                            @endforeach--}}
                                        {{--</select>--}}


                                    {{--</fieldset>--}}
                                {{--</div>--}}
                                <div class="col-sm-3 col-3">
                                    <fieldset class="form-group">

                                        <label for="roundText">  الفئة  </label>
                                        <select id="test" class="select_cat form-control" style="width: 100%"></select>

                                    </fieldset>
                                </div>


                                    {{--</fieldset>--}}
                                {{--</div>--}}

                                <div class="col-sm-1 col-1">
                                    <fieldset class="form-group">
                                        <br>
                                   <button class="btn btn-success" id="filterBtn">   <i class="fa fa-filter"></i> </button>



                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th width='5%'>#</th>

                                            <th>اسم المنتج</th>
                                            <th>السعر  </th>
                                            <th>الفئة  </th>
                                            <th>صور المنتج </th>
                                            <th>صورة العرض </th>
                                            <th>سعر العرض </th>
                                            <th>الخصم </th>
                                            <th>التقييم</th>
                                            <th> متوفر</th>
                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> إضافة منتج  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
               <div class="container">
                   <form action="{{route('products.store')}}" method="post" id="addForm">
                       <input type="hidden" name="_id" id="_id" >
                       <div class="row">
                           @csrf

                           <div class="col-sm-6">
                               <fieldset class="form-group">
                                   <label for="name">  اسم المنتج</label>
                                   <input type="text" id="name" name="name" class="form-control" placeholder="  ادخل اسم المنتج">


                               </fieldset>
                           </div>


                           <div class="col-sm-6">
                               <fieldset class="form-group">
                                   <label for="cat">  الفئة  </label>
                                   <br>
                                   <select id="cat" name="cat" class="select_cat form-control" style="width: 100%"></select>



                               </fieldset>
                           </div>

                           <div class="col-sm-6">
                               <fieldset class="form-group">
                                   <label for="offer_price">  سعر العرض  </label>
                                   <input type="text" id="offer_price" name="offer_price" class="form-control" placeholder="  ادخل سعر العرض">

                               </fieldset>
                           </div>

                           <div class="col-sm-6">
                               <fieldset class="form-group">
                                   <label for="discount">  الخصم  </label>
                                   <input type="number" id="discount" name="discount" class="form-control" placeholder="  ادخل الخصم">

                               </fieldset>
                           </div>

                           <div class="col-sm-9">
                               <fieldset class="form-group">
                                   <label for="roundText">  صور المنتج  </label>
                                   <!-- Draggable cards section start -->
                                   <section id="draggable-cards1">
                                       <div class="row match-height" id="card-drag-area">

                                       </div>
                                   </section>
                                   <!-- // Draggable cards section end -->
                                   <div class="needsclick dropzone" id="document-dropzone">

                                   </div>

                               </fieldset>
                           </div>

                           <div class="col-sm-9">
                               <fieldset class="form-group">
                                   <label for="roundText">  صورة العرض  </label>
                                   <div id="image_edit">
                                   </div>
                                   <br>
                                   <div class="needsclick dropzone" id="document-dropzone2">

                                   </div>

                               </fieldset>
                           </div>

                           <div class="col-sm-12">

                               <fieldset class="form-group">

                                   <br>
                                   <button type="button" id="addFormBtn" class="btn gradient-purple-bliss">حفظ</button>
                               </fieldset>
                           </div>

                       </div>
                   </form>
               </div>
            </div>
        </div>
    </div>



    <div class="modal fade text-left" id="viewDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">  عرض بيانات المنتج  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromDataView">
                    @csrf
                    <input type="hidden" name="_id" id="_id_view">
                    <input type="hidden" name="_accept" id="_accept" value="1">
                    <div class="modal-body">

                        <div class="form-group">

                            <label for="">اسم المنتج </label>
                            <input type="text" value="" id="view_name" name="name" class="form-control" disabled>
                        </div>

                        <div class="form-group">

                            <label for=""> الفئة    </label>
                            <br>
                            <select id="view_cat" class="select_cat form-control" style="width: 100%" disabled></select>

                        </div>
                        <div class="form-group">

                            <label for=""> سعر العرض    </label>
                            <input type="text" value="" id="view_offer_price" name="price"  class="form-control" disabled>
                        </div>


                        <div class="form-group">
                            <label for="">  الخصم  </label>
                            <input type="text" id="view_discount" name="view_discount" class="form-control" disabled>

                        </div>

                        <div class="form-group">
                            <label for="">  صور المنتج  </label>
                            <!-- Draggable cards section start -->
                            <section id="draggable-cards1">
                                <div class="row match-height" id="view_image">

                                </div>
                            </section>
                            <!-- // Draggable cards section end -->
                        </div>

                        <div class="form-group">
                            <label for="">  صورة العرض  </label>
                            <br>
                            <img src="" alt="" id="view_offer_image" width="100" height="100">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">


                    </div>
                </form>
            </div>
        </div>
    </div>




    <div class="modal fade text-left" id="subForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">  سعر الوحدة </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromDataSubs">
                    @csrf
                    <input type="hidden" name="_product_id" id="_product_id">
                    <div class="modal-body">

                        <div class="form-group">
                            <fieldset>
                            <div class="row">

                                    <div class="col-md-6">
                                     <label >  الوحدة </label>
                                        <div class="form-group">
                                    <input type="text" id="priceName" name="priceName" class="form-control" placeholder=" ">
                                </div>
                              </div>
                                    <div class="col-md-6">
                                        <label>  السعر </label>
                                         <div class="form-group">
                                    <input type="number" id="product_price" name="product_price" class="form-control" placeholder=" ">
                                </div>
                              </div>
                            </div>
                            <div class="row  d-flex justify-content-center" >
                                    <div class="input-group-append">
                                        <button class="btn btn-success" id="add_subPrice" type="button" >إضافة الوحدة</button>
                                    </div> </div>

                            </fieldset>
                        </div>

                        </div>
                    <div id="subs_content">
                        <div class="col-sm-12" id="sortable-lists" >
                        <ul class="list-group" id="basic-list-group">



                        </ul>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveSortBtn" class="btn btn-primary" value="حفظ الترتيب">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="commentForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">  التعليقات </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editComments">
                    @csrf
                    <input type="hidden" name="product_id" id="product_id">

                    <div id="subs_content">
                        <div class="col-sm-12" >
                        <ul class="list-group" id="comment-list-group">



                        </ul>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">

                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('myjs')
    {{--dropzone--}}
    <script type="text/javascript">

        $("#main_cat").select2({
            theme: "classic",
            dir: "rtl"
        });

        var uploadedDocumentMap = {};
        var alesDropZone  =   new Dropzone( "#document-dropzone", {
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('#addFormBtn').attr('disabled',false);
                $('#addForm').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name

            },  // end auccess
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('#addForm').find('input[name="document[]"][value="' + name + '"]').remove()
            }, // end remove file

            url: '{{ route('projects.storeMedia',['table'=>"products"]) }}',
            maxFilesize: 1, // MB
            addRemoveLinks: true,
            timeout:0,
            acceptedFiles: ".jpg, .png",
            init: function() {
                // this.on( 'removedfile', removedFileCallback );

            }, // end init
            processing:function () {
                $('#addFormBtn').attr('disabled',true);
                toastr.options.positionClass = 'toast-top-center';
                toastr.warning('انتظر اكتمال التحميل');
            }

        } ); // end dropzone.


        var uploadedDocumentMap2 = {};
        var ales2DropZone  =   new Dropzone( "#document-dropzone2", {
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('#addFormBtn').attr('disabled',false);
                $('#addForm').append('<input type="hidden" name="offer_image" value="' + response.name + '">')
                uploadedDocumentMap2[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('#addForm').find('input[name="offer_image"][value="' + name + '"]').remove()
            },

            url: '{{ route('projects.storeMedia',['table'=>"products"]) }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            timeout:0,
            init: function() {
                this.on( 'addedfile', function (file) {
                    if(this.files.length > 1){
                        this.removeFile(this.files[1]);
                        toastr.options.positionClass = 'toast-top-center';
                        toastr.warning('لا يمكن رفع أكثر من ملف');
                    }
                } );
            },
            acceptedFiles: ".jpg, .png",
            processing:function () {
                $('#addFormBtn').attr('disabled',true);
                toastr.options.positionClass = 'toast-top-center';
                toastr.warning('انتظر اكتمال التحميل');

            }
        } );

        $(function () {

            $("#add").click(function () {
                $("#addFormBtn").html('حفظ');
                $('#_id').val('');
                $("#addForm").trigger("reset");
                $("#cat").val('').trigger('change');
                $("#inlineForm").modal('show');
                alesDropZone.removeAllFiles( true );
                ales2DropZone.removeAllFiles( true );
                $("#card-drag-area").html("");
                $("#image_edit").html("");

            });


            $("#search").click(function (e) {
                e.preventDefault();
                $("#search").html('جاري البحث ..');
                $("#search").attr('disabled',true);
                var product_id = $("#date").val();
                $.get("{{ url('report') }}"+ '/' + product_id, function (data) {

                        $("#receive_report").val(data.received_products);
                        $("#order_report").val(data.order_products);
                        $("#search").html("<i class='fa fa-search'> </i>");
                        $("#search").attr('disabled',false);
                })
            });


            $("#filterBtn").on('click', function (e) {


                var cat_id = $("#test").val();


                table.clear();
                table = $('.data-table').DataTable({
                    destroy: true,
                    processing: true,

                    serverSide: true,
                    stateSave: true,

                    {{--ajax: "{{ url('filter/products') }}/"+type_send+"/"+cat_id,--}}
                    ajax: "{{ url('filter/products') }}"+"/"+cat_id ,

                    columns: [

                        {data: 'DT_RowIndex', name: 'id'},

                        {data: 'name', name: 'name'},
                        {data: 'price', name: 'price'},
                        {data: 'cat', name: 'cat'},
                        // {data: 'market', name: 'market'},
                        {data: 'image', name: 'image'},
                        {data: 'offer_image', name: 'offer_image'},
                        {data: 'offer_price', name: 'offer_price'},
                        {data: 'discount', name: 'discount'},
                        {data: 'stars', name: 'stars'},
                        {data: 'available', name: 'available'},

                        {data: 'action', name: 'action', orderable: false, searchable: false},

                    ]

                });


            });



            var table = $('.data-table').DataTable({
                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('products.index') }}",

                columns: [

                    {data: 'DT_RowIndex', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'price', name: 'price'},
                    {data: 'cat', name: 'cat'},
                    // {data: 'market', name: 'market'},
                    {data: 'image', name: 'image'},
                    {data: 'offer_image', name: 'offer_image'},
                    {data: 'offer_price', name: 'offer_price'},
                    {data: 'discount', name: 'discount'},
                    {data: 'stars', name: 'stars'},
                    {data: 'available', name: 'available'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });



            $("#addFormBtn").click(function (e) {
                e.preventDefault();



                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('products.store') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                            $("#addFormBtn").html(' حفظ');
                            $("#addFormBtn").attr('disabled',false);

                            $('#addForm').trigger("reset");

                            showSuccesFunction();
                            $("#inlineForm").modal('hide');

                            table.draw(false);

                            // table.row.add(data.data).draw( false );
                        } else{
                            showErrorFunction();
                            $("#addFormBtn").attr('disabled', false);
                            $("#addFormBtn").html(' حفظ');
                        }
                    },

                    error: function (data) {
                        $("#addFormBtn").html(' حفظ');
                        $("#addFormBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end add new record

            $('body').on('click', '.edit', function () {
                $("input[name='document[]']").remove();

                $("#addFormBtn").html('تعديل');
                $("#myModalLabel33").html('تعديل بيانات المنتج');
                alesDropZone.removeAllFiles( true );
                ales2DropZone.removeAllFiles( true );
                $("#card-drag-area").html("");
                $("#cat").trigger('reset');

                var product_id = $(this).data('id');

                $.get("{{ route('products.index') }}" + '/' + product_id + '/edit', function (data) {
                    $("#addFormBtn").html('تعديل');

                    $('#_id').val(data.id);

                    $('#name').val(data.name);
                    $('#offer_price').val(data.offer_price);
                    $('#discount').val(data.discount);
                    $('#cat').val(data.cat_id).trigger('change');

                    $("#image_edit").html('');

                    if(data.offer_image != null && data.offer_image.length > 5) {

                        var offer_image = data.offer_image;

                        var basic_list_group2 = "";
                        var asset_url2 = '{{asset('/storage/')}}';

                            basic_list_group2 += "<div class=\"col-xl-3 col-md-6 col-12\">\n" +
                                "                                           <div class=\"card draggable\">\n" +
                                "                                               <div class=\"card-header\">\n" +
                                "                                                   <h4 class=\"card-title\">\n" +
                                "                                    <input type=\"hidden\" name=\"sort_order[]\" value='" + offer_image + "'>\n" +

                                "                                    <input type=\"hidden\" name=\"offer_image\" value='" + offer_image + "'>\n" +

                                "                                                       <p class=\"m-0\">\n" +
                                "                                                           <img  id='" + offer_image + "' src='" + asset_url2 +"/"+ offer_image + "' width=\"100\" height=\"100\">\n" +
                                "                                                       </p>\n" +
                                "\n" +
                                "                                                   </h4>\n" +
                                "                                               </div>\n" +
                                "                                               <div class=\"card-content\">\n" +
                                "                                                   <div class=\"card-body\">\n" +
                                "<button data-img='" + offer_image + "' data-id='" + data.id + "' class='btn btn-danger deleteOfferImg' >  <i class='fa fa-trash'></i> </button>" +
                                "\n" +
                                "                                                   </div>\n" +
                                "                                               </div>\n" +
                                "                                           </div>\n" +
                                "                                       </div>";

                        $("#image_edit").html(basic_list_group2);
                    }

                    $("#card-drag-area").html("");
                    if(data.images != null && data.images.length > 5) {


                        var images = data.images.split(",");

                        var basic_list_group = "";
                        var asset_url = '{{asset('/storage/')}}';
                        for (var i = 0; i < images.length; i++) {

                            basic_list_group += "<div class=\"col-xl-3 col-md-6 col-12\">\n" +
                                "                                           <div class=\"card draggable\">\n" +
                                "                                               <div class=\"card-header\">\n" +
                                "                                                   <h4 class=\"card-title\">\n" +
                                "                                    <input type=\"hidden\" name=\"sort_order[]\" value='" + images[i] + "'>\n" +

                                "                                    <input type=\"hidden\" name=\"document[]\" value='" + images[i] + "'>\n" +

                                "                                                       <p class=\"m-0\">\n" +
                                "                                                           <img  id='" + images[i] + "' src='" + asset_url +"/"+ images[i] + "' width=\"100\" height=\"100\">\n" +
                                "                                                       </p>\n" +
                                "\n" +
                                "                                                   </h4>\n" +
                                "                                               </div>\n" +
                                "                                               <div class=\"card-content\">\n" +
                                "                                                   <div class=\"card-body\">\n" +
                                "<button data-img='" + images[i] + "' data-id='" + data.id + "' class='btn btn-danger deleteImg' >  <i class='fa fa-trash'></i> </button>" +
                                "\n" +
                                "                                                   </div>\n" +
                                "                                               </div>\n" +
                                "                                           </div>\n" +
                                "                                       </div>";

                        }
                        $("#card-drag-area").html(basic_list_group);
                    }

                    $("#inlineForm").modal('show');
                })

            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ route('products.index',['table'=>'products']) }}"+ '/' + product_id + '/update',

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        showSuccesFunction();
                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );
                        } else{
                            Swal.fire({
                                title: "  ", text: data.message,
                                type: "warning", confirmButtonClass: "btn btn-primary", buttonsStyling: !1
                            });
                            $("#saveBtn").attr('disabled', false);
                        }
                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data

            $("#sendNotification").click(function (e) {
                e.preventDefault();

                $("#sendNotification").html('جاري الارسال ..');
                $("#sendNotification").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#notificationsForm').serialize(),

                    url: "{{ route('app.send-notitication') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#sendNotification").html(' ارسال');
                        $("#sendNotification").attr('disabled',false);

                        $('#notificationsForm').trigger("reset");

                        alert(data.message);


                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#sendNotification").html(' ارسال');
                        $("#sendNotification").attr('disabled',false);



                    }

                });
            }); // end save  record data


            $('body').on('click', '.deleteImg', function (e) {
                e.preventDefault();

               var id= $(this).data("id");
               var img = $(this).data('img');

                $.ajax({

                    type: "DELETE",

                    url: "{{ route('projects.deleteMedia',['table'=>'products']) }}",
                    data:{
                        '_token':'{{csrf_token()}}',
                        'image':img,
                        'product_id':id
                    },
                    success: function (data) {
                        if(data.images.length > 5) {


                            var images = data.images.split(",");

                            var basic_list_group = "";
                            var asset_url = '{{asset('/storage/')}}';
                            for (var i = 0; i < images.length; i++) {
                                basic_list_group += "<div class=\"col-xl-3 col-md-6 col-12\">\n" +
                                    "                                           <div class=\"card draggable\">\n" +
                                    "                                               <div class=\"card-header\">\n" +
                                    "                                                   <h4 class=\"card-title\">\n" +
                                    "                                    <input type=\"hidden\" name=\"sort_order[]\" value='" + images[i] + "'>\n" +

                                    "                                    <input type=\"hidden\" name=\"document[]\" value='" + images[i] + "'>\n" +

                                    "                                                       <p class=\"m-0\">\n" +
                                    "                                                           <img  id='" + images[i] + "' src='" + asset_url +"/"+   images[i] + "' width=\"100\" height=\"100\">\n" +
                                    "                                                       </p>\n" +
                                    "\n" +
                                    "                                                   </h4>\n" +
                                    "                                               </div>\n" +
                                    "                                               <div class=\"card-content\">\n" +
                                    "                                                   <div class=\"card-body\">\n" +
                                    "<button data-img='" + images[i] + "' data-id='" + id + "' class='btn btn-danger deleteImg' >  <i class='fa fa-trash'></i> </button>" +
                                    "\n" +
                                    "                                                   </div>\n" +
                                    "                                               </div>\n" +
                                    "                                           </div>\n" +
                                    "                                       </div>";

                            }
                            $("#card-drag-area").html(basic_list_group);
                            table.draw(false);
                        }
                        else {
                            $("#card-drag-area").html("");
                        }
                        Swal.fire({title:" ",text:"تم الحذف بنجاح",
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        table.draw(false);

                    },

                    error: function (data) {
                        Swal.fire({title:"  ",text:"حدث خطأ ما",
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        console.log('خطأ:', data);

                    }

                });


            });


            $('body').on('click', '.deleteOfferImg', function (e) {
                e.preventDefault();

                var id = $(this).data("id");
                var img = $(this).data('img');

                $.ajax({

                    type: "DELETE",

                    url: "{{ route('projects.deleteMedia',['table'=>'offer_products']) }}",
                    data:{
                        '_token':'{{csrf_token()}}',
                        'image':img,
                        'product_id':id
                    },
                    success: function (data) {
                        if(data.offer_image.length > 5) {

                            var offer_image5 = data.offer_image;
                            var basic_list_group5 = "";
                            var asset_url5 = '{{asset('/storage/')}}';
                                basic_list_group5 += "<div class=\"col-xl-3 col-md-6 col-12\">\n" +
                                    "                                           <div class=\"card draggable\">\n" +
                                    "                                               <div class=\"card-header\">\n" +
                                    "                                                   <h4 class=\"card-title\">\n" +
                                    "                                    <input type=\"hidden\" name=\"sort_order[]\" value='" + offer_image5 + "'>\n" +

                                    "                                    <input type=\"hidden\" name=\"offer_image5\" value='" + offer_image5 + "'>\n" +

                                    "                                                       <p class=\"m-0\">\n" +
                                    "                                                           <img  id='" + offer_image5 + "' src='" + asset_url5 +"/"+   offer_image5 + "' width=\"100\" height=\"100\">\n" +
                                    "                                                       </p>\n" +
                                    "\n" +
                                    "                                                   </h4>\n" +
                                    "                                               </div>\n" +
                                    "                                               <div class=\"card-content\">\n" +
                                    "                                                   <div class=\"card-body\">\n" +
                                    "<button data-img='" + offer_image5 + "' data-id='" + id + "' class='btn btn-danger deleteImg' >  <i class='fa fa-trash'></i> </button>" +
                                    "\n" +
                                    "                                                   </div>\n" +
                                    "                                               </div>\n" +
                                    "                                           </div>\n" +
                                    "                                       </div>";


                            $("#image_edit").html(basic_list_group5);
                            table.draw(false);
                        }
                        else {
                            $("#image_edit").html("");
                        }
                        Swal.fire({title:" ",text:"تم الحذف بنجاح",
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        table.draw(false);

                    },

                    error: function (data) {
                        Swal.fire({title:"  ",text:"حدث خطأ ما",
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        console.log('خطأ:', data);

                    }

                });

            });


            $('body').on('click', '.addToSlider', function () {


                var product_id = $(this).data("id");
              var btn =   $(this);

                sweetConfirm( function (confirmed) {
                    if (confirmed) {


                    $.ajax({

                        data:{
                            "_token":$("input[name=_token]").val(),
                            "slider":1,
                            "change_slider":true ,
                            "product":product_id

                        },

                        url: "{{ route('products.slider') }}",

                        type: "POST",

                        dataType: 'json',
                        timeout:4000,
                        success: function (data) {


                            btn.html("<i class='fa fa-minus'></i>");
                            showSuccesFunction();
                            table.draw( false);

                            // table.row.add(data.data).draw( false );

                        },

                        error: function (data) {
                            $("#saveBtn").html(' حفظ');
                            $("#saveBtn").attr('disabled',true);



                        }

                    });
                    }

                 });

            }); // end delete row

            $('body').on('click', '.makeNotAvailable', function () {


                var product_id = $(this).data("id");
              var btn =   $(this);

                sweetConfirm( function (confirmed) {
                    if (confirmed) {


                    $.ajax({

                        data:{
                            "_token":$("input[name=_token]").val(),
                            "status":0,
                            "change_status":true ,
                            "product":product_id

                        },

                        url: "{{ route('products.available') }}",

                        type: "POST",

                        dataType: 'json',
                        timeout:4000,
                        success: function (data) {


                            btn.html("<i class='fa fa-minus'></i>");
                            showSuccesFunction();
                            table.draw( false);

                            // table.row.add(data.data).draw( false );

                        },

                        error: function (data) {
                            $("#saveBtn").html(' حفظ');
                            $("#saveBtn").attr('disabled',true);



                        }

                    });
                    }

                 });

            }); // end delete row


            $('body').on('click', '.makeAvailable', function () {


                var product_id = $(this).data("id");
                var btn =   $(this);
                sweetConfirm( function (confirmed) {
                if (confirmed) {


                $.ajax({

                    data:{
                        "_token":$("input[name=_token]").val(),
                        "status":1,
                        "change_status":true,
                        "product":product_id

                        },

                    url: "{{ route('products.available') }}",
                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {


                        btn.html("<i class='fa fa-check'></i>");
                        showSuccesFunction();
                        table.draw( false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);


                    }

                });
            }

});

            }); // end delete row

            $('body').on('click', '.view', function () {

                var product_id = $(this).data('id');

                $.get("{{ route('products.index') }}" + '/' + product_id + '/edit', function (data) {


                    var img_path = "{{url('/public/storage')}}/";
                    $('#view_name').val(data.name);
                    $('#view_offer_price').val(data.offer_price);
                    $('#view_cat').val(data.cat_id).trigger('change');
                    $('#view_discount').val(data.discount).trigger('change');

                    $("#view_image").html("");
                    if(data.images != null && data.images.length > 5) {

                        var images = data.images.split(",");

                        var basic_list_group = "";
                        var asset_url = '{{asset('/storage/')}}';
                        for (var i = 0; i < images.length; i++) {

                            basic_list_group += "<div class=\"col-xl-3 col-md-6 col-12\">\n" +
                                "                                           <div class=\"card draggable\">\n" +
                                "                                               <div class=\"card-header\">\n" +
                                "                                                   <h4 class=\"card-title\">\n" +
                                "                                    <input type=\"hidden\" name=\"sort_order[]\" value='" + images[i] + "'>\n" +

                                "                                    <input type=\"hidden\" name=\"document[]\" value='" + images[i] + "'>\n" +

                                "                                                       <p class=\"m-0\">\n" +
                                "                                                           <img  id='" + images[i] + "' src='" + asset_url +"/"+ images[i] + "' width=\"100\" height=\"100\">\n" +
                                "                                                       </p>\n" +
                                "\n" +
                                "                                                   </h4>\n" +
                                "                                               </div>\n" +
                                "                                               <div class=\"card-content\">\n" +
                                "                                                   <div class=\"card-body\">\n" +
                                "\n" +
                                "                                                   </div>\n" +
                                "                                               </div>\n" +
                                "                                           </div>\n" +
                                "                                       </div>";

                        }
                        $("#view_image").html(basic_list_group);
                    }

                    var offer_img = img_path+ data.offer_image;
                    $('#view_offer_image').attr('src', offer_img);

                    $("#viewDetailsModal").modal('show');
                })


            }) ;// end edit function;


            //soft delete
            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('products.index') }}"+ '/' + product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
                showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });

            });// end delete product


            $('body').on('click', '.price', function (e) {
                e.preventDefault();
                $('#editFromDataSubs').trigger("reset");
                var product_id=$(this).data('id');
                $("#_product_id").val(product_id);
                $.get("{{ url('product') }}" + '/' + product_id + '/prices', function (data) {
                    $("#basic-list-group").html("");

                var arrayData = data;
                for (var i = 0; i < arrayData.length; i++) {
                    var html = " <li class=\"list-group-item draggable\">\n" +
                        "   <input type=\"hidden\" name=\"sort_order[]\" value='" + arrayData[i].id + "'>\n" +
                        "   <input type=\"hidden\" name=\"price_id[]\" value='" + arrayData[i].id + "'>\n" +

                        "   <div class=\"media\">\n" +
                        "\n" +
                        "  <div class=\"media-body\">\n" +
                        "     <h5 class=\"mt-0\"><input type='text' name='name[]' value='"+ arrayData[i].name + "' class='form-control' > </h5>\n" +
                        "\n" +
                        "     <h5 class=\"mt-0\"><input type='number' name='price[]' value='"+ arrayData[i].price + "' class='form-control' > </h5>\n" +
                        "\n" +
                        "<p> "+
                        "<button class='btn btn-danger deleteSub' data-id='"+arrayData[i].id+"'> <i class='fa fa-trash'></i> </button>" +
                        "</p>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </li>";
                    $("#basic-list-group").append(html);
                }
                $('#subForm').modal('show');


                      });
                });//end show product prices

            $("#add_subPrice").click(function (e) {
                e.preventDefault();

                var name =$("#priceName").val();
                if(name.length < 2){
                    alert("enter name");
                    return;
                }
                    $("#add_subPrice").html('<i class="fa fa-load"></i> ... ');
                    $.ajax({

                        data: {
                        "_token":$("input[name=_token]").val(),
                        "name":$("#priceName").val(),
                        "price":$("#product_price").val(),
                        "product_id":$("#_product_id").val()
                    },

                    url: "{{ route('store_price') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {

                        $("#add_subPrice").html('إضافة سعر');
                        if(data.status==200) {


                            showSuccesFunction();
                            $("#basic-list-group").html("");

                            var arrayData = data.data;
                            for (var i = 0; i < arrayData.length; i++) {
                                var html = " <li class=\"list-group-item draggable\">\n" +
                                    "   <input type=\"hidden\" name=\"sort_order[]\" value='" + arrayData[i].id + "'>\n" +
                                    "   <input type=\"hidden\" name=\"price_id[]\" value='" + arrayData[i].id + "'>\n" +
                                    "   <div class=\"media\">\n" +
                                    "\n" +
                                    "  <div class=\"media-body\">\n" +
                                    "     <h5 class=\"mt-0\"><input type='text' name='name[]' value='"+ arrayData[i].name + "' class='form-control' > </h5>\n" +
                                    "\n" +
                                    "     <h5 class=\"mt-0\"><input type='number' name='price[]' value='"+ arrayData[i].price + "' class='form-control' > </h5>\n" +
                                    "\n" +
                                    "<p> "+
                                    "<button class='btn btn-danger deleteSub' data-id='"+arrayData[i].id+"'> <i class='fa fa-trash'></i> </button>" +
                                    "</p>\n" +
                                    "                                        </div>\n" +
                                    "                                    </div>\n" +
                                    "                                </li>";
                                $("#basic-list-group").append(html);
                                $("#priceName").val('');
                                $("#product_price").val('');
                                table.draw(false);

                            }
                        }
                        else{
                            showErrorFunction();
                        }




                    },

                    error: function (data) {
                        $("#add_subPrice").html('إضافة سعر');



                    }

                });
            }); // end add new price to a product


             $("#saveSortBtn").click(function (e) {
                e.preventDefault();


                $("#saveSortBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $("#editFromDataSubs").serialize(),

                    url: "{{ route('updatePrice') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {

                        $("#saveSortBtn").html('حفظ الترتيب');

                        showSuccesFunction();
                            table.draw(false);
                            $("#subForm").modal('hide');

                    },

                    error: function (data) {

                        $("#saveSortBtn").html('حفظ الترتيب');

                        showErrorFunction();
                        $("#subForm").modal('hide');

                    }

                });
            }); // end add new record

            $('body').on('click', '.deleteSub', function (e) {
               e.preventDefault();
               var item =  $(this);
               var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ url('prices')}}"  +"/"+ product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
            item.parent().parent().parent().remove();
             showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });
            table.draw(false);
            });//end delete price for product


            $('body').on('click', '.comments', function (e) {
                e.preventDefault();

                var product_id=$(this).data('id');
                $("#product_id").val(product_id);
                $.get("{{ url('product') }}" + '/' + product_id + '/comment', function (data) {
                    $("#comment-list-group").html("");

                var arrayData = data;
                for (var i = 0; i < arrayData.length; i++) {
                    var html = " <li class=\"list-group-item \">\n" +
                        "   <input type=\"hidden\" name=\"comment_id[]\" value='" + arrayData[i].id + "'>\n" +

                        "   <div class=\"media\">\n" +
                        "\n" +
                        "  <div class=\"media-body\">\n" +
                        "     <h3 class=\"mt-0\">"+ arrayData[i].user.name + " </h5>\n" +
                        "\n" +
                        "     <h5 class=\"mt-0\">"+ arrayData[i].comment + " </h5>\n" +
                        "\n" +
                        "<p> "+
                        "<button class='btn btn-danger deleteComment' data-id='"+arrayData[i].id+"'> <i class='fa fa-trash'></i> </button>" +
                        "</p>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </li>";
                    $("#comment-list-group").append(html);
                }
                $('#commentForm').modal('show');


                      });
                });// get comments



                $('body').on('click', '.deleteComment', function (e) {
               e.preventDefault();
               var item =  $(this);
               var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ url('comments')}}"  +"/"+ product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
                item.parent().parent().parent().remove();
                showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }

            });

            });//end delete price for product

            });

       $(document).ready(function() {
        $('.select_cat').select2({
            placeholder: 'ابحث عن فئة',
            data: [

                @foreach ($cats as $cat)

                    @if (strlen($cat->childs) <4)
                {
                    id: '{{$cat->id}}', text: '{{$cat->name}}*'
                },

                    @else
        {
            text: '{{$cat->name}}*' ,id: '{{$cat->id}}', children:
            [
                    @foreach ($cat->childs as $cat2)
                {
                    id: '{{$cat2->id}}', text: '{{$cat2->name}}'
                },

                    @endforeach

            ],

        },
                    @endif
                @endforeach


            ],
        });
      });


</script>
@endpush
