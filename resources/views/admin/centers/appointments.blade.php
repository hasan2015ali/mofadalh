@extends("admin_layout")
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                 المواعيد


                            </p>
                            <div class="row">
                                <form action="#" method="post" id="editFromData" class="row">
                                    @csrf
                                    <input type="hidden" name="_id" id="_id">
                                <div class="col-md-3">
                                    <label for="">الاسم  </label>
                                    <input type="text" class="form-control" name="name" id="name_edit">
                                </div>
                                <div class="col-md-3">
                                    <label for="">اسم الأم  </label>
                                    <input type="text" class="form-control" name="mother_name" id="mother_name_edit" >
                                </div>
                                <div class="col-md-3">
                                    <label for="">الكنية    </label>
                                    <input type="text" class="form-control" name="last_name" id="last_name_edit">
                                </div>

                                <div class="col-md-3">
                                    <label for="">الرقم الوطني    </label>
                                    <input type="number" class="form-control" id="national_id_edit" name="national_id">
                                </div>
                                <div class="col-md-3">
                                    <label for=""> الموبايل      </label>
                                    <input type="number" class="form-control" id="mobile_edit" name="mobile">
                                </div>

                                <div class="col-md-3">
                                    <label for="">التاريخ      </label>
                                    <input type="date" name="date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <label for="">الوقت</label>
                                    <select name="hour" id="hour">
                                        @for ($i=8;$i<=16;$i++)
                                            <option value='{{$i}}'>{{$i}}</option>
                                        @endfor
                                    </select>
                                    <select name="minute" id="minute">
                                        @for ($i=0;$i<=60;$i+=10)
                                            <option value='{{$i}}'>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                </form>
                            </div>
                            @can('category-create')
                                <div class="row" style="margin: 20px;">
                                    <button type="button" id="addClick" class="btn btn-primary">إضافة</button>

                                </div>
                            @endcan
                            <br>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th> الاسم</th>
                                            <th>اسم الأم    </th>
                                            <th>الكنية    </th>
                                            <th> الرقم الوطني  </th>
                                            <th>  رقم الموبايل    </th>
                                            <th> التاريخ    </th>
                                            <th> الوقت    </th>
                                                 <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('myjs')


        <script type="text/javascript">

            $(function () {


            var table = $('.data-table').DataTable({

            destroy: true,
            processing: true,

            serverSide: true,
            stateSave: true,

            ajax: "{{ route('appointments.index') }}",

            columns: [

        {data: 'DT_RowIndex', name: 'id'},

        {data: 'student.name', name: 'student.name'},
        {data: 'student.last_name', name: 'student.last_name'},
        {data: 'student.mother_name', name: 'student.mother_name'},
        {data: 'student.national_id', name: 'student.national_id'},
        {data: 'student.mobile', name: 'student.mobile'},
        {data: 'date', name: 'date'},
        {data: 'time', name: 'time'},


        {data: 'action', name: 'action', orderable: false, searchable: false},

            ]

        });




            $('body').on('click', '.edit', function () {

            var product_id = $(this).data('id');
            $("#title").html("تعديل مدير المركز");

            $.get("{{ route('appointments.index') }}" + '/' + product_id + '/edit', function (data) {




            $("#saveBtn").val("حفظ");

            $('#_id').val(data.id);
            $('#name_edit').val(data.student.name);
            $('#last_name_edit').val(data.student.last_name);
            $('#mother_name_edit').val(data.student.mother_name);
            $('#national_id_edit').val(data.student.national_id);
            $('#mobile_edit').val(data.student.mobile);
            $('#date_edit').val(data.date);
            $('#time_edit').val(data.time);


            $("#inlineForm").modal('show');
        })


        }) ;// end edit function;

            $("#addClick").click(function (e) {
            e.preventDefault();

            $("#addClick").html('جاري الخفظ ..');
            $("#addClick").attr('disabled',true);
            var product_id = $("#_id").val();
            var url = "{{ route('appointments.store') }}";
                var name  = $("#name_edit").val();
                if(name.length < 3){
                    showErrorFunctionCustom("يرجى ادخال الاسم");
                    return;
                }
                    var mother  = $("#mother_name_edit").val();
                if(mother.length < 3){
                    showErrorFunctionCustom("يرجى ادخال اسم الام");
                    return;
                }
                var last  = $("#last_name_edit").val();
                if(last.length < 3){
                    showErrorFunctionCustom("يرجى ادخال الكنية  ");
                    return;
                }

            $.ajax({

            data: $('#editFromData').serialize(),

            url: url,

            type: "POST",

            dataType: 'json',
            timeout:40000,
            success: function (data) {
            $("#addClick").html(' حفظ');
            $("#addClick").attr('disabled',false);

            $('#editFromData').trigger("reset");

            showSuccesFunction();

            $("#inlineForm").modal('hide');

            table.draw(false);



        },

            error: function (data) {
            $("#addClick").html(' حفظ');
            $("#addClick").attr('disabled',false);

            showErrorFunction();

        }

        });
        }); // end save  record data



            $('body').on('click', '.delete', function () {
            var product_id = $(this).data("id");

            sweetConfirm( function (confirmed) {
            if (confirmed) {
            $.ajax({

            type: "DELETE",

            url: "{{ route('appointments.index') }}"+ '/' + product_id,
            data:{
            '_token':'{{csrf_token()}}'
        },
            success: function (data) {
            showSuccesFunction();
            table.draw(false);

        },

            error: function (data) { }

        });   }

        });

        }); // end delete row


        });

    </script>
@endpush
