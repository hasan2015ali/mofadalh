@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                الملف الشخصي
                            </p>

                            <form action="{{route('profile.update')}}" id="formData">
                                <div class="row">

                                    @csrf


                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">  الاسم  </label>
                                            <input type="text" id="name" name="name" class="form-control round" placeholder=" " value="{{$user->name}}">


                                        </fieldset>
                                    </div>

                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">  البريد الإلكتروني   </label>
                                            <input type="text" id="email" name="email" class="form-control round" placeholder=" " value="{{$user->email}}">


                                        </fieldset>
                                    </div>



                                    <div class="col-sm-3">
                                        <fieldset class="form-group">

                                            <label for="roundText">   كلمة السر(في حال أردت تعديلها)</label>
                                            <input type="password" id="password" name="password" class="form-control round" placeholder="  ادخل  كلمة السر">
                                            <br>


                                        </fieldset>
                                    </div>

                                </div>
                            </form>
                       <div class="row">
                           <br>
                           <button type="button" id="saveBtn" class="btn gradient-purple-bliss">حفظ</button>
                       </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@push('myjs')
    {{--dropzone--}}
    <script>
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('projects.storeMedia',["table"=>"sliders"]) }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                    var item = " <li class=\"list-group-item draggable\">\n" +
                        "                                                    <input type=\"hidden\" name=\"sort_order[]\" >\n" +
                        "                                                    <input type=\"hidden\" name=\"document[]\" value='"+response.name+"'>\n" +
                        "                                                    <div class=\"media\">\n" +
                        "\n" +
                        "                                                        <div class=\"media-body\">\n" +
                        "                                                            <img width=\"100\" height=\"100\" src='{{url('public/storage/')}}/"+response.name+"' alt=\"\">\n" +
                        "\n" +
                        "                                                        </div>\n" +
                        "                                                    </div>\n" +
                        "                                                </li>";

                $('#basic-list-group').append(item)
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('#addForm').find('input[name="document[]"][value="' + name + '"]').remove()
            },
            init: function () {
                        @if(isset($project) && $project->document)
                var files =
                {!! json_encode($project->document) !!}
                    for (var i in files) {
                    var file = files[i];
                    this.options.addedfile.call(this, file);
                    file.previewElement.classList.add('dz-complete');
                    $('#addForm').append('<input type="hidden" name="document[]" value="' + file.file_name + '">');
                }
                @endif
            }
        }
    </script>

    <script type="text/javascript">

        $(function () {



            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $("#formData").serialize(),

                    url: "{{ route('profile.update') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html('حفظ');
                     if(data.status==200){
                         Swal.fire({title:"  ",text:"تم الحفظ بنجاح",
                             type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});

                     }
                     else if(data.status==201){
                         Swal.fire({title:"  ",text:" البريد الالكتروني مستخدم سابقا",
                             type:"warning",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                     }
                     else{
                         Swal.fire({title:"  ",text:" يرجى المحاولة لاحقا",
                             type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                     }
                    window.location.reload();


                    },

                    error: function (data) {
                        $("#saveBtn").html('حفظ');

                        Swal.fire({title:"  ",text:" حدث خطأ ما  ",
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});

                    }

                });
            }); // end add new record




        });

    </script>

@endpush
