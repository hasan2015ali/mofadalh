@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                  المراكز
                            </p>
                            @can('category-create')
                                <div class="row" style="margin: 20px;">
                                    <button type="button" id="addClick" class="btn btn-primary">إضافة</button>

                                </div>
                            @endcan
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>
                                            <th>#</th>
                                            <th>اسم المركز</th>
                                            <th>اسم العنوان</th>
                                            <th>  النوع</th>
                                            <th>  المدير</th>
                                            <th>  عدد العاملين</th>


                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="title">title  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>

                    </button>
                </div>
                <form action="#" method="post" id="editFromData">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6"> <label>   المدير </label>
                                <div class="form-group">
                                    <select name="manager_id" id="manager_id_edit" class="form-control">
                                        @foreach($managers as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div><br>


                            </div>   <div class="col-md-6"> <label>   النوع </label>
                                <div class="form-group">
                                    <select name="center_type_id" id="center_type_id_edit" class="form-control">
                                        @foreach($cats as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div><br>


                            </div>

                            <div class="col-md-6"> <label> اسم المركز </label>
                                <div class="form-group">
                                    <input type="text" name="name" id="name_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label> العنوان   </label>
                                <div class="form-group">
                                    <input type="text" name="address" id="address_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label> عدد العاملين   </label>
                                <div class="form-group">
                                    <input type="number" name="workers" id="workers_edit" placeholder="" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label>  خط الطول     </label>
                                <div class="form-group">
                                    <input type="text" name="lat" id="lat_edit" placeholder="lat" class="form-control" required>

                                </div><br>


                            </div>
                            <div class="col-md-6"> <label> خط العرض     </label>
                                <div class="form-group">
                                    <input type="text" name="lng" id="lng_edit" placeholder="lang" class="form-control" required>

                                </div><br>


                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveBtn" class="btn btn-primary" value="حفظ">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('myjs')

    <script type="text/javascript">

        $(function () {

            $("#addClick").click(function (e) {
                e.preventDefault();
                $("#saveBtn").html("إضافة");
                $("#title").html("إضافة فئة");
                $('#_id').val('') ;
                $('#editFromData').trigger("reset");
                $("#inlineForm").modal("show");
                alesDropZone.removeAllFiles(true);
                $("#image_sub").html("");
                $("#card-drag-area").html("");
            });
            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('centers.index') }}",

                columns: [

                    {data: 'DT_RowIndex', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'address', name: 'address'},
                    {data: 'type.name', name: 'type.name'},
                    {data: 'manager.name', name: 'manager.name'},
                    {data: 'workers', name: 'workers'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });




            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');
                $("#title").html("تعديل مركز");

                $.get("{{ route('centers.index') }}" + '/' + product_id + '/edit', function (data) {




                    $("#saveBtn").val("حفظ");

                    $('#_id').val(data.id);
                    $('#name_edit').val(data.name);
                    $('#address_edit').val(data.address);
                    $('#center_type_id_edit').val(data.center_type_id);
                    $('#workers_edit').val(data.workers);
                    $('#lat_edit').val(data.lat);
                    $('#lng_edit').val(data.lng);


                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                var url = "{{ route('centers.store') }}";
                // if(product_id){
                //    url = "{{ route('centers.index') }}"+ '/' + product_id + '/update'
                // }
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        showSuccesFunction();

                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data



            $('body').on('click', '.delete', function () {
                var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                    if (confirmed) {
                        $.ajax({

                            type: "DELETE",

                            url: "{{ route('centers.index') }}"+ '/' + product_id,
                            data:{
                                '_token':'{{csrf_token()}}'
                            },
                            success: function (data) {
                                showSuccesFunction();
                                table.draw(false);

                            },

                            error: function (data) { }

                        });   }

                });

            }); // end delete row


        });

    </script>

@endpush
