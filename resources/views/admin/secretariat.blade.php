@extends("admin_layout")
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                السكرتاريا
                            </p>
                            <div class="row">
                                <form action="{{route('secretariat.store')}}" method="post" id="addForm">

                                    <div class="row">
                                        @csrf

                                        <div class="col-sm-3">
                                            <fieldset class="form-group">
                                                <label for="roundText">  اسم  السكرتير/ة</label>
                                                <input type="text" id="name" name="name" class="form-control round" placeholder="  ادخل الاسم">


                                            </fieldset>
                                        </div>
                                        <div class="col-sm-3">
                                            <fieldset class="form-group">
                                                <label for="roundText"> الهاتف </label>
                                                <input type="text" id="phone" name="phone" class="form-control round" placeholder="  ادخل رقم الهاتف  ">


                                            </fieldset>
                                        </div>
                                        <div class="col-sm-3">
                                            <fieldset class="form-group">
                                                <label for="roundText">  البريد الالكتروني  </label>
                                                <input type="email" id="email" name="email" class="form-control round" placeholder="  ادخل البريد الالكتروني  ">


                                            </fieldset>
                                        </div>

                                        <div class="col-sm-3">
                                            <fieldset class="form-group">

                                                <label for="roundText">   كلمة السر  </label>
                                                <input type="password" id="password" name="password" class="form-control round" placeholder="  ادخل كلمة السر  ">
                                                <br>


                                                <button type="button" id="addFormBtn" class="btn gradient-purple-bliss">إضافة</button>
                                            </fieldset>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="row">

                                <div class="col-sm-12" style="overflow-x:auto;">
                                    <table class="table table-striped table-sm data-table">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th>اسم  السكرتيرة</th>

                                            <th>رقم الهاتف </th>
                                            <th>البريد الالكتروني </th>
                                            <th>العنوان بالتفصيل  </th>
                                            <th >العمليات</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل بيانات السكرتير  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
                <div class="container">
                        <form action="{{route('secretariat.store')}}" method="post" id="editFromData">
                        <input type="hidden" name="_id" id="_id" >
                        <div class="row">
                            @csrf

                            <div class="col-sm-3">
                                <fieldset class="form-group">
                                    <label for="roundText">  الاسم  </label>
                                    <input type="text" id="name_edit" name="name" class="form-control round" placeholder="  ادخل اسم ">


                                </fieldset>
                            </div>
                            <div class="col-sm-3">
                                <fieldset class="form-group">
                                    <label for="roundText"> رقم الهاتف  </label>
                                    <input type="text" id="phone_edit" name="phone" class="form-control round" placeholder="  ادخل رقم الهاتف  ">


                                </fieldset>
                            </div>
                            <div class="col-sm-3">
                                <fieldset class="form-group">
                                    <label for="roundText"> البريد الالكتروني   </label>
                                    <input type="text" id="email_edit" name="email" class="form-control round" placeholder="  ادخل البريد  ">


                                </fieldset>
                            </div>
                            <div class="col-sm-3">
                                <fieldset class="form-group">
                                    <label for="roundText">   العنوان  </label>
                                    <input type="text" id="address_edit" name="address" class="form-control round" placeholder="  ادخل اسم العنوان">


                                </fieldset>
                            </div>
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label for="roundText">   كلمة السر  </label>
                                    <input type="password" id="password_edit" name="password" class="form-control round" placeholder="  اتركها فارغة اذا لم ترغب بالتعديل    ">


                                </fieldset>
                            </div>


                            <div class="col-sm-9">
                                <fieldset class="form-group">





                                    <br>
                                    <button type="button" id="saveBtn" class="btn gradient-purple-bliss">تعديل</button>
                                </fieldset>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('myjs')
    <script type="text/javascript">

        $(function () {

            // fill select delegate
            $(".select2").select2();



            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax: "{{ route('secretariat.index') }}",

                columns: [

                    {data: 'id', name: 'id'},

                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'address', name: 'address'},

                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]

            });

            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('secretariat.store') }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');
                        if(data.status==200){
                            Swal.fire({title:" ",text:"تمت الإضافة  بنجاح",
                                type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                            $('#addForm').trigger("reset");
                        }
                        else{
                            Swal.fire({title:"  ",text:data.message,
                                type:"warning",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        }
                        table.draw(false);

                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');

                        Swal.fire({title:" حدث خطأ ما ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});


                    }

                });
            }); // end add new record

            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');

                $.get("{{ route('secretariat.index') }}" + '/' + product_id + '/edit', function (data) {


                    $("#saveBtn").val("حفظ");


                    $('#_id').val(data.id);

                    $('#name_edit').val(data.name);
                    $('#phone_edit').val(data.phone);
                    $('#email_edit').val(data.email);
                    $('#address_edit').val(data.address);
                    $('#password_edit').val('');

                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ route('secretariat.index') }}"+ '/' + product_id ,

                    type: "PUT",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        Swal.fire({title:" ",text:"تم الحفظ بنجاح",
                            type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                        $("#inlineForm").modal('hide');

                        table.draw(false);


                        } else{
                            Swal.fire({
                                title: "  ", text: data.message,
                                type: "warning", confirmButtonClass: "btn btn-primary", buttonsStyling: !1
                            });
                            $("#saveBtn").attr('disabled',false);
                            $("#saveBtn").html(' حفظ');
                        }

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);
                        $("#inlineForm").modal('hide');
                        Swal.fire({title:" حدث خطأ ما ",text:data.message,
                            type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});



                    }

                });
            }); // end save  record data

            $('body').on('click', '.delete', function () {

                var product_id = $(this).data("id");
                
                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

            type: "DELETE",

            url: "{{ route('secretariat.index') }}"+'/' + product_id,
            data:{
                '_token':'{{csrf_token()}}'
            },
            success: function (data) {
                showSuccesFunction();
            table.draw(false);

            },

            error: function (data) { }

            });   }
                
            });
               

            }); // end delete row



        });

    </script>


@endpush
