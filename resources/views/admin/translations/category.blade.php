@extends("admin_layout")
@section('content')


<section id="input-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <p>
                     الترجمة - {!! $item_to_trans->name !!}
                    </p><br>
                    <button type="button" id="addClick" class="btn gradient-purple-bliss">إضافة</button>

                        <div class="row">
                            <div class="col-sm-12" style="overflow-x:auto;">
                                <table id="tableData" class="table table-striped table-sm data-table">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> اللغة  </th>
                                        <th> الاسم   </th>
                                        <th >العمليات </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
<div class="modal-dialog modal-xl" role="document">
<div class="modal-content">
    <div class="modal-header">
        <label class="modal-title text-text-bold-600" id="myModalLabel33">   المعلومات   </label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times"></i></span>
        </button>
    </div>
                <form action="#" method="post" id="editFromData" >
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label> اللغة   </label>
                                <div class="form-group">
                                        <select name="lang" id="lang" class="form-control">
                                            @foreach ($langs as $lang )
                                                <option value="{{$lang->code}}">{{$lang->name}}</option>
                                            @endforeach
                                        </select>
                                </div></div>
                                <div class="col-md-6">
                                    <label>   الاسم </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" id="name">

                                    </div>
                                </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" name="saveBtn" id="saveBtn" class="btn btn-primary" value="حفظ">

                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- print forms -->
    <div id="print_form_1_print" style="display: none !important;">

    </div>

@endsection


@push('myjs')

    <script type="text/javascript">

        $(function () {


            var table = $('.data-table').DataTable({

                destroy: true,
                processing: true,

                serverSide: true,
                stateSave: true,

                ajax:"{{url('translations/categories')}}/"+{{$item_to_trans->id}},

                columns: [

                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},

                    {data: 'lang', name: 'lang'},
                    {data: 'name', name: 'name'},


                    {data: 'action', name: 'action', orderable: false, searchable: false},

                        ]

            });

            // fill select delegate


           $('body').on('click', '#addClick', function (e) {
                e.preventDefault();
                $("#_id").val('');
                $('#editFromData').trigger("reset");
                $("#saveBtn").val("إضافة");
                $("#inlineForm").modal("show");
            });




            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');

                $.get("{{url('translations/categories')}}/"+{{$item_to_trans->id}} + '/' + product_id + '/edit', function (data) {

                    $("#saveBtn").val("حفظ");
                    $('#_id').val(data.id);
                    $('#name').val(data.name);
                    $('#lang').val(data.lang);
                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

              $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                var method="post";
                var url = "{{url('translations/categories')}}/"+{{$item_to_trans->id}};

                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: method,

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        showSuccesFunction();
                        $("#inlineForm").modal('hide');

                        table.draw(false);

                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save  record data


            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                sweetConfirm( function (confirmed) {
                if (confirmed) {
                    $.ajax({

                type: "DELETE",

                url: "{{url('translations/categories')}}/"+{{$item_to_trans->id}} + '/' + product_id ,
                data:{
                    '_token':'{{csrf_token()}}'
                },
                success: function (data) {
                    showSuccesFunction();
                table.draw(false);

                },

                error: function (data) { }

                });   }

                });


            }) ;// end delete function;




    });



    </script>

@endpush
