@extends("admin_layout")
@section('content')
    <style>
        .doneBtn {
            background-color: rgba(0,56,255,0) !important;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                قائمة الطبيات الجديدة
                            </p>

                            <div class="row justify-content-center">
                                <div class="col-sm-3">
                                    <fieldset class="form-group">
                                        <label for="roundText">  فلترة  </label>
                                        <select class="form-control" id="status_filter" name="status">

                                            @foreach($status as  $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endforeach

                                        </select>


                                    </fieldset>
                                </div>

                                <div class="col-sm-1">
                                    <fieldset class="form-group">
                                        <br>
                                        <button class="btn btn-info" id="filterBtn"> <i class="fa fa-filter"></i> </button>

                                    </fieldset>
                                </div>
                            </div>
                            <div class="row" id="order_div">
                                @if(count($offers) > 0)
                                    @foreach($offers as $item)
                                        <div class="col-lg-4 col-md-4">
                                            <div class="card overflow-hidden" style="height: 320px;">
                                                <div class="card-content">
                                                    <div class="card-img p-3">

                                                        @if($item->status == 0)
                                                            <a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="{{$item->id}}" data-status="{{$item->status}}"><img src="{{asset('app-assets/images/icon/check.ico')}}"></a>
                                                        @elseif($item->status == 1)
                                                            <a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="{{$item->id}}" data-status="{{$item->status}}"><img src="{{asset('app-assets/images/icon/like.ico')}}"></a>
                                                        @elseif($item->status == 2)
                                                            <a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="{{$item->id}}" data-status="{{$item->status}}"><img src="{{asset('app-assets/images/icon/cart.ico')}}"></a>
                                                        @elseif($item->status == 3)
                                                            <a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="{{$item->id}}" data-status="{{$item->status}}"><img src="{{asset('app-assets/images/icon/delivery.ico')}}"></a>
                                                        @elseif($item->status == 4)
                                                            <a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="{{$item->id}}" data-status="{{$item->status}}"><img src="{{asset('app-assets/images/icon/trust.ico')}}"></a>
                                                        @endif

                                                    </div>
                                                    <div class="card-body">
                                                        <h4 class="card-title">#  {{$item->id}}  </h4>
                                                        {{---  <i class="fa fa-money"></i>  {{$item->full_price}}--}}
                                                        <h4 class="card-title"><i class="fa fa-user"></i> {{$item->client->name ?? 'مستخدم محذوف'}}  </h4>

                                                        <p class="card-text"><i class="fa fa-map-marker"></i> {{$item->city ?? 'غير محدد'}} </p>

                                                        {{-- <p class="card-text"> <i class="fa fa-clock-o"></i> {{$item->created_at->format('H:i d/m/Y') ?? 'غير محدد'}} </p> --}}
                                                        <p class="card-text"> <i class="fa fa-clock-o"></i> @php echo date("D" , strtotime($item->created_at)) ." ".date("d/m/yy H:i" , strtotime($item->created_at))  @endphp </p>

                                                        <p>

                                                        </p>
                                                        <button type="button"  class="btn btn-info viewCart" data-id="{{$item->id}}" data-re="{{$item->region_id}}"> <i class="fa fa-info-circle"></i> </button>
                                                        <a href="{{url('print-document/'.$item->id.'/'.$item->region_id)}}" target="_blank" class="btn btn-outline-tumblr printCart"> <i class="fa fa-print"></i> </a>
                                                        <button type="button"  class="btn btn-danger deleteCart" data-id="{{$item->id}}"> <i class="fa fa-trash-o"></i>  </button>
                                                        <button type="button"  class="btn btn-outline-grey mapCart" data-id="{{$item->id}}"> <i class="fa fa-map-marker"></i>  </button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else

                                    <div class="alert alert-danger mb-2" role="alert" id="no_carts">
                                        <div class="alert-icon-left">
                                            <i class="fa fa-frown-o mr-2"></i>
                                            <span> لا توجد طلبيات مضافة </span>
                                        </div>
                                    </div>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content" >
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">   تفاصيل الطلبية    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromData" class="striped-rows">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body"  style="overflow-y:auto; height: 600px">
                        <div class="row">
                            <div class="col-md-4">

                                <label> المستخدم        </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="full_name" id="full_name" disabled>

                                </div>

                                <label> المنطقة        </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="region" id="region" disabled>

                                </div>

                                <label> العنوان     </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" id="address" disabled>

                                </div>

                                <label> المدينة     </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="city" id="city" disabled>

                                </div>

                                <label> الشارع     </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="street" id="street" disabled>

                                </div>
                                <label> الرمز البريدي     </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="code" id="code" disabled>

                                </div>
                                {{--<div id="type-2">--}}
                                    {{--<label> وقت الاسنلام        </label>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<input type="text" class="form-control" name="delivery_at" id="delivery_at" disabled>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div id="type-0">
                                    <label> وقت الاستلام الأول        </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="deliv_first_time" id="deliv_first_time" disabled>
                                    </div>

                                    <label> وقت الاستلام الثاني        </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="deliv_end_time" id="deliv_end_time" disabled>
                                    </div>
                                </div>

                                <div id="type-1">
                                    <label> وقت التوصيل السريع        </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="quic_deliv_time" id="quic_deliv_time" disabled>
                                    </div>
                                </div>

                                <label>الإجمالي      </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="full_coast" id="full_coast" disabled>

                                </div>

                                {{--<label>سعر التوصيل      </label>--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" class="form-control" name="deliv_coast" id="deliv_coast" disabled>--}}

                                {{--</div>--}}

                                {{--<label>الكلفة النهائية   </label>--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" class="form-control" name="full" id="full" disabled>--}}

                                {{--</div>--}}

                                <label>الملاحظات     </label>
                                <div class="form-group">
                                    <textarea name="notes" id="notes" cols="30" rows="10" disabled></textarea>

                                </div>
                            </div>
                            <div class="col-md-8" id="offer_products">

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">

                        {{--<button type="button" data-id="0" id="editBtn" class="btn btn-primary">حفظ</button>--}}
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade text-left" id="StatusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel31" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel31">   تحديث حالة الطلبية   </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="statusForm" class="striped-rows">
                    @csrf
                    <input type="hidden" name="_order_id" id="_order_id">
                    <div class="modal-body">

                        <div class="form-group">

                            <select class="form-control" id="status_update" name="status_update">

                                @foreach($status as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach

                            </select>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">

                        <button type="button" data-id="0" id="SelectedBtn" class="btn btn-primary"  >حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- print forms -->
    <div id="print_form_1_print" style="display: none !important;">

    </div>

    {{--<div class="modal fade text-left" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel32" aria-hidden="true">--}}
        {{--<div class="modal-dialog modal-sm" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<label class="modal-title text-text-bold-600" id="myModalLabel31">  الموقع على الخريطة   </label>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true"><i class="fa fa-times"></i></span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                    {{--<div class="modal-body map mt-2">--}}

                        {{--<section class="map mt-2">--}}
                            {{--<div class="container-fluid p-4">--}}
                                {{--<div id="mapid" style="width: 100%; height: 400px;">--}}

                                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d451.3122849899881!2d55.2603857!3d25.1864135!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f694c700e12f9%3A0x190ce8d857467f06!2sPlant%20%26%20Equipment!5e0!3m2!1sen!2s!4v1603439689018!5m2!1sen!2s" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}

                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">--}}

                        {{--<button type="button" data-id="0" id="SelectedBtn" class="btn btn-primary"  >حفظ</button>--}}
                    {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


@endsection

@push('myjs')

    <script type="text/javascript">

        $(function () {

            // fill select delegate
            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });


            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

{{--                    url: "{{ route('crud.store',['table'=>'categories']) }}",--}}

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');


                        $('#addForm').trigger("reset");

                        alert(data.message);
                        // toastr.success('تم الحفظ بنجاح');

                        table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');



                    }

                });
            }); // end add new record


            $('body').on('click', '.mapCart', function () {

                var itemBtn = $(this);
                itemBtn.html('<i class="fa fa-load"></i> ... ');
                var product_id = $(this).data('id');

                $.get( "{{url('user-carts-request-map')}}"+ '/' + product_id, function (data) {

                    $('#loc_x').val(data.data.order.loc_x);
                    $('#loc_y').val(data.data.order.loc_y);
                    $("#mapModal").modal('show');

                        });


            }) ;// end function;

            $('body').on('click', '.deleteCart', function () {

                var product_id = $(this).data("id");
                var item =  $(this);

                sweetConfirm( function (confirmed) {
                    if (confirmed) {
                        $.ajax({

                            type: "DELETE",

                            url: "{{ url('user-carts-request-details') }}" + '/' + product_id,
                            data: {
                                '_token': '{{csrf_token()}}'
                            },
                            success: function (data) {
                                showSuccessDelete();
                                item.parent().parent().parent().parent().remove();
                                if ($('#order_div').is(':empty')) {

                                    $('#order_div').html(" <div class=\"alert alert-danger mb-2\" role=\"alert\" id=\"no_carts\">\n" +
                                        "                    <div class=\"alert-icon-left\">\n" +
                                        "                    <i class=\"fa fa-frown-o mr-2\"></i>\n" +
                                        "                    <span> لا توجد طلبيات مضافة </span>\n" +
                                        "                    </div>\n" +
                                        "                    </div>");
                                }


                            },

                            error: function (data) {
                            }

                        });
                    }

                });

            }) ;// end delete function;


            $('body').on('click', '.viewCart', function () {
                var itemBtn = $(this);
                $('#editFromData').trigger('reset');
                itemBtn.html('<i class="fa fa-load"></i> ... ');
                var product_id = $(this).data('id');
                var region = $(this).data('re');

                $.get( "{{url('user-carts-request-details')}}"+ '/' + product_id+"/"+ region, function (data) {

                    itemBtn.html('التفاصيل');

                    itemBtn.html('<i class="fa fa-info-circle"></i>  ');
                    $('#_id').val(data.data.order.id);
                    $("#saveBtn").attr('data-id',data.data.order.id);
                    $('#full_coast').val(data.data.order.total);
                    $('#notes').val(data.data.order.details);
                    $('#full_name').val(data.data.order.full_name);
                    $('#region').val(data.data.order.region);
                    $('#city').val(data.data.order.city);
                    $('#street').val(data.data.order.street);
                    $('#code').val(data.data.order.zip_code);
                    $('#address').val(data.data.order.region_address);

                    if (data.data.order.deliver_type == 0){
                        $('#type-1').hide();
                        $('#type-0').show();
                        $('#deliv_first_time').val(data.data.date.delivery_at);
                        $('#deliv_end_time').val(data.data.date.deliver_end_time);

                    }else if(data.data.order.deliver_type == 1){
                        $('#type-0').hide();
                        $('#type-1').show();
                        $('#quic_deliv_time').val(data.data.date.delivery_at);
                    }
                    // $('#deliv_coast').val(data.data.order.deliv_coast);
                    // var full = Number(data.data.order.total) + Number(data.data.order.deliv_coast);
                    // $('#full').val(full);


                    var x ="<div class=\"container\">" +
                        "<table class='table table-striped table-responsive' >" +
                        "<tr>" +
                        " <th>#</th>"+
                        " <th>الاسم</th>"+
                        " <th>صورة المنتج</th>" +
                        " <th>السعر</th>"+
                        " <th>اسم الوحدة</th>"+
                        " <th>الكمية</th>"+
                        " <th>الإجمالي</th>" +
                        "</tr>";

                    var ii=0;
                    var sum =0;
                    for (var property in data.data.products) {
                        ii++;
                        if (data.data.products.hasOwnProperty(property)) {
                            // Do things here

                            var xx = " المنتج";
                            // if (Number(data.products[property].po) == 0) {

                                xx = "المنتج";
                                x += "<tr>" +
                                    " <td>"+ii+"</td>";
                                x += " <td>" + data.data.products[property].product_name + "</td>";
                                var split = data.data.products[property].image;
                                if (split != null) {
                                    var images = split.split(",");
                                    var asset_url = '{{asset('/storage/')}}';
                                    x += " <td>" + "<img  id='" + images[0] + "' src='" + asset_url + "/" + images[0] + "' width=\"50\" height=\"50\">" + "</td>";
                                }else{
                                    x += " <td> </td>";
                                }
                                x += " <td>" + Number(data.data.products[property].pr_price) + "</td>";
                                x += " <td>" + data.data.products[property].type_a + "</td>";
                                x += " <td>" + data.data.products[property].p_amount + "</td>";

                                x += " <td>" + Number(data.data.products[property].pr_full_price) + "</td>" ;

                                x +="</tr>";

                            // }


                        }
                    }

                    x += " </table></div>";
                    $("#offer_products").html(x);
                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $('body').on('click','.doneBtn', function () {

                var itemBtn = $(this);
                var product_id = $(this).data('id');
                var status_value = $(this).data('status');

                    $("#status_update").val(status_value);
                    $("#_order_id").val(product_id);

                $("#StatusModal").modal('show');

            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ route('crud.index',['table'=>'categories']) }}"+ '/' + product_id + '/update',

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);

                        $('#editFromData').trigger("reset");

                        alert(data.message);


                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);



                    }

                });
            }); // end save  record data

            $('body').on('click', '#SelectedBtn', function (e) {
                e.preventDefault();

                $("#SelectedBtn").html('جاري الخفظ ..');
                $("#SelectedBtn").attr('disabled',true);
                var product_id = $("#_order_id").val();

                $.ajax({

                data: $('#statusForm').serialize(),
                url: "{{ url('user-carts-request-view')}}"+ '/' + product_id,

                type: "POST",

                dataType: 'json',
                timeout:4000,
                success: function (data) {

                    window.location.reload();
                },

                error: function (data) {
                    $("#SelectedBtn").html('حفظ');
                    $("#SelectedBtn").attr('disabled',false);
                }

                });
            }); // end save  record data

            $("#filterBtn").on('click', function (e) {

                $("#filterBtn").html('<i class="fa fa-spinner"></i>');
                var status = $("#status_filter").val();
                $('#order_div').html('');

                var check = '{{asset("app-assets/images/icon/check.ico")}}';
                var like = '{{asset("app-assets/images/icon/like.ico")}}';
                var cart = '{{asset("app-assets/images/icon/cart.ico")}}';
                var delivery = '{{asset("app-assets/images/icon/delivery.ico")}}';
                var trust = '{{asset("app-assets/images/icon/trust.ico")}}';
                var print = '{{url('print-document/')}}';

                $.get("{{url('filter-carts-requests')}}" + '/' + status, function (data) {
                    var x = "";
                    if (data.data && data.data.length > 0){

                        for( var i = 0; i < data.data.length; i++ ) {

                            var item = data.data[i];

                            x += '<div class="col-lg-4 col-md-4">' +
                                '<div class="card overflow-hidden" style="height: 300px;">' +
                                '<div class="card-content">' +
                                '<div class="card-img p-3">';

                            if (item.status == 0) {
                                x += '<a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="' + item.id+'" data-status="' + item.status +'"><img src="'+check+'"></a>';
                            }
                            else if (item.status == 1) {
                                x += '<a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="' +item.id +'" data-status="' + item.status +'"><img src="'+like+'"></a>';
                            }
                            else if (item.status == 2) {
                                x += '<a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="' +item.id +'" data-status="' + item.status +'"><img src="'+cart+'"></a>';
                            }
                            else if (item.status == 3) {
                                x += '<a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="' +item.id +'" data-status="' + item.status +'"><img src="'+delivery+'"></a>';
                            }
                            else if (item.status == 4) {
                                x += '<a class="btn btn-floating halfway-fab btn-sm doneBtn" data-id="' +item.id +'" data-status="' + item.status +'"><img src="'+trust+'"></a>';
                            }

                               x += '</div>' +
                                '<div class="card-body">' +
                                '<h4 class="card-title"># ' + item.id ;
                                   // + ' - ' +'<i class="fa fa-money"></i> '+ item.coast  +' </h4>' ;
                                    if(item.client)
                                   x+= '<h4 class="card-title"><i class="fa fa-user"></i> ' + item.client.name  + '</h4>' ;
                            else
                                x+= '<h4 class="card-title"> <i class="fa fa-user"></i>  ' + ''  + '</h4>' ;
                                    if(item.region_address)
                                x += '<p class="card-text"> <i class="fa fa-map-marker"></i> ' + item.city  +'</p>' ;
                            else
                                x += '<p class="card-text"> <i class="fa fa-map-marker"></i> ' + ''  +'</p>' ;
                                if(item.created_at)
                                x += '<p class="card-text"> <i class="fa fa-clock-o"></i> ' + item.created_at +'</p>' ;
                            else
                                x += '<p class="card-text"><i class="fa fa-clock-o"></i> ' + '' +'</p>';

                                x+='<p>' +

                                '</p>' +
                                '<button type="button"  class="btn btn-info viewCart" data-id="' +item.id +'" data-re="' +item.region_id +'"> <i class="fa fa-info-circle"></i> </button>&nbsp' +

                                '<a href="'+print+ '/'+ item.id + '/' + item.region_id +'" target="_blank" class="btn btn-outline-tumblr printCart"> <i class="fa fa-print"></i> </a>&nbsp' +

                                '<button type="button"  class="btn btn-danger deleteCart" data-id="' +item.id +' "> <i class="fa fa-trash-o"></i>  </button>&nbsp' +

                                '<button type="button"  class="btn btn-outline-grey mapCart" data-id="' +item.id +' "> <i class="fa fa-map-marker"></i>  </button>' +

                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            }
                        }
                    else{

                       x = '<div class="alert alert-danger mb-2" role="alert">' +
                        '<div class="alert-icon-left">' +
                        '<i class="fa fa-frown-o mr-2"></i>' +
                        '<span> لا توجد طلبيات مضافة </span>' +
                        '</div>' +
                        '</div>';

                }

                    $('#order_div').html(x);
                    $("#filterBtn").html('<i class="fa fa-filter"></i>');

                });


            });


            $("#editBtn").click(function (e) {
                e.preventDefault();

                $("#editBtn").html('جاري الخفظ ..');
                $("#editBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ url('update/order') }}"+ '/' + product_id,

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        if(data.status==200) {
                        $("#editBtn").html(' حفظ');
                        $("#editBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        showSuccesFunction();
                            $("#inlineForm").modal('hide');
                            table.draw(false);

                            // table.row.add(data.data).draw( false );
                        }else{
                            showErrorFunction();

                            $("#editBtn").attr('disabled', false);
                            $("#editBtn").html(' حفظ');
                        }


                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#editBtn").html(' حفظ');
                        $("#editBtn").attr('disabled',false);
                        showErrorFunction();

                    }

                });
            }); // end save  record data

        });

    </script>

<script>

</script>
@endpush
