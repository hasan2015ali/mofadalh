@extends("admin_layout")
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                قائمة الطبيات  المكتملة
                            </p>
                            <div class="row">

                            </div>
                            <div class="row">
                                @if(count($offers) > 0)
                                    @foreach($offers as $item)
                                        <div class="col-lg-4 col-md-12 col-12">
                                            <div class="card overflow-hidden" style="height: 400.547px;">
                                                <div class="card-content">
                                                    <div class="card-img">
                                                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                                            <ol class="carousel-indicators">
                                                                <li data-target="#carousel-example" data-slide-to="0" class=""></li>
                                                                <li data-target="#carousel-example" data-slide-to="1" class="active"></li>
                                                                <li data-target="#carousel-example" data-slide-to="2" class=""></li>
                                                            </ol>
                                                            <div class="carousel-inner" role="listbox">

                                                                <div class="carousel-item active carousel-item-left">
                                                                    <img src="{{asset('/srorage/products/uploads'.$item->products[0])}}" class="d-block w-100" style="height: 200px" alt="First slide">
                                                                </div>


                                                            </div>
                                                            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                                                <span class="fa fa-angle-left icon-prev" aria-hidden="true"></span>
                                                                <span class="sr-only">Previous</span>
                                                            </a>
                                                            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                                                <span class="fa fa-angle-right icon-next" aria-hidden="true"></span>
                                                                <span class="sr-only">Next</span>
                                                            </a>
                                                        </div>
                                                        <a class="btn btn-floating halfway-fab btn-large bg-warning doneBtn" data-id="{{$item->id}}" ><i class="fa fa-check-circle-o"></i></a>
                                                    </div>
                                                    <div class="card-body">
                                                        <h4 class="card-title">رقم:  {{$item->id}} -  <i class="fa fa-money"></i>  {{$item->coast}}   </h4>
                                                        <h4 class="card-title"> الصيدلي : {{$item->pharmacist->name ?? 'صيدلي محذوف'}}  </h4>
                                                        <p class="card-text"> المندوب : {{$item->delegate->name ?? 'غير محدد'}} </p>
                                                        <p class="card-text"> التاريخ : {{$item->created_at ?? 'غير محدد'}} </p>
                                                        <button type="button"  class="btn btn-warning viewCart" data-id="{{$item->id}}"> التفاصيل </button>
                                                        <button type="button"   class="btn btn-warning printCart" data-id="{{$item->id}}"> طباعة <i class="fa fa-print"></i> </button>
                                                        <button type="button"  class="btn btn-danger deleteCart" data-id="{{$item->id}}"> <i class="fa fa-trash-o"></i>  </button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else

                                    <div class="alert alert-danger mb-2" role="alert">
                                        <div class="alert-icon-left">
                                            <i class="fa fa-frown-o mr-2"></i>
                                            <span> لاتوجد طلبيات مضافة </span>
                                        </div>
                                    </div>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">   تفاصيل الطلبية    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromData" class="striped-rows">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>الإجمالي      </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="full_coast" id="full_coast">

                                </div>

                                 <label> الصيدلية       </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="pharma" id="pharma">

                                </div>

                                <label> المندوب   </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="delegate" id="delegate">

                                </div>

                                <label>الملاخظات     </label>
                                <div class="form-group">
                                    <textarea name="notes" id="notes" cols="30" rows="10"></textarea>

                                </div>
                            </div>
                            <div class="col-md-8" id="offer_products">

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">

                        <input type="button" id="saveBtn" class="btn btn-primary printCart" value="طباعة">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- print forms -->
    <div id="print_form_1_print" style="display: none !important;">

    </div>

@endsection

@push('myjs')

    <script type="text/javascript">

        $(function () {

            // fill select delegate



            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('crud.store',['table'=>'categories']) }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');


                        $('#addForm').trigger("reset");

                        alert(data.message);
                        // toastr.success('تم الحفظ بنجاح');

                        table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');



                    }

                });
            }); // end add new record


            {{--$(".viewCart").click( function () {--}}
            {{--var itemBtn = $(this);--}}
            {{--itemBtn.html('<i class="fa fa-load"></i> ... ');--}}
            {{--var product_id = $(this).data('id');--}}

            {{--$.get("{{ route('crud.index',['table'=>'cart']) }}" + '/' + product_id + '/edit', function (data) {--}}



            {{--itemBtn.html('التفاصيل');--}}



            {{--$('#_id').val(data.id);--}}
            {{--$("#saveBtn").attr('data-id',data.id);--}}
            {{--$('#full_coast').val(data.item.coast);--}}
            {{--$('#notes').val(data.item.notes);--}}
            {{--if(data.item.pharmacist){--}}

            {{--$('#pharma').val(data.item.pharmacist.name);--}}

            {{--}--}}
            {{--else{--}}
            {{--$('#pharma').val("غير  محدد");--}}
            {{--}--}}
            {{--if(data.item.delegate){--}}

            {{--$('#delegate').val(data.item.delegate.name);--}}

            {{--}--}}
            {{--else{--}}
            {{--$('#delegate').val("غير  محدد");--}}
            {{--}--}}


            {{--var x ="<div class=\"form-group row\">";--}}
            {{--for (var i=0;i<data.products.length;i++){--}}
            {{--console.log(data.products[i]);--}}
            {{--var xx = " المنتج";--}}

            {{--if(Number(data.products[i].po )==0){--}}
            {{--xx = "المنتج";--}}
            {{--x +="<div class=\"form-group row\">";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >"+xx+"</label><br> <input type=\"text\" class='form-control' value='"+data.products[i].name+"'  disabled> </div> ";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >الكمية</label><br> <input type=\"text\"  value='"+data.products[i].quantity+"'  disabled> </div> ";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >السعر </label><br> <input type=\"text\"  value='"+Number(data.products[i].price)+"'  disabled> </div> ";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >الكلي  </label><br> <input type=\"text\"  value='"+Number(data.products[i].price)*Number(data.products[i].quantity)+"'  disabled> </div> ";--}}

            {{--// x += " <div class=\"form-group\"><label class=\"label-control\" >الكلي </label><br> <input type=\"text\"  value='"+data.products[i].single_price+"'  disabled> </div> ";--}}
            {{--if(data.products[i].option_title)--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" > '"+data.products[i].option_title+"' </label><br> <input type=\"text\"  value='"+data.products[i].option+"'  disabled> </div> ";--}}


            {{--x += " </div>";--}}
            {{--}--}}
            {{--else{--}}
            {{--xx = "العرض";--}}
            {{--x +="<div class=\"form-group row\">";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >"+xx+"</label><br> <input type=\"text\"  value='"+data.products[i].name+"'  disabled> </div> ";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >الكمية</label><br> <input type=\"text\"  value='"+data.products[i].quantity+"'  disabled> </div> ";--}}
            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >السعر </label><br> <input type=\"text\"  value='"+data.products[i].price+"'  disabled> </div> ";--}}
            {{--// if(data.products[i].option_title)--}}

            {{--x += " <div class=\"form-group\"><label class=\"label-control\" >الكلي </label><br> <input type=\"text\"  value='"+data.products[i].price+"'  disabled> </div> ";--}}


            {{--x += " </div>";--}}
            {{--}--}}


            {{--}--}}

            {{--x += " </div>";--}}
            {{--$("#offer_products").html(x);--}}
            {{--$("#inlineForm").modal('show');--}}
            {{--})--}}


            {{--}) ;// end edit function;--}}
            $(".viewCart").click( function () {
                var itemBtn = $(this);
                itemBtn.html('<i class="fa fa-load"></i> ... ');
                var product_id = $(this).data('id');

                $.get("{{ route('crud.index',['table'=>'cart']) }}" + '/' + product_id + '/edit', function (data) {



                    itemBtn.html('التفاصيل');



                    $('#_id').val(data.item.id);
                    $("#saveBtn").attr('data-id',data.item.id);
                    $('#full_coast').val(data.item.coast);
                    $('#notes').val(data.item.notes);
                    if(data.item.pharmacist){

                        $('#pharma').val(data.item.pharmacist.name);

                    }
                    else{
                        $('#pharma').val("غير  محدد");
                    }
                    if(data.item.delegate){

                        $('#delegate').val(data.item.delegate.name);

                    }
                    else{
                        $('#delegate').val("غير  محدد");
                    }


                    var x ="<div class=\"container\">" +
                        "<table class='table table-striped table-responsive' >" +
                        "<tr>" +
                        " <th>#</th>"+
                        " <th>النوع</th>"+
                        " <th>الاسم</th>"+
                        " <th>الرمز</th>"+
                        " <th>السعر</th>"+
                        " <th>الخيارات</th>"+
                        " <th>الكمية</th>"+
                        " <th>الإجمالي</th>" +
                        "</tr>";
                    var ii=0;
                    for (var property in data.products) {
                        ii++;
                        if (data.products.hasOwnProperty(property)) {
                            // Do things here

                            var xx = " المنتج";

                            if (Number(data.products[property].po) == 0) {

                                xx = "المنتج";
                                x += "<tr>" +
                                    " <td>"+ii+"</td>";
                                x += " <td>" + xx + "</td>";
                                x += " <td>" + data.products[property].name + "</td>";
                                x += " <td>" + data.products[property].code + "</td>";
                                x += " <td>" + Number(data.products[property].price) + "</td>";
                                x += " <td>" ;
                                for(var op1 in data.products[property].option){
                                    x+= data.products[property].option[op1].op +":" + data.products[property].option[op1].q +"<br>"
                                }
                                "" +
                                "</td>";


                                x += " <td>" + data.products[property].quantity + "</td>";

                                x += " <td>" + Number(data.products[property].price) * Number(data.products[property].quantity) + "</td>" +
                                    "</tr>";
                                // x +="<div class=\"form-group row\">";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >"+xx+"</label><br> <input type=\"text\" class='form-control' value='"+data.products[i].name+"'  disabled> </div> ";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >الكمية</label><br> <input type=\"text\"  value='"+data.products[i].quantity+"'  disabled> </div> ";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >السعر </label><br> <input type=\"text\"  value='"+Number(data.products[i].price)+"'  disabled> </div> ";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >الكلي  </label><br> <input type=\"text\"  value='"+Number(data.products[i].price)*Number(data.products[i].quantity)+"'  disabled> </div> ";
                                //
                                // // x += " <div class=\"form-group\"><label class=\"label-control\" >الكلي </label><br> <input type=\"text\"  value='"+data.products[i].single_price+"'  disabled> </div> ";
                                // if(data.products[i].option_title)
                                // x += " <div class=\"form-group\"><label class=\"label-control\" > '"+data.products[i].option_title+"' </label><br> <input type=\"text\"  value='"+data.products[i].option+"'  disabled> </div> ";
                                //
                                //
                                // x += " </div>";
                            }
                            else {
                                xx = "العرض";
                                x += "<tr>" +
                                    " <td>"+ii+"</td>";
                                x += " <td>" + xx + "</td>";
                                x += " <td>" + data.products[property].name + "</td>";
                                x += " <td>" + data.products[property].code + "</td>";
                                x += " <td>" + Number(data.products[property].price) + "</td>";
                                x += " <td>الخيارات</td>";


                                x += " <td>" + data.products[property].quantity + "</td>";

                                x += " <td>" + Number(data.products[property].price) * Number(data.products[property].quantity) + "</td>" +
                                    "</tr>";
                                // x +="<div class=\"form-group row\">";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >"+xx+"</label><br> <input type=\"text\"  value='"+data.products[i].name+"'  disabled> </div> ";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >الكمية</label><br> <input type=\"text\"  value='"+data.products[i].quantity+"'  disabled> </div> ";
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >السعر </label><br> <input type=\"text\"  value='"+data.products[i].price+"'  disabled> </div> ";
                                // // if(data.products[i].option_title)
                                //
                                // x += " <div class=\"form-group\"><label class=\"label-control\" >الكلي </label><br> <input type=\"text\"  value='"+data.products[i].price+"'  disabled> </div> ";
                                //
                                //
                                // x += " </div>";
                            }


                        }
                    }

                    x += " </table></div>";
                    $("#offer_products").html(x);
                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            {{--$("#saveBtn").click(function (e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#saveBtn").html('جاري الخفظ ..');--}}
                {{--$("#saveBtn").attr('disabled',true);--}}
                {{--var product_id = $("#_id").val();--}}
                {{--$.ajax({--}}

                    {{--data: $('#editFromData').serialize(),--}}

                    {{--url: "{{ route('crud.index',['table'=>'categories']) }}"+ '/' + product_id + '/update',--}}

                    {{--type: "POST",--}}

                    {{--dataType: 'json',--}}
                    {{--timeout:4000,--}}
                    {{--success: function (data) {--}}
                        {{--$("#saveBtn").html(' حفظ');--}}
                        {{--$("#saveBtn").attr('disabled',true);--}}

                        {{--$('#editFromData').trigger("reset");--}}

                        {{--alert(data.message);--}}


                        {{--// table.row.add(data.data).draw( false );--}}

                    {{--},--}}

                    {{--error: function (data) {--}}
                        {{--$("#saveBtn").html(' حفظ');--}}
                        {{--$("#saveBtn").attr('disabled',true);--}}



                    {{--}--}}

                {{--});--}}
            {{--}); // end save  record data--}}

            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                var co = confirm("  هل أنت متأكد من الحذف  !");
                if (!co) {
                    return;
                }


                $.ajax({

                    type: "DELETE",

                    url: "{{ route('crud.index',['table'=>'categories']) }}"+ '/' + product_id +"/delete",
                    data:{
                        '_token':'{{csrf_token()}}'
                    },
                    success: function (data) {
                        alert(data.message);
                        table.draw(false);

                    },

                    error: function (data) {
                        toastr.error("حدث خطأ     ");
                        console.log('خطأ:', data);

                    }

                });

            }); // end delete row
            $(".deleteCart").click( function () {

                var product_id = $(this).data("id");

                Swal.fire({
                    title: "طول بالك !",
                    text: "يعني عنجد رح تحذفني ؟ ",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonColor: "#2F8BE6",
                    cancelButtonColor: "#F55252",
                    confirmButtonText: "احذف وقول الله",
                    confirmButtonClass: "btn btn-primary",
                    cancelButtonClass: "btn btn-danger ml-1",
                    cancelButtonText: "بلاها رجعنا",
                    buttonsStyling: !1
                }).then(function (t) {

                    if(t.value){
                        $.ajax({

                            type: "DELETE",

                            url: "{{ route('crud.index',['table'=>'carts']) }}"+ '/' + product_id +"/delete",
                            data:{
                                '_token':'{{csrf_token()}}'
                            },
                            success: function (data) {
                                Swal.fire({
                                    type: "success",
                                    title: "راح!",
                                    text: "تم الحذف بنجاح",
                                    confirmButtonClass: "btn btn-success"
                                });
                                window.location.reload();

                            },

                            error: function (data) {



                            }

                        });
                    }

                }) ;







            }) ;// end delete function;

        $(".doneBtn").click(function (e) {
            e.preventDefault();
            var item = $(this);


            var item_id = $(this).data("id");

            $.ajax({

                data: {
                    "item_id":item_id,
                    "status":1,
                    "_token":$("input[name=_token]").val()

                },

                url: "{{ route('crud.index',['table'=>'cart']) }}"+ '/' + item_id + '/update',

                type: "POST",

                dataType: 'json',
                timeout:4000,
                success: function (data) {


                    item.removeClass("bg-warning");
                    item.addClass("bg-success");



                },

                error: function (data) {



                }

            });


        });

        $(".printCart").click(function (e) {
            e.preventDefault();
            var item = $(this);


            var item_id = $(this).data("id");


            $.ajax({
                url:"{{route('app.print-document')}}",
                method:"POST",
                data:{
                    '_token':$('input[name=_token]').val(),
                    'cart_id':item_id
                },
                // dataType:"json",
                success:function(data)
                {
                    $("#print_form_1_print").html(data);
                    printReport(data);

                },
                error:function(data){
                    alert(data.responseText);
                }
            })



        });

        });



    </script>

@endpush