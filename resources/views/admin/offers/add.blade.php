@extends("admin_layout")
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <section id="input-style">
        <form action="{{route('crud.store',['table'=>'offers'])}}" method="post" id="addForm">

        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                إضافة عرض جديد
                            </p>

                                <div class="row">
                                    @csrf

                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">   عنوان العرض    </label>
                                            <input type="text" id="offer_name" name="offer_name" class="form-control round" placeholder="  ادخل اسم العرض ">


                                        </fieldset>
                                    </div>
                                    <div class="col-sm-2">
                                        <fieldset class="form-group">
                                            <label for="roundText">   بعد الحسم </label>
                                            <input type="text" id="offer_price" name="offer_price" class="form-control round" placeholder="  ادخل السعر الكلي للعرض">


                                        </fieldset>
                                    </div>
                                    <div class="col-sm-2">
                                        <fieldset class="form-group">
                                            <label for="roundText">   قبل الحسم   </label>
                                            <input type="text" id="f_price" name="f_price" value="0" class="form-control round" placeholder="  ادخل السعر الكلي للعرض">


                                        </fieldset>
                                    </div>  <div class="col-sm-2">
                                        <fieldset class="form-group">
                                            <label for="roundText">    الحسم   </label>
                                            <input type="text" id="d_price" name="d_price" value="0" class="form-control round" placeholder="  ادخل السعر الكلي للعرض">


                                        </fieldset>
                                    </div>
                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">   الوصف  </label>
                                            <input type="text" id="details" name="details" class="form-control round" placeholder="  الوصف">


                                        </fieldset>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">  اسم المنتج  </label>
                                            <select class="form-control select2" id="product_id" name="product_id">
                                                @foreach($products as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>


                                        </fieldset>
                                    </div>
                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">  الكمية    </label>
                                            <input type="number" class="form-control" name="quat" id="quant" value="1">


                                        </fieldset>
                                    </div>
                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">  السعر      </label>
                                            <input type="number" class="form-control" name="product_price" id="product_price">

                                        </fieldset>
                                    </div>
                                    <div class="col-sm-3">
                                        <fieldset class="form-group">
                                            <label for="roundText">  الإجمالي    </label>

                                            <select name="decrrease_num" id="decrrease_num">
                                                @for($i =0;$i<50;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                            <button type="button" class="btn gradient-pomegranate decrease "><i class="fa fa-trash-o"></i></button>
                                            <button type="button" class="btn gradient-pomegranate reset "><i class="fa fa-refresh"></i></button>

                                            <br>
                                            <input type="number" class="form-control" name="products_price" id="products_price">

                                            <br>
                                            <button type="button" id="addProductBtn" class="btn gradient-purple-bliss">إضافة منتج</button>

                                        </fieldset>
                                    </div>
                                </div>

                            <div class="row">

                                <div class="col-sm-12">
                                    <table class="table tab-info" id="raw_materials_table">
                                        <tr>
                                            <th>المادة</th>
                                            <th>الكمية </th>
                                            <th>السعر الافرادي </th>

                                            <th>السعر الاجمالي</th>
                                            <th>العمليات</th>
                                        </tr>
                                    </table>
                                    <div class="row">
                                       <div class="container">
                                           <div class="checkbox">
                                               <input type="checkbox" id="checkbox2"  name="rec[]" value="2">
                                               <label for="checkbox2"><span>الصيدلاني</span></label>
                                           </div>
                                           <div class="checkbox">
                                               <input type="checkbox" id="checkbox3" name="rec[]" value="3">
                                               <label for="checkbox3"><span>المندوب</span></label>
                                           </div>
                                           <br>
                                       </div>
                                        <button type="button" id="addFormBtn" class="btn gradient-purple-bliss"> حفظ العرض</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل بيانات المنتج  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromData">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <fieldset class="form-group">
                                <label for="roundText">  اسم المنتج</label>
                                <input type="text" id="name_edit" name="name" class="form-control round" placeholder="  ادخل اسم الفئة">


                            </fieldset>
                        </div>
                        <div class="col-sm-12">
                            <fieldset class="form-group">
                                <label for="roundText"> السعر </label>
                                <input type="text" id="price_edit" name="price" class="form-control round" placeholder="  ادخل اسم الفئة">


                            </fieldset>
                        </div>
                        <div class="col-sm-12">
                            <fieldset class="form-group">
                                <label for="roundText">   رمز المنتج</label>
                                <input type="text" id="code_edit" name="code" class="form-control round" placeholder="  ادخل اسم الفئة">


                            </fieldset>
                        </div> <div class="col-sm-12">
                            <fieldset class="form-group">
                                <label for="roundText">  الفئة  </label>
                                <select class="form-control" id="cat_id_edit" name="cat_id">

                                </select>


                            </fieldset>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveBtn" class="btn btn-primary" value="حفظ">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('myjs')

    <script type="text/javascript">

        $(function () {

            $("#addProductBtn").click(function (e) {
                e.preventDefault();
                var prodcut_name = $("#product_id option:selected").text();
                var raw_id = $("#product_id").val();
                var raw_qua = $("#quant").val();
                var raw_price = $("#product_price").val();
                var products_price = $("#products_price").val();
                var money = Number( $("#offer_price").val() );
                var full_price = Number(raw_qua)*Number(raw_price);
                full_price += Number($("#f_price").val());
                money += Number( products_price );
                $("#offer_price").val(money);
                $("#f_price").val(full_price);
                $("#d_price").val(full_price-money);
                $("#money").val(money);
                $("#money_paid").val(money);
                $("#offer_price").val(money);
                $("#raw_materials_table").append("<tr>" +
                    "<td><input type='hidden' name='products_id1[]' value='"+raw_id+"'>"+prodcut_name+"</td>" +
                    "<td><input type='number' name='raws_qua[]' class='qua_edit'   value='"+raw_qua+"'> "+ "</td>" +
                    "<td><input type='number' name='raws_price[]' value='"+raw_price+"'> "+ "</td>" +
                    "<td><input type='number' name='products_price[]' value='"+products_price+"'> "+ "</td>" +
                    "<td> <button class='btn btn-danger deleteProductFromOffer' data-products_price='"+products_price+"' data-raw_price='"+raw_price+"' data-raw_qua='"+raw_qua+"'> <i class='fa fa-trash'></i> </button> </td>" +

                    "</tr>");

            });

            $("#product_id").on('change', function(){
                var product_id =this.value;


                $.get("{{ route('crud.index',["table"=>"products"]) }}" + '/' + product_id + '/edit', function (data) {

                    var price = data.price;
                    var count = $("#quant").val();
                    $("#product_price").val(price);
                    $("#products_price").val(count * price);
                });

                });

            $("#quant").on('change', function(){

                    var price = $("#product_price").val();
                    var count = $("#quant").val();

                    $("#products_price").val(count * price);


                });
            $("#product_id").select2({
                    dir: "rtl",
                    dropdownAutoWidth: true
                }
                );



            $("#addFormBtn").click(function (e) {
                e.preventDefault();
                var offer_price = $("#offer_price").val();

                if(Number(offer_price) <=0){
                    Swal.fire({title:"  عذرا    ",text: "الرجاء إضافة منتجات إلى العرض ",
                        type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                    return;
                }
                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    {{--data: {--}}
                        {{--"offer_name":$("#offer_name").val(),--}}
                        {{--"offer_price":$("#offer_price").val(),--}}
                        {{--"details":$("#details").val(),--}}
                        {{--"products":$("input[name='products_id1[]']").val(),--}}
                        {{--"products_qua[]":$("input[name='raws_qua[][]']").val(),--}}
                        {{--"_token":'{{csrf_token()}}'--}}
                    {{--},--}}
                    data:$("#addForm").serialize(),
                    url: "{{ route('crud.store',['table'=>'offers']) }}",

                    type: "POST",

                    dataType: 'json',

                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');

if(data.status ==200)
                        $('#addForm').trigger("reset");
                if(data.status ==200){
                    Swal.fire({title:" تهانينا ",text:"تم حفظ العرض بنجاح",
                        type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});
                }
                else{
                    Swal.fire({title:"حدث خطأ ما",text:data.message,
                        type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});

                }



                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');



                    }

                });
            }); // end add new record

            $('body').on('click', '.edit', function () {

                var product_id = $(this).data('id');

                $.get("{{ route('crud.index',['table'=>'products']) }}" + '/' + product_id + '/edit', function (data) {




                    $("#saveBtn").val("حفظ");


                    $('#_id').val(data.id);

                    $('#name_edit').val(data.name);
                    $('#price_edit').val(data.price);
                    $('#code_edit').val(data.code);
                    $('#cat_id_edit').val(data.cat_id);



                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;
            $('body').on('click', '.deleteProductFromOffer', function (e) {
                e.preventDefault();
                var item = $(this);
                item.parent().parent().remove();
                // var product_id = $(this).data('id');

                var raw_id = $("#product_id").val();
                var raw_qua = item.data('raw_qua');
                var raw_price = item.data('raw_price');
                var products_price = item.data('products_price');

            var before_dis = Number(raw_price)*Number(raw_qua);
            var after_dis = Number(products_price);

                var money = Number( $("#offer_price").val() );
                var full_price = Number($("#f_price").val());
                full_price -= before_dis ;
                money -= Number( products_price );
                $("#offer_price").val(money);
                $("#f_price").val(full_price);
                $("#d_price").val(full_price-money);
                $("#money").val(money);
                $("#money_paid").val(money);
                // $("#offer_price").val(money);

            }) ;// end edit function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ route('crud.index',['table'=>'products']) }}"+ '/' + product_id + '/update',

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);

                        $('#editFromData').trigger("reset");

                        alert(data.message);


                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',true);



                    }

                });
            }); // end save  record data

            $('body').on('click', '.delete', function () {


                var product_id = $(this).data("id");

                var co = confirm("  هل أنت متأكد من الحذف  !");
                if (!co) {
                    return;
                }


                $.ajax({

                    type: "DELETE",

                    url: "{{ route('crud.index',['table'=>'products']) }}"+ '/' + product_id +"/delete",
                    data:{
                        '_token':'{{csrf_token()}}'
                    },
                    success: function (data) {
                        alert(data.message);
                        table.draw(false);

                    },

                    error: function (data) {
                        toastr.error("حدث خطأ     ");
                        console.log('خطأ:', data);

                    }

                });

            }); // end delete row
            
            $(".decrease").click(function (e) {
                e.preventDefault();
                var num = $("#decrrease_num").val();
                var all_price = $("#products_price").val();
                var item_price = $("#product_price").val();
                num = Number(num);
                all_price = Number(all_price);
                item_price = Number(item_price);
                var remian = all_price - (num * item_price);
                $("#products_price").val(remian);

            });
     $(".reset").click(function (e) {
                e.preventDefault();
                var num = $("#quant").val();

                var item_price = $("#product_price").val();
                num = Number(num);

                item_price = Number(item_price);
                var remian =   (num * item_price);
                $("#products_price").val(remian);

            });

        });

    </script>

@endpush