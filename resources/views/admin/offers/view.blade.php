@extends("admin_layout")
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" ></script>
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                              قائمة العروض
                            </p>
                            <div class="row">

                            </div>
                            <div class="row">
                                @if(count($offers) > 0)
                        @foreach($offers as $item)
                                <div class="col-lg-4 col-md-12 col-12">
                                    <div class="card overflow-hidden" style="height: 400.547px;">
                                        <div class="card-content">
                                            <div class="card-img">
                                                <div id="carousel-example{{$item->id}}" class="carousel slide" data-ride="carousel">
                                                    <ol class="carousel-indicators">
                                                        <li data-target="#carousel-example{{$item->id}}" data-slide-to="0" class="active"></li>
                                                        <li data-target="#carousel-example{{$item->id}}" data-slide-to="1" class=""></li>
                                                        <li data-target="#carousel-example{{$item->id}}" data-slide-to="2" class=""></li>
                                                    </ol>
                                                    <div class="carousel-inner" role="listbox">

                                                        @foreach( $item->products1 as $item2)
                                                        <div class="carousel-item  carousel-item-left @if($loop->index ==0) active @endif">
                                                            <img src="{{asset('/storage/'.explode(",",$item2->images)[0] )}}" class="d-block w-100" style="height: 200px" alt="First slide">
                                                        </div>

                                                @endforeach
                                                        @if(count($item->products1) <2)
                                                                <div class="carousel-item  carousel-item-left @if($loop->index ==0) active @endif">
                                                                    <img src="{{asset('/storage/'.explode(",",$item->products1->first()->images)[0] )}}" class="d-block w-100" style="height: 200px" alt="First slide">
                                                                </div>
                                                            @endif


                                                    </div>
                                                    <a class="carousel-control-prev" href="#carousel-example{{$item->id}}" role="button" data-slide="prev">
                                                        <span class="fa fa-angle-left icon-prev" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carousel-example{{$item->id}}" role="button" data-slide="next">
                                                        <span class="fa fa-angle-right icon-next" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                                <a class="btn btn-floating halfway-fab btn-large bg-warning"><i class="fa fa-check-circle-o"></i></a>
                                            </div>
                                            <div class="card-body">
                                                <h4 class="card-title">   <i class="fa fa-money"></i>  {{$item->price}}   </h4>
                                                <h4 class="card-title"> {{$item->name}}  </h4>
                                                <p class="card-text"> {{$item->details}} </p>
                                                <button type="button"  class="btn btn-warning editOffer" data-id="{{$item->id}}"> التفاصيل </button>
                                                <button type="button"  class="btn btn-danger deleteOffer" data-id="{{$item->id}}"> <i class="fa fa-trash-o"></i>  </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    @endforeach
                                    @else

                                    <div class="alert alert-danger mb-2" role="alert">
                                        <div class="alert-icon-left">
                                            <i class="fa fa-frown-o mr-2"></i>
                                            <span> لاتوجد عروض مضافة </span>
                                        </div>
                                    </div>

                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"> تعديل بيانات العرض  </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times"></i></span>
                    </button>
                </div>
                <form action="#" method="post" id="editFromData" class="striped-rows">
                    @csrf
                    <input type="hidden" name="_id" id="_id">
                    <div class="modal-body">
                       <div class="row">

                           <div class="col-sm-3">
                               <fieldset class="form-group">
                                   <label for="roundText">   عنوان العرض    </label>
                                   <input type="text" id="offer_name" name="offer_name" class="form-control round" placeholder="  ادخل اسم العرض ">


                               </fieldset>
                           </div>
                           <div class="col-sm-2">
                               <fieldset class="form-group">
                                   <label for="roundText">   بعد الحسم </label>
                                   <input type="text" id="offer_price" name="offer_price" class="form-control round" placeholder="  ادخل السعر الكلي للعرض">


                               </fieldset>
                           </div>
                           <div class="col-sm-2">
                               <fieldset class="form-group">
                                   <label for="roundText">   قبل الحسم   </label>
                                   <input type="text" id="f_price" name="f_price" value="0" class="form-control round" placeholder="  ادخل السعر الكلي للعرض">


                               </fieldset>
                           </div>  <div class="col-sm-2">
                               <fieldset class="form-group">
                                   <label for="roundText">    الحسم   </label>
                                   <input type="text" id="d_price" name="d_price" value="0" class="form-control round" placeholder="  ادخل السعر الكلي للعرض">


                               </fieldset>
                           </div>
                           <div class="col-sm-3">
                               <fieldset class="form-group">
                                   <label for="roundText">   الوصف  </label>
                                   <input type="text" id="details" name="details" class="form-control round" placeholder="  الوصف">


                               </fieldset>
                           </div>
                           <div class="col-md-8" id="offer_products">

                           </div>
                       </div>


                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn bg-light-secondary" data-dismiss="modal" value="إغلاق">
                        <input type="submit" id="saveBtn" class="btn btn-primary" value="حفظ">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('myjs')

    <script type="text/javascript">

        $(function () {

            // fill select delegate



            $("#addFormBtn").click(function (e) {
                e.preventDefault();

                $("#addFormBtn").html('<i class="fa fa-load"></i> ... ');
                $.ajax({

                    data: $('#addForm').serialize(),

                    url: "{{ route('crud.store',['table'=>'categories']) }}",

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#addFormBtn").html('إضافة');


                        $('#addForm').trigger("reset");

                        alert(data.message);
                        // toastr.success('تم الحفظ بنجاح');

                        table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#addFormBtn").html('إضافة');



                    }

                });
            }); // end add new record

            $(".editOffer").click( function () {

                var product_id = $(this).data('id');

                $.get("{{ route('crud.index',['table'=>'offers']) }}" + '/' + product_id + '/edit', function (data) {




                    $("#saveBtn").val("حفظ");


                    $('#_id').val(data.id);

                    $('#offer_name').val(data.name);
                    $('#offer_price').val(data.price);
                    $('#f_price').val(data.f_price);
                    $('#d_price').val(data.d_price);
                    $('#details').val(data.details);
                    var x ="<div class=\"form-group row\">";
                        for (var i=0;i<data.products1.length;i++){
                            x +="<div class=\"form-group row\">";
                             x += " <div class=\"form-group\"><label class=\"label-control\" >المنتج</label><br> <input type=\"text\"  value='"+data.products1[i].name+"'  disabled> </div> ";
                             x += "  <input  type=\"hidden\" name=\"productid1[]\" value='"+data.products1[i].price+"'> ";
                             x += "<div class=\"form-group\"><label class=\"label-control\">الكمية </label> <br> <input type=\"number\" name=\"productcount[]\" style='width:100px' value='"+data.products1[i].pivot.count+"'> </div>";
                             x += "<div class=\"form-group\"><label class=\"label-control\">السعر</label><br> <input type=\"number\" name=\"full_price_p[]\" style='width:100px' value='"+data.products1[i].pivot.full_price+"'> </div>";
                             x += "<div class=\"form-group\"><label class=\"label-control\">السعر بعد الخصم</label> <br><input type=\"number\" name=\"d_price_p[]\" style='width:100px' value='"+data.products1[i].pivot.d_price+"'> <button class='btn btn-danger deleteProductFromOffer'" +
                                 " data-products_price='"+data.products1[i].pivot.public_price+"' data-raw_price='"+data.products1[i].price+"' data-raw_qua='"+data.products1[i].pivot.count+"'> <i class='fa fa-trash'></i> </button></div>";
                            x += " </div>";
                        }

                    x += " </div>";
                    $("#offer_products").html(x);
                    $("#inlineForm").modal('show');
                })


            }) ;// end edit function;

            $(".deleteOffer").click( function () {

                var product_id = $(this).data("id");

                Swal.fire({
                    title: "طول بالك !",
                    text: "يعني عنجد رح تحذفني ؟ ",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonColor: "#2F8BE6",
                    cancelButtonColor: "#F55252",
                    confirmButtonText: "احذف وقول الله",
                    confirmButtonClass: "btn btn-primary",
                    cancelButtonClass: "btn btn-danger ml-1",
                    cancelButtonText: "بلاها رجعنا",
                    buttonsStyling: !1
                }).then(function (t) {

                   if(t.value){
                       $.ajax({

                           type: "DELETE",

                           url: "{{ route('crud.index',['table'=>'offers']) }}"+ '/' + product_id +"/delete",
                           data:{
                               '_token':'{{csrf_token()}}'
                           },
                           success: function (data) {
                               Swal.fire({
                                   type: "success",
                                   title: "راح!",
                                   text: "تم الحذف بنجاح",
                                   confirmButtonClass: "btn btn-success"
                               });
                               window.location.reload();

                           },

                           error: function (data) {



                           }

                       });
                   }

                }) ;







            }) ;// end delete function;

            $("#saveBtn").click(function (e) {
                e.preventDefault();

                $("#saveBtn").html('جاري الخفظ ..');
                $("#saveBtn").attr('disabled',true);
                var product_id = $("#_id").val();
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: "{{ route('crud.index',['table'=>'offers']) }}"+ '/' + product_id + '/update',

                    type: "POST",

                    dataType: 'json',
                    timeout:4000,
                    success: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);

                        $('#editFromData').trigger("reset");

                        alert(data.message);


                        // table.row.add(data.data).draw( false );

                    },

                    error: function (data) {
                        $("#saveBtn").html(' حفظ');
                        $("#saveBtn").attr('disabled',false);



                    }

                });
            }); // end save  record data




        });

    </script>

@endpush
