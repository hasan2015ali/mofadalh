@extends("admin_layout")
@section('content')

    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <p>
                               إعدادات الطباعة
                            </p>

                            <form action="{{route('settings.print.update')}}" id="formData" method="post" enctype="multipart/form-data">
                                <div class="row">

                                    @csrf


                                    <div class="col-sm-12">
                                        <fieldset class="form-group">
                                            <label for="roundText">  لوغو الطباعة  </label>
                                            @if(strlen($print_logo->title)> 0)
                                                <img src="{{url('public/storage/'.$print_logo->title)}}" alt="" width="100" height="100">
                                            @endif
                                            <input type="file" name="print_logo" value="Choose File">

                                        </fieldset>
                                    </div>

                                    <div class="col-sm-9">
                                        <fieldset class="form-group">
                                            <label for="roundText"> اسم الشركة في الطباعة </label>
                                            <input type="text" id="email" name="print_co" class="form-control round" placeholder=" " value="{{$print_co->title}}">


                                        </fieldset>
                                    </div>



                                    <div class="col-sm-12">
                                        <fieldset class="form-group">

                                            <label for="roundText">  التواصل معنا </label>
                                            <input type="text" name="print_contact" class="form-control" value="{{$print_contact->title}}">
                                            <br>


                                        </fieldset>
                                    </div>
                                    <div class="col-sm-12">
                                        <fieldset class="form-group">

                                            <label for="roundText">    التفاصيل</label>
                                            <input type="text" name="company_details" class="form-control" value="{{$company_details->title}}">
                                            <br>


                                        </fieldset>
                                    </div>

                                </div>
                                <div class="row">
                                    <br>
                                    <button type="submit" id="saveBtn" class="btn gradient-purple-bliss">حفظ</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@push('myjs')


    <script type="text/javascript">

        $(function () {



            {{--$("#saveBtn").click(function (e) {--}}
{{--//                e.preventDefault();--}}

                {{--$("#saveBtn").html('<i class="fa fa-load"></i> ... ');--}}
                {{--$.ajax({--}}

                    {{--data: $("#formData").serialize(),--}}

                    {{--url: "{{ route('profile.update') }}",--}}

                    {{--type: "POST",--}}

                    {{--dataType: 'json',--}}
                    {{--timeout:4000,--}}
                    {{--success: function (data) {--}}
                        {{--$("#saveBtn").html('حفظ');--}}
                        {{--if(data.status==200){--}}
                            {{--Swal.fire({title:" تهانينا ",text:"تم الحفظ بنجاح",--}}
                                {{--type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});--}}

                        {{--}--}}
                        {{--else if(data.status==201){--}}
                            {{--Swal.fire({title:" تهانينا ",text:" البريد الالكتروني مستخدم سابقا",--}}
                                {{--type:"warning",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});--}}
                        {{--}--}}
                        {{--else{--}}
                            {{--Swal.fire({title:" تهانينا ",text:" يرجى المحاولة لاحقا",--}}
                                {{--type:"success",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});--}}
                        {{--}--}}
                        {{--window.location.reload();--}}


                    {{--},--}}

                    {{--error: function (data) {--}}
                        {{--$("#saveBtn").html('حفظ');--}}

                        {{--Swal.fire({title:"حدث خطأ ما",text:" حدث خطأ ما - طول بالك اتوسة وحاول مرة اخرى    ",--}}
                            {{--type:"error",confirmButtonClass:"btn btn-primary",buttonsStyling:!1});--}}

                    {{--}--}}

                {{--});--}}
            {{--}); // end add new record--}}




        });

    </script>

@endpush