<?php

use App\Http\Controllers\AdvertNotificationController;
use App\Http\Controllers\AdvNotificationsTranslationController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\CenterController;
use App\Http\Controllers\CenterEmployeeController;
use App\Http\Controllers\CenterManagerController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CenterAppointmentsController;
use App\Http\Controllers\HeaController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\DelegateController;
use App\Http\Controllers\DistirbuationRegionsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\PharmacistsController;
use App\Http\Controllers\PrintController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SecretariatController;
use App\Http\Controllers\DeliveryTimeController;
use App\Http\Controllers\UserCartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PushNotificationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoryTranslationsController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CoinController;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Http\Controllers\SortController;
use App\Http\Controllers\InboxController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\couponSettingController;
use App\Http\Controllers\DeliverRegionController;
use App\Http\Controllers\HelpTranslationController;
use App\Http\Controllers\InboxTranslationController;
use App\Http\Controllers\LangController;
use App\Http\Controllers\ProductTranslationController;
use App\Models\AdvertNotificationTranslation;
use App\Http\Controllers\UserManagmentController;
use App\Http\Controllers\RuleController;
use App\Http\Controllers\DownloadController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[HomeController::class,'studentHome']);
Route::get('/appointment/home',[AppointmentController::class ,'view'])->name('appointment');
Route::post('/appointment',[AppointmentController::class ,'search'])->name('appointment');
Route::get('/appointment/{tt?}/{center?}',[AppointmentController::class ,'search'])->name('appointment');
Route::post('/appointment-get',[AppointmentController::class ,'getAppointment'])->name('appointment-get');
Route::get('/view-centers',[AppointmentController::class ,'viewCenters'])->name('view-centers');
Route::get('/file-download', [DownloadController::class, 'index'])->name('file.download.index');
Route::get('/app-download', [DownloadController::class, 'indexapp'])->name('app.download.indexapp');



Route::get('/login', function () {
    return view('login');
})->name('login_form');
Route::post("login","App\Http\Controllers\UserController@login")->name("login");
Route::post("logout","App\Http\Controllers\UserController@logout")->name("logout");

Route::get('/home', [HomeController::class, 'index'])->name("home");
Route::get('/profile', [UserController::class, 'showProfile'])->middleware('auth')->name("profile");
Route::post('/profile', [UserController::class, 'updateProfile'])->middleware('auth')->name("profile.update");
Route::group(['middleware' => ['auth']], function() {
Route::resource('center_types', CategoryController::class);
Route::resource('centers', CenterController::class);
Route::resource('center-managers', CenterManagerController::class);
Route::resource('emps', EmployeeController::class);
Route::resource('hea', HeaController::class);

Route::get('center/emps/{id}',[CenterEmployeeController::class ,'index']);
Route::get('center/emps/{id}/{item_id}/edit',[CenterEmployeeController::class ,'edit']);
Route::delete('center/emps/{id}/{item_id}',[CenterEmployeeController::class ,'destroy']);
Route::post('center/emps/{id}',[CenterEmployeeController::class ,'store']);

Route::resource('appointments', CenterAppointmentsController::class);


Route::resource('usersRequests', UserController::class);


//Route::resource('distribuation-region', DistirbuationRegionsController::class);
//Route::get('pharmacists-requests',[ PharmacistsController::class,'pharmasticsRequests'])->name('pharmacists-requests.index');

Route::get('user-carts-requests',[ UserCartController::class,'viewRequests'])->name('user-carts-requests.index');
Route::get('filter-carts-requests/{status}',[ UserCartController::class,'filterRequests'])->name('user-carts-requests.filter');
Route::post('user-carts-request-view/{id}',[ UserCartController::class,'markOrderViewed'])->name('user-carts-requests.viewed');
Route::get('user-carts-request-details/{id}/{region?}',[ UserCartController::class,'getOrderDetails'])->name('order.details');
Route::delete('user-carts-request-details/{id}',[ UserCartController::class,'deleteOrder'])->name('order.delete');
Route::post('user-request-delegate',[ UserCartController::class,'setOrderDelegate'])->name('user-request-delegate');

Route::get('update/order',[UserCartController::class,'updateOrder'])->name('order.update');
Route::post('update/order/{id}',[UserCartController::class,'updateOrder']);

//Route::get('region/delegates/{id}',[ DistirbuationRegionsController::class,'getRegionDelegates'])->name('region.getRegionDelegates');
//Route::get('user-notifications',[ UserNotificationsController::class,'viewNotifications'])->name('user-notifications.index');
//Route::post('user-notifications',[ UserNotificationsController::class,'store'])->name('user-notifications.store');

//
Route::group(['prefix' => 'crud/{table}','middleware'=>['admin']], function($table) {
    Route::get('get', [UserController::class, 'index'])->name('crud.index');
    Route::get('get/{id}/edit', [UserController::class, 'edit'])->name('crud.edit');
    Route::get('add', [UserController::class, 'showForm'])->name('crud.add');
    Route::post('add', [UserController::class, 'store'])->name('crud.store');
    Route::post('get/{id}/update', [UserController::class, 'update'])->name('crud.edit');
    Route::delete('get/{id}/delete', [UserController::class, 'destroy'])->name('crud.destroy');
//
//
}) ;// end crud;

Route::get('categories/subs/{id}', [CategoryController::class, 'getSubs'])->name('category.subs');
Route::get('categories/subs/{id}', [CategoryController::class, 'getSubs'])->name('category.subs1');

//Route::get('categories/subs/index/', [CategoryController::class, 'sub_index'])->name('sub_index');

//get sub cat view
Route::get('categories/subs/index/{id}',[CategoryController::class, 'get_sub_cat'])->name("get_sub.category");
//Route::get('categories/subs/index/',[CategoryController::class, 'get_sub_cat'])->name("get_sub.category");

/// end sub
Route::group(['prefix' => 'sort/{table}'], function($table) {
    Route::get('get', [SortController::class, 'index'])->name('sort.index');
    Route::post('save', [SortController::class, 'store'])->name('sort.store');

}) ;// end get table data select 2;
//ales
//Route::post('store-city',[CountryController::class, 'storeCity']);
//Route::post('countries/cities',[CountryController::class, 'getCities'])->name('cities');
//Route::get('countries/cities/{country_id}',[CountryController::class, 'getCities']);
Route::post('countries/regions/', [CountryController::class, 'getSubs'])->name('countries.regions');
Route::get('countries/regions/{id}', [CountryController::class, 'getSubs']);

    Route::resource('roles', RuleController::class);
    Route::resource('users', UserManagmentController::class);

Route::get('filter/products/{cat_id}',[ProductController::class, 'filter'])->name('products');
Route::get('products_degrease', [ProductController::class, 'degreaseProducts'])->name('degrease.products');
Route::get('filter/degrease_products/{cat_id}',[ProductController::class, 'filterDegreaseProducts'])->name('degrease.products.filter');
Route::get('report/{date}', [ProductController::class, 'report'])->name('report');

//product and category soft delete
Route::get('delete/cat',[CategoryController::class,'deleteCat'])->name('categories.delete');
Route::post('delete/cat/{id}',[CategoryController::class,'deleteCat']);


Route::get('/users-accepted',[UserController::class,'users'])->name('users.accepted');
Route::get('agree/user',[UserController::class,'agree'])->name('usersRequests.agree');
Route::post('agree/user/{id}',[UserController::class,'agree']);

Route::post('send-notification', [PushNotificationController::class, 'sendNotification'])->name('notifications');


//
Route::post('projects/media/{table}', [MediaController::class, 'storeMedia'])
    ->name('projects.storeMedia');
Route::delete('projects/media/{table}', [MediaController::class, 'destroyMedia'])
    ->name('projects.deleteMedia');

Route::post('push-notification', [PushNotificationController::class, 'sendNotification'])
    ->name('app.send-notitication');

Route::get('print-document/{id}/{region?}', [PrintController::class, 'printInvoice']);

Route::post('qrcode/{var}', function ($var) {
    return QrCode::size(250)

        ->generate($var);
});
Route::get('storage-link', function(){
    return Artisan::call('storage:link');
});





//offers ales
Route::resource('offers', OfferController::class);
Route::get('filter/offers/{cat_id}',[OfferController::class, 'filter'])->name('offer.products');
//delete offer
Route::get('delete/offer',[OfferController::class,'deleteOffer'])->name('offers.delete');
Route::post('delete/offer/{id}',[OfferController::class,'deleteOffer']);
//home cards ales
Route::get('view/new_orders',[ HomeController::class,'viewNewOrders'])->name('newOrders');
Route::get('view/all_orders',[ HomeController::class,'viewAllOrders'])->name('allOrders');
//deliver times ales
Route::resource('delivery_times', DeliveryTimeController::class);

Route::prefix('translations')->group(function () {
// categories
Route::get('categories/{id}',[CategoryTranslationsController::class ,'index']);
Route::get('categories/{id}/{item_id}/edit',[CategoryTranslationsController::class ,'edit']);
Route::delete('categories/{id}/{item_id}',[CategoryTranslationsController::class ,'destroy']);
Route::post('categories/{id}',[CategoryTranslationsController::class ,'store']);
// products.
Route::get('product/{id}',[ProductTranslationController::class ,'index']);
Route::get('product/{id}/{item_id}/edit',[ProductTranslationController::class ,'edit']);
Route::delete('product/{id}/{item_id}',[ProductTranslationController::class ,'destroy']);
Route::post('product/{id}',[ProductTranslationController::class ,'store']);
// advert notifications.
Route::get('adv_notif/{id}',[AdvNotificationsTranslationController::class ,'index']);
Route::get('adv_notif/{id}/{item_id}/edit',[AdvNotificationsTranslationController::class ,'edit']);
Route::delete('adv_notif/{id}/{item_id}',[AdvNotificationsTranslationController::class ,'destroy']);
Route::post('adv_notif/{id}',[AdvNotificationsTranslationController::class ,'store']);
// help.
Route::get('help/{id}',[HelpTranslationController::class ,'index']);
Route::get('help/{id}/{item_id}/edit',[HelpTranslationController::class ,'edit']);
Route::delete('help/{id}/{item_id}',[HelpTranslationController::class ,'destroy']);
Route::post('help/{id}',[HelpTranslationController::class ,'store']);
// inbox.
Route::get('inbox/{id}',[InboxTranslationController::class ,'index']);
Route::get('inbox/{id}/{item_id}/edit',[InboxTranslationController::class ,'edit']);
Route::delete('inbox/{id}/{item_id}',[InboxTranslationController::class ,'destroy']);
Route::post('inbox/{id}',[InboxTranslationController::class ,'store']);

});

Route::resource('adv_notif', AdvertNotificationController::class);
Route::resource('lang', LangController::class);
Route::resource('inbox', InboxController::class);
Route::resource('deliverRegion', DeliverRegionController::class);
Route::resource('help', HelpController::class);
Route::resource('contact', ContactController::class);
Route::resource('color', ColorController::class);

// product price
Route::post('price', [ProductController::class, 'store_price'])->name('store_price');
Route::get('product/{id}/prices', [ProductController::class, 'getPrices'])->name('getPrices');
Route::post('updateprice', [ProductController::class, 'updatePrice'])->name('updatePrice');
Route::delete('prices/{id}', [ProductController::class, 'deletePrice'])->name('deletePrice');


//product comments
Route::get('product/{id}/comment', [ProductController::class, 'getComments'])->name('getComments');
Route::delete('comments/{id}', [ProductController::class, 'deletComment'])->name('deletComment');

//coupon
Route::get('couponSetting', [couponSettingController::class,'index'])->name('index');
Route::get('coupon/{id}', [couponSettingController::class, 'getCouponInfo'])->name('getCouponInfo');
Route::post('updateCoupon/{id}', [couponSettingController::class, 'updateCoupon'])->name('updateCoupon');

});
