<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'category-list',
            'category-create',
            'category-edit',
            'category-delete',
            'cat_sub-list',
            'cat_sub-create',
            'cat_sub-edit',
            'cat_sub-delete',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete',
            'coupon-list',
            'coupon-create',
            'coupon-edit',
            'coupon-delete',
            'notify-list',
            'notify-create',
            'notify-edit',
            'notify-delete',
            'color-list',
            'color-create',
            'color-edit',
            'color-delete',
            'inbox-list',
            'inbox-create',
            'inbox-edit',
            'inbox-delete',
            'help-list',
            'help-create',
            'help-edit',
            'help-delete',
            'contact-list',
            'contact-create',
            'contact-edit',
            'contact-delete',
            'coupon_set-list',
            'coupon_set-create',
            'coupon_set-edit',
            'coupon_set-delete',
            'offer-list',
            'offer-create',
            'offer-edit',
            'offer-delete',
            'deliver_time-list',
            'deliver_time-create',
            'deliver_time-edit',
            'deliver_time-delete',
            'deliver_region-list',
            'deliver_region-create',
            'deliver_region-edit',
            'deliver_region-delete',
            'user_cart-list',
            'user_cart-create',
            'user_cart-edit',
            'user_cart-delete',
            'user_request-list',
            'user_request-create',
            'user_request-edit',
            'user_request-delete',

        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
