<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
            for($i=0;$i<3000;$i++){

                DB::table("students")->insert([
            "name" => $faker->name(),
            "last_name" => $faker->name(),
            "mother_name" => $faker->name(),
            "national_id" => "12345678".$i,

            "mobile" => $faker->phoneNumber,
            "center_type_id" => $faker->numberBetween(1, 10),
        ]);
                DB::table("student_appointments")->insert([
            "date" => $faker->date(),

            "time" => $faker->time(),
            "student_id" => "12345678".$i,
            "center_id" =>1,
        ]);
            }
        // \App\Models\User::factory(10)->create();
        // $this->call([
        //     // PermissionTableSeeder::class,
        //     // CreateAdminUserSeeder::class,


        // ]);
    }
}
