<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCenterEmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('center_emps', function (Blueprint $table) {
            $table->id();
            $table->foreignId("center_id");
            $table->foreignId("emp_id");
            $table->integer("status")->default(1);
            $table->time("start_at")->nullable();
            $table->time("leave_at")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('center_emps');
    }
}
