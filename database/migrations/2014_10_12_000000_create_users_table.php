<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->string('phone')->unique()->nullable();
            $table->text('address')->nullable();
            $table->softDeletes();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->integer('role')->default(2);
            $table->string('avatar')->nullable();
            $table->string('image')->nullable();
            $table->string('status')->nullable();
            $table->string('is_verified')->default(0);
            $table->foreignId("center_id")->nullable();


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
