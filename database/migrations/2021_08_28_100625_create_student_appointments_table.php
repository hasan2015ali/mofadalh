<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId("student_id");
            $table->foreignId("center_id");
            $table->date("date");
            $table->time("time");
            $table->integer("status")->default(0);
            $table->foreignId("emp_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_appointments');
    }
}
